package com.ruoyi.common.msg;

import com.ruoyi.common.constant.StatusReturnCodeConstant;

/**
 * @Description: TODO
 * @Author: daisy
 * @Date: Created in 2022/7/12 14:28
 * @Version: 1.0
 */
public class ObjectRestResponse<T> extends BaseResponse {

    T data;
    boolean rel;

    public boolean isRel() {
        return rel;
    }

    public void setRel(boolean rel) {
        this.rel = rel;
    }


    public ObjectRestResponse rel(boolean rel) {
        this.setRel(rel);
        if(rel){
            this.setStatus(StatusReturnCodeConstant.RETURN_CODE_SUCCESS_NOT_ALERT);
        }else{
            this.setStatus(StatusReturnCodeConstant.RETURN_CODE_FAILED);
        }
        return this;
    }


    public ObjectRestResponse data(T data) {
        this.setData(data);
        return this;
    }
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public ObjectRestResponse msg(String msg) {
        this.setMessage(msg);
        return this;
    }

}
