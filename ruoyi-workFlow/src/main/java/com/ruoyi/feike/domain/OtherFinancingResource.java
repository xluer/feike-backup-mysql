package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * feike对象 h_other_financing_resource
 * 
 * @author zmh
 * @date 2022-07-11
 */
public class OtherFinancingResource extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String index;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String institutionName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String loanType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long approvedLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal security;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal interestRate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date dueDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String purpose;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setInstitutionName(String institutionName)
    {
        this.institutionName = institutionName;
    }

    public String getInstitutionName() 
    {
        return institutionName;
    }
    public void setLoanType(String loanType) 
    {
        this.loanType = loanType;
    }

    public String getLoanType() 
    {
        return loanType;
    }
    public void setApprovedLimit(Long approvedLimit) 
    {
        this.approvedLimit = approvedLimit;
    }

    public Long getApprovedLimit() 
    {
        return approvedLimit;
    }
    public void setOs(Long os) 
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setSecurity(BigDecimal security) 
    {
        this.security = security;
    }

    public BigDecimal getSecurity() 
    {
        return security;
    }
    public void setInterestRate(BigDecimal interestRate) 
    {
        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() 
    {
        return interestRate;
    }
    public void setDueDate(Date dueDate) 
    {
        this.dueDate = dueDate;
    }

    public Date getDueDate() 
    {
        return dueDate;
    }
    public void setPurpose(String purpose) 
    {
        this.purpose = purpose;
    }

    public String getPurpose() 
    {
        return purpose;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return "OtherFinancingResource{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", index='" + index + '\'' +
                ", dealername='" + dealername + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", loanType='" + loanType + '\'' +
                ", approvedLimit=" + approvedLimit +
                ", os=" + os +
                ", security=" + security +
                ", interestRate=" + interestRate +
                ", dueDate=" + dueDate +
                ", purpose='" + purpose + '\'' +
                ", instanceId='" + instanceId + '\'' +
                '}';
    }
}
