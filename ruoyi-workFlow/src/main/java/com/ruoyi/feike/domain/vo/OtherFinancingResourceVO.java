package com.ruoyi.feike.domain.vo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 神
 * @date 2022年07月11日 16:29
 */
public class OtherFinancingResourceVO {

    private String id;
    private String index;
    private String dealername;
    private String institutionname;
    private String loantype;
    private Long approvedlimit;
    private Long os;
    private BigDecimal security;
    private BigDecimal interestrate;
    private Date duedate;
    private String purpose;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getInstitutionname() {
        return institutionname;
    }

    public void setInstitutionname(String institutionname) {
        this.institutionname = institutionname;
    }

    public String getLoantype() {
        return loantype;
    }

    public void setLoantype(String loantype) {
        this.loantype = loantype;
    }

    public Long getApprovedlimit() {
        return approvedlimit;
    }

    public void setApprovedlimit(Long approvedlimit) {
        this.approvedlimit = approvedlimit;
    }

    public Long getOs() {
        return os;
    }

    public void setOs(Long os) {
        this.os = os;
    }

    public BigDecimal getSecurity() {
        return security;
    }

    public void setSecurity(BigDecimal security) {
        this.security = security;
    }

    public BigDecimal getInterestrate() {
        return interestrate;
    }

    public void setInterestrate(BigDecimal interestrate) {
        this.interestrate = interestrate;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    @Override
    public String toString() {
        return "OtherFinancingResourceVO{" +
                "id='" + id + '\'' +
                ", index='" + index + '\'' +
                ", dealername='" + dealername + '\'' +
                ", institutionname='" + institutionname + '\'' +
                ", loantype='" + loantype + '\'' +
                ", approvedlimit=" + approvedlimit +
                ", os=" + os +
                ", security=" + security +
                ", interestrate=" + interestrate +
                ", duedate=" + duedate +
                ", purpose='" + purpose + '\'' +
                '}';
    }
}
