package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_securities
 * 
 * @author zmh
 * @date 2022-07-11
 */
public class SecuritieVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String dealername;
    private String guaranteeType;
    private String currentNameCN;
    private String currentNameEN;
    private String proposalNameCN;
    private String proposalNameEN;
    private String personalGuaranteeEN;
    private String index;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getCurrentNameCN() {
        return currentNameCN;
    }

    public void setCurrentNameCN(String currentNameCN) {
        this.currentNameCN = currentNameCN;
    }

    public String getCurrentNameEN() {
        return currentNameEN;
    }

    public void setCurrentNameEN(String currentNameEN) {
        this.currentNameEN = currentNameEN;
    }

    public String getProposalNameCN() {
        return proposalNameCN;
    }

    public void setProposalNameCN(String proposalNameCN) {
        this.proposalNameCN = proposalNameCN;
    }

    public String getProposalNameEN() {
        return proposalNameEN;
    }

    public void setProposalNameEN(String proposalNameEN) {
        this.proposalNameEN = proposalNameEN;
    }

    public String getPersonalGuaranteeEN() {
        return personalGuaranteeEN;
    }

    public void setPersonalGuaranteeEN(String personalGuaranteeEN) {
        this.personalGuaranteeEN = personalGuaranteeEN;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "SecuritieVO{" +
                "id='" + id + '\'' +
                ", dealername='" + dealername + '\'' +
                ", guaranteeType='" + guaranteeType + '\'' +
                ", currentNameCN='" + currentNameCN + '\'' +
                ", currentNameEN='" + currentNameEN + '\'' +
                ", proposalNameCN='" + proposalNameCN + '\'' +
                ", proposalNameEN='" + proposalNameEN + '\'' +
                ", personalGuaranteeEN='" + personalGuaranteeEN + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
