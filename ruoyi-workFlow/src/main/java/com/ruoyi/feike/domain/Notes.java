package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_notes
 * 
 * @author lsn
 * @date 2022-07-06
 */
public class Notes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String basicNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String loyaltyNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String auditNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String wholesaleNotes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proposalNotes;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setBasicNotes(String basicNotes) 
    {
        this.basicNotes = basicNotes;
    }

    public String getBasicNotes() 
    {
        return basicNotes;
    }
    public void setDealerNotes(String dealerNotes) 
    {
        this.dealerNotes = dealerNotes;
    }

    public String getDealerNotes() 
    {
        return dealerNotes;
    }
    public void setLoyaltyNotes(String loyaltyNotes) 
    {
        this.loyaltyNotes = loyaltyNotes;
    }

    public String getLoyaltyNotes() 
    {
        return loyaltyNotes;
    }
    public void setAuditNotes(String auditNotes) 
    {
        this.auditNotes = auditNotes;
    }

    public String getAuditNotes() 
    {
        return auditNotes;
    }
    public void setWholesaleNotes(String wholesaleNotes) 
    {
        this.wholesaleNotes = wholesaleNotes;
    }

    public String getWholesaleNotes() 
    {
        return wholesaleNotes;
    }
    public void setProposalNotes(String proposalNotes) 
    {
        this.proposalNotes = proposalNotes;
    }

    public String getProposalNotes() 
    {
        return proposalNotes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("instanceId", getInstanceId())
            .append("basicNotes", getBasicNotes())
            .append("dealerNotes", getDealerNotes())
            .append("loyaltyNotes", getLoyaltyNotes())
            .append("auditNotes", getAuditNotes())
            .append("wholesaleNotes", getWholesaleNotes())
            .append("proposalNotes", getProposalNotes())
            .toString();
    }
}
