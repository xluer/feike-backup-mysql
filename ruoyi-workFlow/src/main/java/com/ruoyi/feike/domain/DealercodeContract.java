package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生成的合同详细信息对象 h_dealercode_contract
 * 
 * @author ruoyi
 * @date 2022-08-10
 */
public class DealercodeContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 合同号 */
    @Excel(name = "合同号")
    private String contract;

    /** 经销商中文 */
    @Excel(name = "经销商中文")
    private String dealerNameCN;

    /** 主机厂 */
    @Excel(name = "主机厂")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limitType;

    /** 合同地址 */
    @Excel(name = "合同地址")
    private String contractLocation;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractName;

    /** 业务场景  '优先级''New application(4) &gt;Three Party Info Change(6)&gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)；另外还有Temp Limit(7)、MRG limit(8)''',
 */
    @Excel(name = "业务场景")
    private String priority;

    /** 流程id */
    @Excel(name = "流程id")
    private String instanceId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setContract(String contract)
    {
        this.contract = contract;
    }

    public String getContract()
    {
        return contract;
    }
    public void setDealerNameCN(String dealerNameCN) 
    {
        this.dealerNameCN = dealerNameCN;
    }

    public String getDealerNameCN() 
    {
        return dealerNameCN;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setLimitType(String limitType) 
    {
        this.limitType = limitType;
    }

    public String getLimitType() 
    {
        return limitType;
    }
    public void setContractLocation(String contractLocation) 
    {
        this.contractLocation = contractLocation;
    }

    public String getContractLocation() 
    {
        return contractLocation;
    }
    public void setContractName(String contractName) 
    {
        this.contractName = contractName;
    }

    public String getContractName() 
    {
        return contractName;
    }
    public void setPriority(String priority) 
    {
        this.priority = priority;
    }

    public String getPriority() 
    {
        return priority;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contract", getContract())
            .append("dealerNameCN", getDealerNameCN())
            .append("sector", getSector())
            .append("limitType", getLimitType())
            .append("contractLocation", getContractLocation())
            .append("contractName", getContractName())
            .append("priority", getPriority())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
