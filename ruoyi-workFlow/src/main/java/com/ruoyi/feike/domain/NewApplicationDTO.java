package com.ruoyi.feike.domain;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author lss
 * @version 1.0
 * @description: 新经销商申请 比如DS/P 入参DTO
 * @date 2022/8/2 10:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class NewApplicationDTO {
    private static final long serialVersionUID = 1L;
    //基础信息
    private List<BasicInformationDTO> dealername;
    //经销商信息
    private List<DealerNegativeInformationCheck> dealerInformationList;
    //销售业绩及目标
    private List<SalesPerformanceAndTarget> sales_performance_and_target;
    //经销商销量预计计算
    private List<LimitCalculationForCommericalNeeds> limit_calculation_for_commerical_needs;
    //经销商其他相关贷款信息
    private List<OtherFinancingResource> other_financing_resource;
    //经销商创建相关信息
    private List<TemporaryConditionFollowUp> temporaryConditionFollowUpList;
    // TODO 确定改实体类的作用
    private List<ProposalByCommericalAndMarketing> proposal_by_commerical_and_marketing;
    //担保信息
    private List<Securities> securitieList;

    private List<ActFormDTO> actForm;
    //记录信息DTO
    private Notes notesDTO;
    private String basicNotes;
    private String loyaltyNotes;
    private String auditNotes;
    private String wholesaleNotes;
    private String proposalNotes;

    private String processDefinitionKey;

    private String name;

    @NotBlank(message = "流程ID不能为空!")
    private String instanceId;
    @NotBlank(message = "流程业务名称不能为空!")
    private String instanceName;



}
