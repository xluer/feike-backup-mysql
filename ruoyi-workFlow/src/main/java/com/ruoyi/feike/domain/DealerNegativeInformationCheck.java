package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * feike对象 h_dealer_negative_information_check
 *
 * @author zmh
 * @date 2022-07-05
 */
public class DealerNegativeInformationCheck extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerid;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer ishavenagetiveinformation;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String nagetiveinformationtext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer dmvisitandsubmit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String visitandsubmittext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer ishavestockauditissue;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String stockauditissuetext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer isgetcarsto4sshop;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String getcarsto4sshoptext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer ishaveillegalparkingin3month;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String illegalparkingtext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer isabnormalsituation;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String abnormalsituationtext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer ishaveloanextension;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String loanextensiontext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer ispaidup;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String paiduptext;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private boolean isStartDM;

    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private boolean isStartPLM;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerid(String dealerid)
    {
        this.dealerid = dealerid;
    }

    public String getDealerid()
    {
        return dealerid;
    }
    public void setDealername(String dealername)
    {
        this.dealername = dealername;
    }

    public String getDealername()
    {
        return dealername;
    }
    public void setIshavenagetiveinformation(Integer ishavenagetiveinformation)
    {
        this.ishavenagetiveinformation = ishavenagetiveinformation;
    }

    public Integer getIshavenagetiveinformation()
    {
        return ishavenagetiveinformation;
    }
    public void setNagetiveinformationtext(String nagetiveinformationtext)
    {
        this.nagetiveinformationtext = nagetiveinformationtext;
    }

    public String getNagetiveinformationtext()
    {
        return nagetiveinformationtext;
    }
    public void setDmvisitandsubmit(Integer dmvisitandsubmit)
    {
        this.dmvisitandsubmit = dmvisitandsubmit;
    }

    public Integer getDmvisitandsubmit()
    {
        return dmvisitandsubmit;
    }
    public void setVisitandsubmittext(String visitandsubmittext)
    {
        this.visitandsubmittext = visitandsubmittext;
    }

    public String getVisitandsubmittext()
    {
        return visitandsubmittext;
    }
    public void setIshavestockauditissue(Integer ishavestockauditissue)
    {
        this.ishavestockauditissue = ishavestockauditissue;
    }

    public Integer getIshavestockauditissue()
    {
        return ishavestockauditissue;
    }
    public void setStockauditissuetext(String stockauditissuetext)
    {
        this.stockauditissuetext = stockauditissuetext;
    }

    public String getStockauditissuetext()
    {
        return stockauditissuetext;
    }
    public void setIsgetcarsto4sshop(Integer isgetcarsto4sshop)
    {
        this.isgetcarsto4sshop = isgetcarsto4sshop;
    }

    public Integer getIsgetcarsto4sshop()
    {
        return isgetcarsto4sshop;
    }
    public void setGetcarsto4sshoptext(String getcarsto4sshoptext)
    {
        this.getcarsto4sshoptext = getcarsto4sshoptext;
    }

    public String getGetcarsto4sshoptext()
    {
        return getcarsto4sshoptext;
    }
    public void setIshaveillegalparkingin3month(Integer ishaveillegalparkingin3month)
    {
        this.ishaveillegalparkingin3month = ishaveillegalparkingin3month;
    }

    public Integer getIshaveillegalparkingin3month()
    {
        return ishaveillegalparkingin3month;
    }
    public void setIllegalparkingtext(String illegalparkingtext)
    {
        this.illegalparkingtext = illegalparkingtext;
    }

    public String getIllegalparkingtext()
    {
        return illegalparkingtext;
    }
    public void setIsabnormalsituation(Integer isabnormalsituation)
    {
        this.isabnormalsituation = isabnormalsituation;
    }

    public Integer getIsabnormalsituation()
    {
        return isabnormalsituation;
    }
    public void setAbnormalsituationtext(String abnormalsituationtext)
    {
        this.abnormalsituationtext = abnormalsituationtext;
    }

    public String getAbnormalsituationtext()
    {
        return abnormalsituationtext;
    }
    public void setIshaveloanextension(Integer ishaveloanextension)
    {
        this.ishaveloanextension = ishaveloanextension;
    }

    public Integer getIshaveloanextension()
    {
        return ishaveloanextension;
    }
    public void setLoanextensiontext(String loanextensiontext)
    {
        this.loanextensiontext = loanextensiontext;
    }

    public String getLoanextensiontext()
    {
        return loanextensiontext;
    }
    public void setIspaidup(Integer ispaidup)
    {
        this.ispaidup = ispaidup;
    }

    public Integer getIspaidup()
    {
        return ispaidup;
    }
    public void setPaiduptext(String paiduptext)
    {
        this.paiduptext = paiduptext;
    }

    public String getPaiduptext()
    {
        return paiduptext;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public boolean getIsStartDM() {
        return isStartDM;
    }

    public void setIsStartDM(boolean isStartDM) {
        this.isStartDM = isStartDM;
    }

    public boolean getIsStartPLM() {
        return isStartPLM;
    }

    public void setIsStartPLM(boolean isStartPLM) {
        this.isStartPLM = isStartPLM;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerid", getDealerid())
            .append("dealername", getDealername())
            .append("ishavenagetiveinformation", getIshavenagetiveinformation())
            .append("nagetiveinformationtext", getNagetiveinformationtext())
            .append("dmvisitandsubmit", getDmvisitandsubmit())
            .append("visitandsubmittext", getVisitandsubmittext())
            .append("ishavestockauditissue", getIshavestockauditissue())
            .append("stockauditissuetext", getStockauditissuetext())
            .append("isgetcarsto4sshop", getIsgetcarsto4sshop())
            .append("getcarsto4sshoptext", getGetcarsto4sshoptext())
            .append("ishaveillegalparkingin3month", getIshaveillegalparkingin3month())
            .append("illegalparkingtext", getIllegalparkingtext())
            .append("isabnormalsituation", getIsabnormalsituation())
            .append("abnormalsituationtext", getAbnormalsituationtext())
            .append("ishaveloanextension", getIshaveloanextension())
            .append("loanextensiontext", getLoanextensiontext())
            .append("ispaidup", getIspaidup())
            .append("paiduptext", getPaiduptext())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
