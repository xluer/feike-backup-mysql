package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.feike.domain.*;

import java.util.List;

/**
 * @author 神
 * @date 2022年07月18日 16:29
 */
public class AnnualReviewyVO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private List<SalesPerformanceAndTarget> salesPerformanceAndTarget;
    private LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds;
    private ProposalByCommericalAndMarketing proposalByCommericalAndMarketing;
    private OtherFinancingResource otherFinancingResource;
    private Securities securities;
    private Notes notes ;

    public List<SalesPerformanceAndTarget> getSalesPerformanceAndTarget() {
        return salesPerformanceAndTarget;
    }

    public void setSalesPerformanceAndTarget(List<SalesPerformanceAndTarget> salesPerformanceAndTarget) {
        this.salesPerformanceAndTarget = salesPerformanceAndTarget;
    }

    public LimitCalculationForCommericalNeeds getLimitCalculationForCommericalNeeds() {
        return limitCalculationForCommericalNeeds;
    }

    public void setLimitCalculationForCommericalNeeds(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds) {
        this.limitCalculationForCommericalNeeds = limitCalculationForCommericalNeeds;
    }

    public ProposalByCommericalAndMarketing getProposalByCommericalAndMarketing() {
        return proposalByCommericalAndMarketing;
    }

    public void setProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing) {
        this.proposalByCommericalAndMarketing = proposalByCommericalAndMarketing;
    }

    public OtherFinancingResource getOtherFinancingResource() {
        return otherFinancingResource;
    }

    public void setOtherFinancingResource(OtherFinancingResource otherFinancingResource) {
        this.otherFinancingResource = otherFinancingResource;
    }

    public Securities getSecurities() {
        return securities;
    }

    public void setSecurities(Securities securities) {
        this.securities = securities;
    }

    public Notes getNotes() {
        return notes;
    }

    public void setNotes(Notes notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "AnnualReviewyVO{" +
                "salesPerformanceAndTarget=" + salesPerformanceAndTarget +
                ", limitCalculationForCommericalNeeds=" + limitCalculationForCommericalNeeds +
                ", proposalByCommericalAndMarketing=" + proposalByCommericalAndMarketing +
                ", otherFinancingResource=" + otherFinancingResource +
                ", securities=" + securities +
                ", notes=" + notes +
                '}';
    }
}
