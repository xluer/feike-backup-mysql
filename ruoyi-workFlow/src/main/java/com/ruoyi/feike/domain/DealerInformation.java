package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_dealer_information
 *
 * @author zmh
 * @date 2022-08-05
 */
public class DealerInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String groupName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerName;

    /**  注册日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = " 注册日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date incorporationDate;

    /** 厂商授权日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "厂商授权日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date joinSectorDealerNetworkDate;

    /** 经销商英文名 */
    @Excel(name = "经销商英文名")
    private String enName;

    /** 经销商编码 */
    @Excel(name = "经销商编码")
    private String dealerCode;

    /** 注册资本 */
    @Excel(name = "注册资本")
    private Long registeredCapital;

    /** 实缴资本 */
    @Excel(name = "实缴资本")
    private Integer paidUpCapital;

    /** 主机厂 */
    @Excel(name = "主机厂")
    private String make;

    /** 总经理 */
    @Excel(name = "总经理")
    private String gm;

    /** 个人担保 */
    @Excel(name = "个人担保")
    private String individualGuarantor;

    /** 部门担保 */
    @Excel(name = "部门担保")
    private String corporateGuarantor;

    /** 工作年限 */
    @Excel(name = "工作年限")
    private Double relatedWorkingExperience;

    /** 股东and股份占比 */
    @Excel(name = "股东and股份占比")
    private String shareholderStructure;

    /** 股东 */
    @Excel(name = "股东")
    private String shareholders;

    /** 股份占比 */
    @Excel(name = "股份占比")
    private Double share;

    /** $column.columnComment */
    private String instanceId;

    /** 优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)； */
    @Excel(name = "优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)；")
    private Integer priority;

    /** 触发新增修改后存主键ID */
    @Excel(name = "触发新增修改后存主键ID")
    private String priorityId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId()
    {
        return dealerId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
    public void setDealerName(String dealerName)
    {
        this.dealerName = dealerName;
    }

    public String getDealerName()
    {
        return dealerName;
    }
    public void setIncorporationDate(Date incorporationDate)
    {
        this.incorporationDate = incorporationDate;
    }

    public Date getIncorporationDate()
    {
        return incorporationDate;
    }
    public void setJoinSectorDealerNetworkDate(Date joinSectorDealerNetworkDate)
    {
        this.joinSectorDealerNetworkDate = joinSectorDealerNetworkDate;
    }

    public Date getJoinSectorDealerNetworkDate()
    {
        return joinSectorDealerNetworkDate;
    }
    public void setEnName(String enName)
    {
        this.enName = enName;
    }

    public String getEnName()
    {
        return enName;
    }
    public void setDealerCode(String dealerCode)
    {
        this.dealerCode = dealerCode;
    }

    public String getDealerCode()
    {
        return dealerCode;
    }
    public void setRegisteredCapital(Long registeredCapital)
    {
        this.registeredCapital = registeredCapital;
    }

    public Long getRegisteredCapital()
    {
        return registeredCapital;
    }
    public void setPaidUpCapital(Integer paidUpCapital)
    {
        this.paidUpCapital = paidUpCapital;
    }

    public Integer getPaidUpCapital()
    {
        return paidUpCapital;
    }
    public void setMake(String make)
    {
        this.make = make;
    }

    public String getMake()
    {
        return make;
    }
    public void setGm(String gm)
    {
        this.gm = gm;
    }

    public String getGm()
    {
        return gm;
    }
    public void setIndividualGuarantor(String individualGuarantor)
    {
        this.individualGuarantor = individualGuarantor;
    }

    public String getIndividualGuarantor()
    {
        return individualGuarantor;
    }
    public void setCorporateGuarantor(String corporateGuarantor)
    {
        this.corporateGuarantor = corporateGuarantor;
    }

    public String getCorporateGuarantor()
    {
        return corporateGuarantor;
    }
    public void setRelatedWorkingExperience(Double relatedWorkingExperience)
    {
        this.relatedWorkingExperience = relatedWorkingExperience;
    }

    public Double getRelatedWorkingExperience()
    {
        return relatedWorkingExperience;
    }
    public void setShareholderStructure(String shareholderStructure)
    {
        this.shareholderStructure = shareholderStructure;
    }

    public String getShareholderStructure()
    {
        return shareholderStructure;
    }
    public void setShareholders(String shareholders)
    {
        this.shareholders = shareholders;
    }

    public String getShareholders()
    {
        return shareholders;
    }
    public void setShare(Double share)
    {
        this.share = share;
    }

    public Double getShare()
    {
        return share;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }
    public void setPriority(Integer priority)
    {
        this.priority = priority;
    }

    public Integer getPriority()
    {
        return priority;
    }
    public void setPriorityId(String priorityId)
    {
        this.priorityId = priorityId;
    }

    public String getPriorityId()
    {
        return priorityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("groupId", getGroupId())
            .append("groupName", getGroupName())
            .append("dealerName", getDealerName())
            .append("incorporationDate", getIncorporationDate())
            .append("joinSectorDealerNetworkDate", getJoinSectorDealerNetworkDate())
            .append("enName", getEnName())
            .append("dealerCode", getDealerCode())
            .append("registeredCapital", getRegisteredCapital())
            .append("paidUpCapital", getPaidUpCapital())
            .append("make", getMake())
            .append("gm", getGm())
            .append("individualGuarantor", getIndividualGuarantor())
            .append("corporateGuarantor", getCorporateGuarantor())
            .append("relatedWorkingExperience", getRelatedWorkingExperience())
            .append("shareholderStructure", getShareholderStructure())
            .append("shareholders", getShareholders())
            .append("share", getShare())
            .append("instanceId", getInstanceId())
            .append("priority", getPriority())
            .append("priorityId", getPriorityId())
            .toString();
    }
}
