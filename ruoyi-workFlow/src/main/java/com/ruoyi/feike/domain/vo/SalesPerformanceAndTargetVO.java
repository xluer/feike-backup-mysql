package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_sales_performance_and_target
 *
 * @author zmh
 * @date 2022-07-05
 */
public class SalesPerformanceAndTargetVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String dealername;
    private String item;
    private String sector;
    private Long YR2021Actual;
    private Long actualYTDyear2;
    private Long target2022;
    private String index;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public Long getYR2021Actual() {
        return YR2021Actual;
    }

    public void setYR2021Actual(Long YR2021Actual) {
        this.YR2021Actual = YR2021Actual;
    }

    public Long getActualYTDyear2() {
        return actualYTDyear2;
    }

    public void setActualYTDyear2(Long actualYTDyear2) {
        this.actualYTDyear2 = actualYTDyear2;
    }

    public Long getTarget2022() {
        return target2022;
    }

    public void setTarget2022(Long target2022) {
        this.target2022 = target2022;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "SalesPerformanceAndTargetVO{" +
                "id='" + id + '\'' +
                ", dealername='" + dealername + '\'' +
                ", item='" + item + '\'' +
                ", sector='" + sector + '\'' +
                ", YR2021Actual=" + YR2021Actual +
                ", actualYTDyear2=" + actualYTDyear2 +
                ", target2022=" + target2022 +
                ", index='" + index + '\'' +
                '}';
    }
}
