package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * feike对象 h_tier_cash_deposit
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public class TierCashDeposit extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dearler;

    /** 经销商中文名称 */
    @Excel(name = "经销商中文名称")
    private String dealerNameCn;

    /** 集团中文名称 */
    @Excel(name = "集团中文名称")
    private String groupNameCn;

    /** 经销商编码 */
    @Excel(name = "经销商编码")
    private String dealerCode;
    // private String dealerCodes;

    /** 状态 */
    @Excel(name = "状态")
    private String status;
    private Long permitted2ndTierWithoutDesposit;
    private Long permitted2ndTierWithDesposit;
    private BigDecimal registeredTwoTierNumber;
    private BigDecimal twoTierWithDesposit;

    /** 停车位置 */
    @Excel(name = "停车位置")
    private String parkingLocation;

    /*  新加的字段  */
    private String parkinglocations;
    private String[] parkinglocationList;

    /** 单位 */
    @Excel(name = "单位")
    private String units;

    /** 应收账款 */
    @Excel(name = "应收账款")
    private Long tierDepositReceivable;

    /** 保证金差额 */
    @Excel(name = "保证金差额")
    private Long tierDepositGap;

    /** 退款类型 */
    @Excel(name = "退款类型")
    private String refundType;

    /** 原因 */
    @Excel(name = "原因")
    private String reason;

    /** 每日银行反馈 */
    @Excel(name = "每日银行反馈")
    private String dailyBankFeedback;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String notes;

    /** 上传资料 */
    @Excel(name = "上传资料")
    private String uploadFile;

    /** 实例id */
    @Excel(name = "实例id")
    private String instanceId;

    private List<Fileupload> depositFileList;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDearler(String dearler) 
    {
        this.dearler = dearler;
    }

    public String getDearler() 
    {
        return dearler;
    }
    public void setDealerNameCn(String dealerNameCn) 
    {
        this.dealerNameCn = dealerNameCn;
    }

    public String getDealerNameCn() 
    {
        return dealerNameCn;
    }
    public void setGroupNameCn(String groupNameCn) 
    {
        this.groupNameCn = groupNameCn;
    }

    public String getGroupNameCn() 
    {
        return groupNameCn;
    }
    public void setDealerCode(String dealerCode) 
    {
        this.dealerCode = dealerCode;
    }

    public String getDealerCode() 
    {
        return dealerCode;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setParkingLocation(String parkingLocation) 
    {
        this.parkingLocation = parkingLocation;
    }

    public String getParkingLocation() 
    {
        return parkingLocation;
    }
    public void setUnits(String units) 
    {
        this.units = units;
    }

    public String getUnits() 
    {
        return units;
    }
    public void setTierDepositReceivable(Long tierDepositReceivable) 
    {
        this.tierDepositReceivable = tierDepositReceivable;
    }

    public Long getTierDepositReceivable() 
    {
        return tierDepositReceivable;
    }
    public void setTierDepositGap(Long tierDepositGap) 
    {
        this.tierDepositGap = tierDepositGap;
    }

    public Long getTierDepositGap() 
    {
        return tierDepositGap;
    }
    public void setRefundType(String refundType) 
    {
        this.refundType = refundType;
    }

    public String getRefundType() 
    {
        return refundType;
    }
    public void setReason(String reason) 
    {
        this.reason = reason;
    }

    public String getReason() 
    {
        return reason;
    }
    public void setDailyBankFeedback(String dailyBankFeedback) 
    {
        this.dailyBankFeedback = dailyBankFeedback;
    }

    public String getDailyBankFeedback() 
    {
        return dailyBankFeedback;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }
    public void setUploadFile(String uploadFile) 
    {
        this.uploadFile = uploadFile;
    }

    public String getUploadFile() 
    {
        return uploadFile;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public List<Fileupload> getDepositFileList() {
        return depositFileList;
    }

    public void setDepositFileList(List<Fileupload> depositFileList) {
        this.depositFileList = depositFileList;
    }

    public Long getPermitted2ndTierWithoutDesposit() {
        return permitted2ndTierWithoutDesposit;
    }

    public void setPermitted2ndTierWithoutDesposit(Long permitted2ndTierWithoutDesposit) {
        this.permitted2ndTierWithoutDesposit = permitted2ndTierWithoutDesposit;
    }

    public Long getPermitted2ndTierWithDesposit() {
        return permitted2ndTierWithDesposit;
    }

    public void setPermitted2ndTierWithDesposit(Long permitted2ndTierWithDesposit) {
        this.permitted2ndTierWithDesposit = permitted2ndTierWithDesposit;
    }

    public BigDecimal getRegisteredTwoTierNumber() {
        return registeredTwoTierNumber;
    }

    public void setRegisteredTwoTierNumber(BigDecimal registeredTwoTierNumber) {
        this.registeredTwoTierNumber = registeredTwoTierNumber;
    }

    public BigDecimal getTwoTierWithDesposit() {
        return twoTierWithDesposit;
    }

    public void setTwoTierWithDesposit(BigDecimal twoTierWithDesposit) {
        this.twoTierWithDesposit = twoTierWithDesposit;
    }

    public String getParkinglocations() {
        return parkinglocations;
    }

    public void setParkinglocations(String parkinglocations) {
        this.parkinglocations = parkinglocations;
    }

    public String[] getParkinglocationList() {
        return parkinglocationList;
    }

    public void setParkinglocationList(String[] parkinglocationList) {
        this.parkinglocationList = parkinglocationList;
    }

    @Override
    public String toString() {
        return "TierCashDeposit{" +
                "id='" + id + '\'' +
                ", dearler='" + dearler + '\'' +
                ", dealerNameCn='" + dealerNameCn + '\'' +
                ", groupNameCn='" + groupNameCn + '\'' +
                ", dealerCode='" + dealerCode + '\'' +
                ", status='" + status + '\'' +
                ", permitted2ndTierWithoutDesposit=" + permitted2ndTierWithoutDesposit +
                ", permitted2ndTierWithDesposit=" + permitted2ndTierWithDesposit +
                ", registeredTwoTierNumber=" + registeredTwoTierNumber +
                ", twoTierWithDesposit=" + twoTierWithDesposit +
                ", parkingLocation='" + parkingLocation + '\'' +
                ", parkinglocations='" + parkinglocations + '\'' +
                ", units='" + units + '\'' +
                ", tierDepositReceivable=" + tierDepositReceivable +
                ", tierDepositGap=" + tierDepositGap +
                ", refundType='" + refundType + '\'' +
                ", reason='" + reason + '\'' +
                ", dailyBankFeedback='" + dailyBankFeedback + '\'' +
                ", notes='" + notes + '\'' +
                ", uploadFile='" + uploadFile + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", depositFileList=" + depositFileList +
                '}';
    }
}
