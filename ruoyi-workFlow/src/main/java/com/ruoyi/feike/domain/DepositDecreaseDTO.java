package com.ruoyi.feike.domain;

import lombok.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

/**
 * @author lss
 * @version 1.0
 * @description: TODO
 * @date 2022/8/10 21:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class DepositDecreaseDTO {
    //DSO&AGING区分标识，0=DSO;1=AGING
    private String isDso;
    //经销商code字符串
    private String dealerCodeStr;
    //外部接口经销商数组
    private List<String[]> dealerCodeArr;
    //当前年月前三个月
    private String[]  yearMonth;
    //最小月
    private String minYearMonth;
    //最大月
    private String maxYearMonth;

    private String pivotStr;

    private String fieldStr;

    private String[] dealerName;

}
