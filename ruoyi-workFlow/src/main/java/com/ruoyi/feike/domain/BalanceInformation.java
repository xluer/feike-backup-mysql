package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_balance_information
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class BalanceInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long depositCashAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long currentSuspenseAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long unPaidInterestAndFees;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String writeOff;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long unmortgageamount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long netBalance;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setDepositCashAmount(Long depositCashAmount) 
    {
        this.depositCashAmount = depositCashAmount;
    }

    public Long getDepositCashAmount() 
    {
        return depositCashAmount;
    }
    public void setCurrentSuspenseAmount(Long currentSuspenseAmount) 
    {
        this.currentSuspenseAmount = currentSuspenseAmount;
    }

    public Long getCurrentSuspenseAmount() 
    {
        return currentSuspenseAmount;
    }
    public void setOs(Long os) 
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setUnPaidInterestAndFees(Long unPaidInterestAndFees) 
    {
        this.unPaidInterestAndFees = unPaidInterestAndFees;
    }

    public Long getUnPaidInterestAndFees() 
    {
        return unPaidInterestAndFees;
    }
    public void setWriteOff(String writeOff) 
    {
        this.writeOff = writeOff;
    }

    public String getWriteOff() 
    {
        return writeOff;
    }
    public void setUnmortgageamount(Long unmortgageamount) 
    {
        this.unmortgageamount = unmortgageamount;
    }

    public Long getUnmortgageamount() 
    {
        return unmortgageamount;
    }
    public void setNetBalance(Long netBalance) 
    {
        this.netBalance = netBalance;
    }

    public Long getNetBalance() 
    {
        return netBalance;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("depositCashAmount", getDepositCashAmount())
            .append("currentSuspenseAmount", getCurrentSuspenseAmount())
            .append("os", getOs())
            .append("unPaidInterestAndFees", getUnPaidInterestAndFees())
            .append("writeOff", getWriteOff())
            .append("unmortgageamount", getUnmortgageamount())
            .append("netBalance", getNetBalance())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
