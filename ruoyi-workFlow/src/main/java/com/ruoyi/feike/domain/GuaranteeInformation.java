package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_guarantee_information
 * 
 * @author zmh
 * @date 2022-07-14
 */
public class GuaranteeInformation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String guaranteeType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String currentNameCn;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String currentNameEn;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proposalNameCn;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proposalNameEn;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setGuaranteeType(String guaranteeType) 
    {
        this.guaranteeType = guaranteeType;
    }

    public String getGuaranteeType() 
    {
        return guaranteeType;
    }
    public void setCurrentNameCn(String currentNameCn) 
    {
        this.currentNameCn = currentNameCn;
    }

    public String getCurrentNameCn() 
    {
        return currentNameCn;
    }
    public void setCurrentNameEn(String currentNameEn) 
    {
        this.currentNameEn = currentNameEn;
    }

    public String getCurrentNameEn() 
    {
        return currentNameEn;
    }
    public void setProposalNameCn(String proposalNameCn) 
    {
        this.proposalNameCn = proposalNameCn;
    }

    public String getProposalNameCn() 
    {
        return proposalNameCn;
    }
    public void setProposalNameEn(String proposalNameEn) 
    {
        this.proposalNameEn = proposalNameEn;
    }

    public String getProposalNameEn() 
    {
        return proposalNameEn;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("guaranteeType", getGuaranteeType())
            .append("currentNameCn", getCurrentNameCn())
            .append("currentNameEn", getCurrentNameEn())
            .append("proposalNameCn", getProposalNameCn())
            .append("proposalNameEn", getProposalNameEn())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
