package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_cb_fleet_details
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class CbFleetDetails extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fleetCustomer;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date paymentDays;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long proposalLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long additionalDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String vehicle;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long seq;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String vinNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long originalBalance;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setFleetCustomer(String fleetCustomer) 
    {
        this.fleetCustomer = fleetCustomer;
    }

    public String getFleetCustomer() 
    {
        return fleetCustomer;
    }
    public void setPaymentDays(Date paymentDays) 
    {
        this.paymentDays = paymentDays;
    }

    public Date getPaymentDays() 
    {
        return paymentDays;
    }
    public void setProposalLimit(Long proposalLimit) 
    {
        this.proposalLimit = proposalLimit;
    }

    public Long getProposalLimit() 
    {
        return proposalLimit;
    }
    public void setAdditionalDeposit(Long additionalDeposit) 
    {
        this.additionalDeposit = additionalDeposit;
    }

    public Long getAdditionalDeposit() 
    {
        return additionalDeposit;
    }
    public void setVehicle(String vehicle) 
    {
        this.vehicle = vehicle;
    }

    public String getVehicle() 
    {
        return vehicle;
    }
    public void setSeq(Long seq) 
    {
        this.seq = seq;
    }

    public Long getSeq() 
    {
        return seq;
    }
    public void setVinNo(String vinNo) 
    {
        this.vinNo = vinNo;
    }

    public String getVinNo() 
    {
        return vinNo;
    }
    public void setOriginalBalance(Long originalBalance) 
    {
        this.originalBalance = originalBalance;
    }

    public Long getOriginalBalance() 
    {
        return originalBalance;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("dealerName", getDealerName())
            .append("fleetCustomer", getFleetCustomer())
            .append("paymentDays", getPaymentDays())
            .append("proposalLimit", getProposalLimit())
            .append("additionalDeposit", getAdditionalDeposit())
            .append("vehicle", getVehicle())
            .append("seq", getSeq())
            .append("vinNo", getVinNo())
            .append("originalBalance", getOriginalBalance())
            .append("instanceId", getInstanceId())
            .toString();
    }
}
