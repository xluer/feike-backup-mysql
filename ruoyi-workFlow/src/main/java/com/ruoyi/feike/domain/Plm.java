package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * plm导入表对象 h_plm
 *
 * @author ruoyi
 * @date 2022-07-21
 */
public class Plm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealernamecnid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealernamecn;

    /** 年月 */
//    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "年月")
    private String month;

    /** 失信销售 */
    @Excel(name = "失信销售")
    private Long sot;

    /** 延迟还款 */
    @Excel(name = "延迟还款")
    private Long dpd;

    /** 二网违规 */
    @Excel(name = "二网违规")
    private Long twondtierviolation;

    /** 贷后评级 */
    @Excel(name = "贷后评级")
    private Long dc;

    /** 允许无偿使用二网数 */
    @Excel(name = "允许无偿使用二网数")
    private Long permitted2ndtierwithoutdesposit;

    /** 允许交保证金使用二网数 */
    @Excel(name = "允许交保证金使用二网数")
    private Long permitted2ndtierwithdesposit;

    /** 允许报备最大二网数 */
    @Excel(name = "允许报备最大二网数")
    private Long max2ndtier;

    private String instanceid;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealernamecnid(String dealernamecnid)
    {
        this.dealernamecnid = dealernamecnid;
    }

    public String getDealernamecnid()
    {
        return dealernamecnid;
    }
    public void setDealernamecn(String dealernamecn)
    {
        this.dealernamecn = dealernamecn;
    }

    public String getDealernamecn()
    {
        return dealernamecn;
    }
    public void setMonth(String month)
    {
        this.month = month;
    }

    public String getMonth()
    {
        return month;
    }
    public void setSot(Long sot)
    {
        this.sot = sot;
    }

    public Long getSot()
    {
        return sot;
    }
    public void setDpd(Long dpd)
    {
        this.dpd = dpd;
    }

    public Long getDpd()
    {
        return dpd;
    }
    public void setTwondtierviolation(Long twondtierviolation)
    {
        this.twondtierviolation = twondtierviolation;
    }

    public Long getTwondtierviolation()
    {
        return twondtierviolation;
    }
    public void setDc(Long dc)
    {
        this.dc = dc;
    }

    public Long getDc()
    {
        return dc;
    }
    public void setPermitted2ndtierwithoutdesposit(Long permitted2ndtierwithoutdesposit)
    {
        this.permitted2ndtierwithoutdesposit = permitted2ndtierwithoutdesposit;
    }

    public Long getPermitted2ndtierwithoutdesposit()
    {
        return permitted2ndtierwithoutdesposit;
    }
    public void setPermitted2ndtierwithdesposit(Long permitted2ndtierwithdesposit)
    {
        this.permitted2ndtierwithdesposit = permitted2ndtierwithdesposit;
    }

    public Long getPermitted2ndtierwithdesposit()
    {
        return permitted2ndtierwithdesposit;
    }
    public void setMax2ndtier(Long max2ndtier)
    {
        this.max2ndtier = max2ndtier;
    }

    public Long getMax2ndtier()
    {
        return max2ndtier;
    }
    public void setInstanceid(String instanceid)
    {
        this.instanceid = instanceid;
    }

    public String getInstanceid()
    {
        return instanceid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("dealernamecnid", getDealernamecnid())
                .append("dealernamecn", getDealernamecn())
                .append("month", getMonth())
                .append("sot", getSot())
                .append("dpd", getDpd())
                .append("twondtierviolation", getTwondtierviolation())
                .append("dc", getDc())
                .append("permitted2ndtierwithoutdesposit", getPermitted2ndtierwithoutdesposit())
                .append("permitted2ndtierwithdesposit", getPermitted2ndtierwithdesposit())
                .append("max2ndtier", getMax2ndtier())
                .append("instanceid", getInstanceid())
                .toString();
    }
}
