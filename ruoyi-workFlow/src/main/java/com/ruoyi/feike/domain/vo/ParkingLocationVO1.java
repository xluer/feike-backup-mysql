package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.feike.domain.Fileupload;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * feike对象 h_parking_location
 *
 * @author zmh
 * @date 2022-07-22
 */
public class ParkingLocationVO1 extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String groupId;
    private String groupName;
    private String dealernamecnid;
    private String dealernamecn;
    private String dealercode;
    private List dealercCodes;
    private String parkinglocation;
    private String[] parkinglocations;
    private String type;

    /** 起始日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectivedate;

    /** 到期日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date duedate;

    private String vin;
    private String distancefrom4s;
    private String tel;
    private String instanceId;

    /** 当前网点状态 */
    @Excel(name = "当前网点状态",readConverterExp = "0=启用,1=禁用,2=失效")
    private String status;

    // private BigDecimal twondtierdeposit;

    private Long permitted2ndTierWithoutDesposit;
    private Long permitted2ndTierWithDesposit;
    private BigDecimal registeredTwoTierNumber;
    private BigDecimal twoTierWithDesposit;

    private Long tierDepositReceivable;
    private Long tierDepositGap;

    private String notes;

    private List<Fileupload> uploadFile;

    public String[] getParkinglocations() {
        return parkinglocations;
    }

    public void setParkinglocations(String[] parkinglocations) {
        this.parkinglocations = parkinglocations;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Fileupload> getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(List<Fileupload> uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealernamecnid(String dealernamecnid)
    {
        this.dealernamecnid = dealernamecnid;
    }

    public String getDealernamecnid()
    {
        return dealernamecnid;
    }
    public void setDealernamecn(String dealernamecn)
    {
        this.dealernamecn = dealernamecn;
    }

    public String getDealernamecn()
    {
        return dealernamecn;
    }
    public void setDealercode(String dealercode)
    {
        this.dealercode = dealercode;
    }

    public String getDealercode()
    {
        return dealercode;
    }
    public void setParkinglocation(String parkinglocation)
    {
        this.parkinglocation = parkinglocation;
    }

    public String getParkinglocation()
    {
        return parkinglocation;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setEffectivedate(Date effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public Date getEffectivedate()
    {
        return effectivedate;
    }
    public void setDuedate(Date duedate)
    {
        this.duedate = duedate;
    }

    public Date getDuedate()
    {
        return duedate;
    }
    public void setVin(String vin)
    {
        this.vin = vin;
    }

    public String getVin()
    {
        return vin;
    }
    public void setDistancefrom4s(String distancefrom4s)
    {
        this.distancefrom4s = distancefrom4s;
    }

    public String getDistancefrom4s()
    {
        return distancefrom4s;
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getTel()
    {
        return tel;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    // public void setTwondtierdeposit(BigDecimal twondtierdeposit)
    // {
    //     this.twondtierdeposit = twondtierdeposit;
    // }
    //
    // public BigDecimal getTwondtierdeposit()
    // {
    //     return twondtierdeposit;
    // }

    public List getDealercCodes() {
        return dealercCodes;
    }

    public void setDealercCodes(List dealercCodes) {
        this.dealercCodes = dealercCodes;
    }

    public Long getPermitted2ndTierWithoutDesposit() {
        return permitted2ndTierWithoutDesposit;
    }

    public void setPermitted2ndTierWithoutDesposit(Long permitted2ndTierWithoutDesposit) {
        this.permitted2ndTierWithoutDesposit = permitted2ndTierWithoutDesposit;
    }

    public Long getPermitted2ndTierWithDesposit() {
        return permitted2ndTierWithDesposit;
    }

    public void setPermitted2ndTierWithDesposit(Long permitted2ndTierWithDesposit) {
        this.permitted2ndTierWithDesposit = permitted2ndTierWithDesposit;
    }

    public BigDecimal getRegisteredTwoTierNumber() {
        return registeredTwoTierNumber;
    }

    public void setRegisteredTwoTierNumber(BigDecimal registeredTwoTierNumber) {
        this.registeredTwoTierNumber = registeredTwoTierNumber;
    }

    public BigDecimal getTwoTierWithDesposit() {
        return twoTierWithDesposit;
    }

    public void setTwoTierWithDesposit(BigDecimal twoTierWithDesposit) {
        this.twoTierWithDesposit = twoTierWithDesposit;
    }

    public Long getTierDepositReceivable() {
        return tierDepositReceivable;
    }

    public void setTierDepositReceivable(Long tierDepositReceivable) {
        this.tierDepositReceivable = tierDepositReceivable;
    }

    public Long getTierDepositGap() {
        return tierDepositGap;
    }

    public void setTierDepositGap(Long tierDepositGap) {
        this.tierDepositGap = tierDepositGap;
    }

    @Override
    public String toString() {
        return "ParkingLocationVO1{" +
                "id='" + id + '\'' +
                ", groupId='" + groupId + '\'' +
                ", groupName='" + groupName + '\'' +
                ", dealernamecnid='" + dealernamecnid + '\'' +
                ", dealernamecn='" + dealernamecn + '\'' +
                ", dealercode='" + dealercode + '\'' +
                ", dealercCodes=" + dealercCodes +
                ", parkinglocation='" + parkinglocation + '\'' +
                ", parkinglocations=" + Arrays.toString(parkinglocations) +
                ", type='" + type + '\'' +
                ", effectivedate=" + effectivedate +
                ", duedate=" + duedate +
                ", vin='" + vin + '\'' +
                ", distancefrom4s='" + distancefrom4s + '\'' +
                ", tel='" + tel + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", status='" + status + '\'' +
                ", permitted2ndTierWithoutDesposit=" + permitted2ndTierWithoutDesposit +
                ", permitted2ndTierWithDesposit=" + permitted2ndTierWithDesposit +
                ", registeredTwoTierNumber=" + registeredTwoTierNumber +
                ", twoTierWithDesposit=" + twoTierWithDesposit +
                ", tierDepositReceivable=" + tierDepositReceivable +
                ", tierDepositGap=" + tierDepositGap +
                ", notes='" + notes + '\'' +
                ", uploadFile=" + uploadFile +
                '}';
    }
}
