package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同信息对象 h_basic_contract
 *
 * @author ybw
 * @date 2022-07-19
 */
public class BasicContract extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractname;

    /** 合同地址 */
    @Excel(name = "合同地址")
    private String contractlocation;

    /** 优先级 */
    @Excel(name = "优先级")
    private String priority;

    /** 主机厂 */
    @Excel(name = "主机厂")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limittype;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setContractname(String contractname)
    {
        this.contractname = contractname;
    }

    public String getContractname()
    {
        return contractname;
    }
    public void setContractlocation(String contractlocation)
    {
        this.contractlocation = contractlocation;
    }

    public String getContractlocation()
    {
        return contractlocation;
    }
    public void setPriority(String priority)
    {
        this.priority = priority;
    }

    public String getPriority()
    {
        return priority;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setLimittype(String limittype)
    {
        this.limittype = limittype;
    }

    public String getLimittype()
    {
        return limittype;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("contractname", getContractname())
                .append("contractlocation", getContractlocation())
                .append("priority", getPriority())
                .append("sector", getSector())
                .append("limittype", getLimittype())
                .toString();
    }
}
