package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * feike对象 h_securities
 * 
 * @author zmh
 * @date 2022-07-11
 */
public class Securities extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String guaranteeType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String currentNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String currentNameEN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proposalNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String personalGuaranteeEN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String proposalNameEN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String index;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setGuaranteeType(String guaranteeType)
    {
        this.guaranteeType = guaranteeType;
    }

    public String getGuaranteeType() 
    {
        return guaranteeType;
    }
    public void setCurrentNameCN(String currentNameCN) 
    {
        this.currentNameCN = currentNameCN;
    }

    public String getCurrentNameCN() 
    {
        return currentNameCN;
    }
    public void setCurrentNameEN(String currentNameEN) 
    {
        this.currentNameEN = currentNameEN;
    }

    public String getCurrentNameEN() 
    {
        return currentNameEN;
    }
    public void setProposalNameCN(String proposalNameCN) 
    {
        this.proposalNameCN = proposalNameCN;
    }

    public String getProposalNameCN() 
    {
        return proposalNameCN;
    }
    public void setPersonalGuaranteeEN(String personalGuaranteeEN) 
    {
        this.personalGuaranteeEN = personalGuaranteeEN;
    }

    public String getPersonalGuaranteeEN() 
    {
        return personalGuaranteeEN;
    }
    public void setProposalNameEN(String proposalNameEN) 
    {
        this.proposalNameEN = proposalNameEN;
    }

    public String getProposalNameEN() 
    {
        return proposalNameEN;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Securities{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", guaranteeType='" + guaranteeType + '\'' +
                ", currentNameCN='" + currentNameCN + '\'' +
                ", currentNameEN='" + currentNameEN + '\'' +
                ", proposalNameCN='" + proposalNameCN + '\'' +
                ", personalGuaranteeEN='" + personalGuaranteeEN + '\'' +
                ", proposalNameEN='" + proposalNameEN + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
