package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * feike对象 h_limit_calculation_for_commerical_needs
 * 
 * @author zmh
 * @date 2022-07-11
 */
public class LimitCalculationForCommericalNeeds extends BaseEntity  implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String model;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long aip;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String atd;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long estimatedannualwholesaleunits;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long creditlimitfornewvehicle;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String index;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }

    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setModel(String model) 
    {
        this.model = model;
    }

    public String getModel() 
    {
        return model;
    }
    public void setAip(Long aip) 
    {
        this.aip = aip;
    }

    public Long getAip() 
    {
        return aip;
    }
    public void setAtd(String atd) 
    {
        this.atd = atd;
    }

    public String getAtd() 
    {
        return atd;
    }
    public void setEstimatedannualwholesaleunits(Long estimatedannualwholesaleunits) 
    {
        this.estimatedannualwholesaleunits = estimatedannualwholesaleunits;
    }

    public Long getEstimatedannualwholesaleunits() 
    {
        return estimatedannualwholesaleunits;
    }
    public void setCreditlimitfornewvehicle(Long creditlimitfornewvehicle) 
    {
        this.creditlimitfornewvehicle = creditlimitfornewvehicle;
    }

    public Long getCreditlimitfornewvehicle() 
    {
        return creditlimitfornewvehicle;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "LimitCalculationForCommericalNeeds{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", model='" + model + '\'' +
                ", aip=" + aip +
                ", atd='" + atd + '\'' +
                ", estimatedannualwholesaleunits=" + estimatedannualwholesaleunits +
                ", creditlimitfornewvehicle=" + creditlimitfornewvehicle +
                ", instanceId='" + instanceId + '\'' +
                ", index='" + index + '\'' +
                '}';
    }
}
