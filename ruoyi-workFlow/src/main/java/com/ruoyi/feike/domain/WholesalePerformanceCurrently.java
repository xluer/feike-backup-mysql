package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * feike对象 h_wholesale_performance_currently
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class WholesalePerformanceCurrently extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String creditType;
    private String limitType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long creditLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long activatedLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date expiredDate;
    private String expiredDates;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long os;
    private String activeOS;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setCreditType(String creditType) 
    {
        this.creditType = creditType;
    }

    public String getCreditType() 
    {
        return creditType;
    }
    public void setCreditLimit(Long creditLimit) 
    {
        this.creditLimit = creditLimit;
    }

    public Long getCreditLimit() 
    {
        return creditLimit;
    }
    public void setActivatedLimit(Long activatedLimit) 
    {
        this.activatedLimit = activatedLimit;
    }

    public Long getActivatedLimit() 
    {
        return activatedLimit;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getExpiredDates() {
        return expiredDates;
    }

    public void setExpiredDates(String expiredDates) {
        this.expiredDates = expiredDates;
    }

    public void setOs(Long os)
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public String getActiveOS() {
        return activeOS;
    }

    public void setActiveOS(String activeOS) {
        this.activeOS = activeOS;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    @Override
    public String toString() {
        return "WholesalePerformanceCurrently{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", creditType='" + creditType + '\'' +
                ", limitType='" + limitType + '\'' +
                ", creditLimit=" + creditLimit +
                ", activatedLimit=" + activatedLimit +
                ", expiredDate=" + expiredDate +
                ", expiredDates='" + expiredDates + '\'' +
                ", os=" + os +
                ", activeOS='" + activeOS + '\'' +
                ", instanceId='" + instanceId + '\'' +
                '}';
    }
}
