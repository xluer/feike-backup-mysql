package com.ruoyi.feike.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_credit_condition
 * 
 * @author ybw
 * @date 2022-07-12
 */
public class CreditCondition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    private String sector;

    /** 授信类型 */
    @Excel(name = "授信类型")
    private String limittype;

    /** 过期时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expireddate;

    /** $column.columnComment */
    @Excel(name = "过期时间")
    private String reviewtype;

    /** $column.columnComment */
    @Excel(name = "过期时间")
    private String loantenor;

    /** $column.columnComment */
    @Excel(name = "过期时间")
    private String buyback;

    /** $column.columnComment */
    @Excel(name = "过期时间")
    private String instanceid;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerid(String dealerid) 
    {
        this.dealerid = dealerid;
    }

    public String getDealerid() 
    {
        return dealerid;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setLimittype(String limittype) 
    {
        this.limittype = limittype;
    }

    public String getLimittype() 
    {
        return limittype;
    }
    public void setExpireddate(Date expireddate) 
    {
        this.expireddate = expireddate;
    }

    public Date getExpireddate() 
    {
        return expireddate;
    }
    public void setReviewtype(String reviewtype) 
    {
        this.reviewtype = reviewtype;
    }

    public String getReviewtype() 
    {
        return reviewtype;
    }
    public void setLoantenor(String loantenor) 
    {
        this.loantenor = loantenor;
    }

    public String getLoantenor() 
    {
        return loantenor;
    }
    public void setBuyback(String buyback) 
    {
        this.buyback = buyback;
    }

    public String getBuyback() 
    {
        return buyback;
    }
    public void setInstanceid(String instanceid) 
    {
        this.instanceid = instanceid;
    }

    public String getInstanceid() 
    {
        return instanceid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerid", getDealerid())
            .append("dealername", getDealername())
            .append("sector", getSector())
            .append("limittype", getLimittype())
            .append("expireddate", getExpireddate())
            .append("reviewtype", getReviewtype())
            .append("loantenor", getLoantenor())
            .append("buyback", getBuyback())
            .append("instanceid", getInstanceid())
            .toString();
    }
}
