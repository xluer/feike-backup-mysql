package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 合同对象 h_dealer_sector_contractgroup
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public class DealerSectorContractgroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    private String sector;

    /** 合同名称 */
    @Excel(name = "合同名称")
    private String contractname;

    /** 合同号 */
    @Excel(name = "合同号")
    private String contractnumber;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerid(String dealerid) 
    {
        this.dealerid = dealerid;
    }

    public String getDealerid() 
    {
        return dealerid;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setContractname(String contractname) 
    {
        this.contractname = contractname;
    }

    public String getContractname() 
    {
        return contractname;
    }
    public void setContractnumber(String contractnumber) 
    {
        this.contractnumber = contractnumber;
    }

    public String getContractnumber() 
    {
        return contractnumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerid", getDealerid())
            .append("dealername", getDealername())
            .append("sector", getSector())
            .append("contractname", getContractname())
            .append("contractnumber", getContractnumber())
            .toString();
    }
}
