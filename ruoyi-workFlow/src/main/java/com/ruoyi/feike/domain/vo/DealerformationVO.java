package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * feike对象 h_basic_information
 *
 * @author zmh
 * @date 2022-07-05
 */
public class DealerformationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String groupNameEN;
    private String groupNameCN;

    private String dealerNameEN;
    private String dealerNameCN;
    private String dealerValue;

    private List sector;
    private List dealerCodeFromWFS;
    private List dealerCodeFromManufacturer;


    private String registeredAddressCN;
    private String registeredAddressEN;
    private String corporateGuarantor;
    private String individualGuarantor;
    private String shareholderStructure;
    private String newdealername;

    private String incorporationDate;
    private Long registeredCapital;
    private String joinSectorDealerNetworkDate;
    private Long paidupCapital;

    private String legalRepresentativeCN;
    private String legalRepresentativeEN;

    private String gm;
    private Integer relatedWorkingExperience;
    private List shareholders;
    private List share;

    private String priorityId;
    private String province;
    private String instanceId;
    private Long priority;

    public String getNewdealername() {
        return newdealername;
    }

    public void setNewdealername(String newdealername) {
        this.newdealername = newdealername;
    }

    public String getShareholderStructure() {
        return shareholderStructure;
    }

    public void setShareholderStructure(String shareholderStructure) {
        this.shareholderStructure = shareholderStructure;
    }

    public String getCorporateGuarantor() {
        return corporateGuarantor;
    }

    public void setCorporateGuarantor(String corporateGuarantor) {
        this.corporateGuarantor = corporateGuarantor;
    }

    public String getIndividualGuarantor() {
        return individualGuarantor;
    }

    public void setIndividualGuarantor(String individualGuarantor) {
        this.individualGuarantor = individualGuarantor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupNameEN() {
        return groupNameEN;
    }

    public void setGroupNameEN(String groupNameEN) {
        this.groupNameEN = groupNameEN;
    }

    public String getGroupNameCN() {
        return groupNameCN;
    }

    public void setGroupNameCN(String groupNameCN) {
        this.groupNameCN = groupNameCN;
    }

    public String getDealerNameEN() {
        return dealerNameEN;
    }

    public void setDealerNameEN(String dealerNameEN) {
        this.dealerNameEN = dealerNameEN;
    }

    public String getDealerNameCN() {
        return dealerNameCN;
    }

    public void setDealerNameCN(String dealerNameCN) {
        this.dealerNameCN = dealerNameCN;
    }

    public String getDealerValue() {
        return dealerValue;
    }

    public void setDealerValue(String dealerValue) {
        this.dealerValue = dealerValue;
    }

    public List getSector() {
        return sector;
    }

    public void setSector(List sector) {
        this.sector = sector;
    }

    public List getDealerCodeFromWFS() {
        return dealerCodeFromWFS;
    }

    public void setDealerCodeFromWFS(List dealerCodeFromWFS) {
        this.dealerCodeFromWFS = dealerCodeFromWFS;
    }

    public List getDealerCodeFromManufacturer() {
        return dealerCodeFromManufacturer;
    }

    public void setDealerCodeFromManufacturer(List dealerCodeFromManufacturer) {
        this.dealerCodeFromManufacturer = dealerCodeFromManufacturer;
    }

    public String getRegisteredAddressCN() {
        return registeredAddressCN;
    }

    public void setRegisteredAddressCN(String registeredAddressCN) {
        this.registeredAddressCN = registeredAddressCN;
    }

    public String getRegisteredAddressEN() {
        return registeredAddressEN;
    }

    public void setRegisteredAddressEN(String registeredAddressEN) {
        this.registeredAddressEN = registeredAddressEN;
    }

    public String getIncorporationDate() {
        return incorporationDate;
    }

    public void setIncorporationDate(String incorporationDate) {
        this.incorporationDate = incorporationDate;
    }

    public Long getRegisteredCapital() {
        return registeredCapital;
    }

    public void setRegisteredCapital(Long registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    public String getJoinSectorDealerNetworkDate() {
        return joinSectorDealerNetworkDate;
    }

    public void setJoinSectorDealerNetworkDate(String joinSectorDealerNetworkDate) {
        this.joinSectorDealerNetworkDate = joinSectorDealerNetworkDate;
    }

    public Long getPaidupCapital() {
        return paidupCapital;
    }

    public void setPaidupCapital(Long paidupCapital) {
        this.paidupCapital = paidupCapital;
    }

    public String getLegalRepresentativeCN() {
        return legalRepresentativeCN;
    }

    public void setLegalRepresentativeCN(String legalRepresentativeCN) {
        this.legalRepresentativeCN = legalRepresentativeCN;
    }

    public String getLegalRepresentativeEN() {
        return legalRepresentativeEN;
    }

    public void setLegalRepresentativeEN(String legalRepresentativeEN) {
        this.legalRepresentativeEN = legalRepresentativeEN;
    }

    public String getGm() {
        return gm;
    }

    public void setGm(String gm) {
        this.gm = gm;
    }

    public Integer getRelatedWorkingExperience() {
        return relatedWorkingExperience;
    }

    public void setRelatedWorkingExperience(Integer relatedWorkingExperience) {
        this.relatedWorkingExperience = relatedWorkingExperience;
    }

    public List getShareholders() {
        return shareholders;
    }

    public void setShareholders(List shareholders) {
        this.shareholders = shareholders;
    }

    public List getShare() {
        return share;
    }

    public void setShare(List share) {
        this.share = share;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "DealerformationVO{" +
                "id='" + id + '\'' +
                ", groupNameEN='" + groupNameEN + '\'' +
                ", groupNameCN='" + groupNameCN + '\'' +
                ", dealerNameEN='" + dealerNameEN + '\'' +
                ", dealerNameCN='" + dealerNameCN + '\'' +
                ", dealerValue='" + dealerValue + '\'' +
                ", sector=" + sector +
                ", dealerCodeFromWFS=" + dealerCodeFromWFS +
                ", dealerCodeFromManufacturer=" + dealerCodeFromManufacturer +
                ", registeredAddressCN='" + registeredAddressCN + '\'' +
                ", registeredAddressEN='" + registeredAddressEN + '\'' +
                ", corporateGuarantor='" + corporateGuarantor + '\'' +
                ", individualGuarantor='" + individualGuarantor + '\'' +
                ", shareholderStructure='" + shareholderStructure + '\'' +
                ", newdealername='" + newdealername + '\'' +
                ", incorporationDate='" + incorporationDate + '\'' +
                ", registeredCapital=" + registeredCapital +
                ", joinSectorDealerNetworkDate='" + joinSectorDealerNetworkDate + '\'' +
                ", paidupCapital=" + paidupCapital +
                ", legalRepresentativeCN='" + legalRepresentativeCN + '\'' +
                ", legalRepresentativeEN='" + legalRepresentativeEN + '\'' +
                ", gm='" + gm + '\'' +
                ", relatedWorkingExperience=" + relatedWorkingExperience +
                ", shareholders=" + shareholders +
                ", share=" + share +
                ", priorityId='" + priorityId + '\'' +
                ", province='" + province + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", priority=" + priority +
                '}';
    }
}
