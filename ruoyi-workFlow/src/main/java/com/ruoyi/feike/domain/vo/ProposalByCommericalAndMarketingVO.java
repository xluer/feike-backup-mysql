package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * @author 神
 * @date 2022年07月10日 19:51
 */
public class ProposalByCommericalAndMarketingVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String id;
    private String dealername;
    private String sector;
    private String limitType;
    private String dealerCode;
    private Long os;
    private Long approvedLimit;
    private Long proposalLimit;
    private BigDecimal approvedCashDeposit;
    private BigDecimal proposalCashDeposit;
    private String activeLimit;
    private String instanceId;
    private String index;
    private String thisYear;
    private String lastYear;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public Long getOs() {
        return os;
    }

    public void setOs(Long os) {
        this.os = os;
    }

    public Long getApprovedLimit() {
        return approvedLimit;
    }

    public void setApprovedLimit(Long approvedLimit) {
        this.approvedLimit = approvedLimit;
    }

    public Long getProposalLimit() {
        return proposalLimit;
    }

    public void setProposalLimit(Long proposalLimit) {
        this.proposalLimit = proposalLimit;
    }

    public BigDecimal getApprovedCashDeposit() {
        return approvedCashDeposit;
    }

    public void setApprovedCashDeposit(BigDecimal approvedCashDeposit) {
        this.approvedCashDeposit = approvedCashDeposit;
    }

    public BigDecimal getProposalCashDeposit() {
        return proposalCashDeposit;
    }

    public void setProposalCashDeposit(BigDecimal proposalCashDeposit) {
        this.proposalCashDeposit = proposalCashDeposit;
    }

    public String getActiveLimit() {
        return activeLimit;
    }

    public void setActiveLimit(String activeLimit) {
        this.activeLimit = activeLimit;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getThisYear() {
        return thisYear;
    }

    public void setThisYear(String thisYear) {
        this.thisYear = thisYear;
    }

    public String getLastYear() {
        return lastYear;
    }

    public void setLastYear(String lastYear) {
        this.lastYear = lastYear;
    }

    @Override
    public String toString() {
        return "ProposalByCommericalAndMarketingVO{" +
                "id='" + id + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", limitType='" + limitType + '\'' +
                ", os=" + os +
                ", approvedLimit=" + approvedLimit +
                ", proposalLimit=" + proposalLimit +
                ", approvedCashDeposit=" + approvedCashDeposit +
                ", proposalCashDeposit=" + proposalCashDeposit +
                ", activeLimit='" + activeLimit + '\'' +
                ", instanceId='" + instanceId + '\'' +
                ", index='" + index + '\'' +
                ", thisYear='" + thisYear + '\'' +
                ", lastYear='" + lastYear + '\'' +
                '}';
    }
}
