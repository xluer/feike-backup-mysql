package com.ruoyi.feike.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * feike对象 h_parking_location
 *
 * @author zmh
 * @date 2022-07-22
 */
public class ParkingLocationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealernamecnid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealernamecn;

    /** 经销商品牌代码 */
    @Excel(name = "经销商品牌代码")
    private String dealercode;

    /** 网点地址 */
    @Excel(name = "网点地址")
    private String parkinglocation;

    /** 网点性质 */
    @Excel(name = "网点性质")
    private String type;

    /** 起始日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起始日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date effectivedate;

    /** 到期日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date duedate;

    /** 车架号 */
    @Excel(name = "车架号")
    private String vin;

    /** 与本部距离 */
    @Excel(name = "与本部距离")
    private String distancefrom4s;

    /** 联系人电话 */
    @Excel(name = "联系人电话")
    private String tel;

    /** $column.columnComment */
//    @Excel(name = "联系人电话")
    private String instanceId;

    /** 当前网点状态 */
    @Excel(name = "当前网点状态",readConverterExp = "0=启用,1=禁用,2=失效")
    private String status;

    /** 已支付二网保证金 */
    @Excel(name = "已支付二网保证金")
    private BigDecimal twondtierdeposit;

    private String enName;

    private String groupName;

    private Long permitted2ndtierwithdesposit;

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getPermitted2ndtierwithdesposit() {
        return permitted2ndtierwithdesposit;
    }

    public void setPermitted2ndtierwithdesposit(Long permitted2ndtierwithdesposit) {
        this.permitted2ndtierwithdesposit = permitted2ndtierwithdesposit;
    }



    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealernamecnid(String dealernamecnid)
    {
        this.dealernamecnid = dealernamecnid;
    }

    public String getDealernamecnid()
    {
        return dealernamecnid;
    }
    public void setDealernamecn(String dealernamecn)
    {
        this.dealernamecn = dealernamecn;
    }

    public String getDealernamecn()
    {
        return dealernamecn;
    }
    public void setDealercode(String dealercode)
    {
        this.dealercode = dealercode;
    }

    public String getDealercode()
    {
        return dealercode;
    }
    public void setParkinglocation(String parkinglocation)
    {
        this.parkinglocation = parkinglocation;
    }

    public String getParkinglocation()
    {
        return parkinglocation;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setEffectivedate(Date effectivedate)
    {
        this.effectivedate = effectivedate;
    }

    public Date getEffectivedate()
    {
        return effectivedate;
    }
    public void setDuedate(Date duedate)
    {
        this.duedate = duedate;
    }

    public Date getDuedate()
    {
        return duedate;
    }
    public void setVin(String vin)
    {
        this.vin = vin;
    }

    public String getVin()
    {
        return vin;
    }
    public void setDistancefrom4s(String distancefrom4s)
    {
        this.distancefrom4s = distancefrom4s;
    }

    public String getDistancefrom4s()
    {
        return distancefrom4s;
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getTel()
    {
        return tel;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setTwondtierdeposit(BigDecimal twondtierdeposit)
    {
        this.twondtierdeposit = twondtierdeposit;
    }

    public BigDecimal getTwondtierdeposit()
    {
        return twondtierdeposit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("dealernamecnid", getDealernamecnid())
                .append("dealernamecn", getDealernamecn())
                .append("dealercode", getDealercode())
                .append("parkinglocation", getParkinglocation())
                .append("type", getType())
                .append("effectivedate", getEffectivedate())
                .append("duedate", getDuedate())
                .append("vin", getVin())
                .append("distancefrom4s", getDistancefrom4s())
                .append("tel", getTel())
                .append("instanceId", getInstanceId())
                .append("status", getStatus())
                .append("twondtierdeposit", getTwondtierdeposit())
                .append("permitted2ndtierwithdesposit", getPermitted2ndtierwithdesposit())
                .append("enName", getEnName())
                .append("groupName", getGroupName())
                .toString();
    }
}
