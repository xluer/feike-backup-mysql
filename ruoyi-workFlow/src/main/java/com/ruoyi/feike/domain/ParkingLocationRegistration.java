package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * feike对象 h_parking_location_registration
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public class ParkingLocationRegistration extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 集团英文名称 */
    @Excel(name = "集团英文名称")
    private String groupNameEn;

    /** 集团中文名称 */
    @Excel(name = "集团中文名称")
    private String groupNameCn;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealerName;

    /** 经销商集团名称 */
    @Excel(name = "经销商集团名称")
    private String groupAme;

    /** 经销商品牌代码 */
    @Excel(name = "经销商品牌代码")
    private String dealercode;
    private List dealercCodes;

    /** 省 */
    @Excel(name = "省")
    private String province;
    private String provinceCN;

    /** 市 */
    @Excel(name = "市")
    private String city;

    /** 县 */
    @Excel(name = "县")
    private String county;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String parkinglocation;
    private String[] parkinglocations;

    /** 停车地点模式：4S，仓库，二网，同集团，车展; */
    @Excel(name = "停车地点模式：4S，仓库，二网，同集团，车展;")
    private String type;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 车架号 */
    @Excel(name = "车架号")
    private String vin;
    private String[] vins;

    /** 与4S店距离：100Km； */
    @Excel(name = "与4S店距离：100Km；")
    private String distanceFrom4s;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String tel;

    /** 注释 */
    @Excel(name = "注释")
    private String notes;

    /** 已支付二网保证金 */
    @Excel(name = "已支付二网保证金")
    private BigDecimal twondtierdeposit;

    /** 上传资料 */
    @Excel(name = "上传资料")
    private List<Fileupload> uploadFile;

    /** 实例id */
    @Excel(name = "实例id")
    private String instanceId;

    private List<Fileupload> parkingLocationRegistrationFileList;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setGroupNameEn(String groupNameEn) 
    {
        this.groupNameEn = groupNameEn;
    }

    public String getGroupNameEn() 
    {
        return groupNameEn;
    }
    public void setGroupNameCn(String groupNameCn) 
    {
        this.groupNameCn = groupNameCn;
    }

    public String getGroupNameCn() 
    {
        return groupNameCn;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setGroupAme(String groupAme) 
    {
        this.groupAme = groupAme;
    }

    public String getGroupAme() 
    {
        return groupAme;
    }
    public void setProvince(String province) 
    {
        this.province = province;
    }

    public String getProvince() 
    {
        return province;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setCounty(String county) 
    {
        this.county = county;
    }

    public String getCounty() 
    {
        return county;
    }

    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setStartDate(Date startDate) 
    {
        this.startDate = startDate;
    }

    public Date getStartDate() 
    {
        return startDate;
    }
    public void setEndDate(Date endDate) 
    {
        this.endDate = endDate;
    }

    public Date getEndDate() 
    {
        return endDate;
    }
    public void setVin(String vin) 
    {
        this.vin = vin;
    }

    public String getVin() 
    {
        return vin;
    }
    public void setDistanceFrom4s(String distanceFrom4s) 
    {
        this.distanceFrom4s = distanceFrom4s;
    }

    public String getDistanceFrom4s() 
    {
        return distanceFrom4s;
    }
    public void setTel(String tel) 
    {
        this.tel = tel;
    }

    public String getTel() 
    {
        return tel;
    }
    public void setNotes(String notes) 
    {
        this.notes = notes;
    }

    public String getNotes() 
    {
        return notes;
    }

    public List<Fileupload> getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(List<Fileupload> uploadFile) {
        this.uploadFile = uploadFile;
    }

    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    public List<Fileupload> getParkingLocationRegistrationFileList() {
        return parkingLocationRegistrationFileList;
    }

    public void setParkingLocationRegistrationFileList(List<Fileupload> parkingLocationRegistrationFileList) {
        this.parkingLocationRegistrationFileList = parkingLocationRegistrationFileList;
    }

    public String getDealercode() {
        return dealercode;
    }

    public void setDealercode(String dealercode) {
        this.dealercode = dealercode;
    }

    public List getDealercCodes() {
        return dealercCodes;
    }

    public void setDealercCodes(List dealercCodes) {
        this.dealercCodes = dealercCodes;
    }

    public String getParkinglocation() {
        return parkinglocation;
    }

    public void setParkinglocation(String parkinglocation) {
        this.parkinglocation = parkinglocation;
    }

    public String[] getParkinglocations() {
        return parkinglocations;
    }

    public void setParkinglocations(String[] parkinglocations) {
        this.parkinglocations = parkinglocations;
    }

    public BigDecimal getTwondtierdeposit() {
        return twondtierdeposit;
    }

    public void setTwondtierdeposit(BigDecimal twondtierdeposit) {
        this.twondtierdeposit = twondtierdeposit;
    }

    public String[] getVins() {
        return vins;
    }

    public void setVins(String[] vins) {
        this.vins = vins;
    }

    public String getProvinceCN() {
        return provinceCN;
    }

    public void setProvinceCN(String provinceCN) {
        this.provinceCN = provinceCN;
    }

    @Override
    public String toString() {
        return "ParkingLocationRegistration{" +
                "id='" + id + '\'' +
                ", groupNameEn='" + groupNameEn + '\'' +
                ", groupNameCn='" + groupNameCn + '\'' +
                ", dealerName='" + dealerName + '\'' +
                ", groupAme='" + groupAme + '\'' +
                ", dealercode='" + dealercode + '\'' +
                ", dealercCodes=" + dealercCodes +
                ", province='" + province + '\'' +
                ", provinceCN='" + provinceCN + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", parkinglocation='" + parkinglocation + '\'' +
                ", parkinglocations=" + Arrays.toString(parkinglocations) +
                ", type='" + type + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", vin='" + vin + '\'' +
                ", vins=" + Arrays.toString(vins) +
                ", distanceFrom4s='" + distanceFrom4s + '\'' +
                ", tel='" + tel + '\'' +
                ", notes='" + notes + '\'' +
                ", twondtierdeposit=" + twondtierdeposit +
                ", uploadFile=" + uploadFile +
                ", instanceId='" + instanceId + '\'' +
                ", parkingLocationRegistrationFileList=" + parkingLocationRegistrationFileList +
                '}';
    }
}
