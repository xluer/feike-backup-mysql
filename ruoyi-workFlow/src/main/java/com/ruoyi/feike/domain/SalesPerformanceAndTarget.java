package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;

/**
 * feike对象 h_sales_performance_and_target
 *
 * @author zmh
 * @date 2022-07-11
 */
public class SalesPerformanceAndTarget extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String item;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long actual2021;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long actualYTD2022;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long target2022;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String indexId;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerId(String dealerId)
    {
        this.dealerId = dealerId;
    }

    public String getDealerId()
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setItem(String item)
    {
        this.item = item;
    }

    public String getItem()
    {
        return item;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setActual2021(Long actual2021)
    {
        this.actual2021 = actual2021;
    }

    public Long getActual2021()
    {
        return actual2021;
    }
    public void setActualYTD2022(Long actualYTD2022)
    {
        this.actualYTD2022 = actualYTD2022;
    }

    public Long getActualYTD2022()
    {
        return actualYTD2022;
    }
    public void setTarget2022(Long target2022)
    {
        this.target2022 = target2022;
    }

    public Long getTarget2022()
    {
        return target2022;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIndexId() {
        return indexId;
    }

    public void setIndexId(String indexId) {
        this.indexId = indexId;
    }



    @Override
    public String toString() {
        return "SalesPerformanceAndTarget{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealerName='" + dealername + '\'' +
                ", item='" + item + '\'' +
                ", sector='" + sector + '\'' +
                ", actual2021=" + actual2021 +
                ", actualYTD2022=" + actualYTD2022 +
                ", target2022=" + target2022 +
                ", instanceId='" + instanceId + '\'' +
                ", indexId='" + indexId + '\'' +
                '}';
    }
}
