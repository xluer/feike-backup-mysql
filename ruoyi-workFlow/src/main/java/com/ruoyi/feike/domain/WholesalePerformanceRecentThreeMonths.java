package com.ruoyi.feike.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * feike对象 h_wholesale_performance_recent_three_months
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class WholesalePerformanceRecentThreeMonths extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealername;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String creditType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date month;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long os;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long dso;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long aging;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }

    public String getDealername() {
        return dealername;
    }

    public void setDealername(String dealername) {
        this.dealername = dealername;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setCreditType(String creditType) 
    {
        this.creditType = creditType;
    }

    public String getCreditType() 
    {
        return creditType;
    }
    public void setMonth(Date month) 
    {
        this.month = month;
    }

    public Date getMonth() 
    {
        return month;
    }
    public void setOs(Long os) 
    {
        this.os = os;
    }

    public Long getOs() 
    {
        return os;
    }
    public void setDso(Long dso) 
    {
        this.dso = dso;
    }

    public Long getDso() 
    {
        return dso;
    }
    public void setAging(Long aging) 
    {
        this.aging = aging;
    }

    public Long getAging() 
    {
        return aging;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }

    @Override
    public String toString() {
        return "WholesalePerformanceRecentThreeMonths{" +
                "id='" + id + '\'' +
                ", dealerId='" + dealerId + '\'' +
                ", dealername='" + dealername + '\'' +
                ", sector='" + sector + '\'' +
                ", creditType='" + creditType + '\'' +
                ", month=" + month +
                ", os=" + os +
                ", dso=" + dso +
                ", aging=" + aging +
                ", instanceId='" + instanceId + '\'' +
                '}';
    }
}
