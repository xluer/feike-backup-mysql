package com.ruoyi.feike.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * Flag对象 h_dealer_sector_limitflag
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public class DealerSectorLimitflag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 经销商ID */
    @Excel(name = "经销商ID")
    private String dealerid;

    /** 经销商名称 */
    @Excel(name = "经销商名称")
    private String dealername;

    /** 品牌 */
    @Excel(name = "品牌")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String normalflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String demolflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String tempflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String mrgflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String cbflag;

    /** $column.columnComment */
    @Excel(name = "品牌")
    private String threepartyflag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerid(String dealerid) 
    {
        this.dealerid = dealerid;
    }

    public String getDealerid() 
    {
        return dealerid;
    }
    public void setDealername(String dealername) 
    {
        this.dealername = dealername;
    }

    public String getDealername() 
    {
        return dealername;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setNormalflag(String normalflag) 
    {
        this.normalflag = normalflag;
    }

    public String getNormalflag() 
    {
        return normalflag;
    }
    public void setDemolflag(String demolflag) 
    {
        this.demolflag = demolflag;
    }

    public String getDemolflag() 
    {
        return demolflag;
    }
    public void setTempflag(String tempflag) 
    {
        this.tempflag = tempflag;
    }

    public String getTempflag() 
    {
        return tempflag;
    }
    public void setMrgflag(String mrgflag) 
    {
        this.mrgflag = mrgflag;
    }

    public String getMrgflag() 
    {
        return mrgflag;
    }
    public void setCbflag(String cbflag) 
    {
        this.cbflag = cbflag;
    }

    public String getCbflag() 
    {
        return cbflag;
    }
    public void setThreepartyflag(String threepartyflag) 
    {
        this.threepartyflag = threepartyflag;
    }

    public String getThreepartyflag() 
    {
        return threepartyflag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerid", getDealerid())
            .append("dealername", getDealername())
            .append("sector", getSector())
            .append("normalflag", getNormalflag())
            .append("demolflag", getDemolflag())
            .append("tempflag", getTempflag())
            .append("mrgflag", getMrgflag())
            .append("cbflag", getCbflag())
            .append("threepartyflag", getThreepartyflag())
            .toString();
    }
}
