package com.ruoyi.feike.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * feike对象 fileupload
 *
 * @author ruoyi
 * @date 2022-07-22
 */
public class Fileupload extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    private String instanceId;
    @Excel(name = "文件完整路径")
    private String fileName;
    @Excel(name = "浏览器可以访问的url路径")
    private String url;
    @Excel(name = "文件中文名加后缀")
    private String originalFileName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fileCreateTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "文件修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date fileEndTime;
    @Excel(name = "创建者名称")
    private String createName;
    @Excel(name = "类型")
    private String type;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getFileName()
    {
        return fileName;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }
    public void setOriginalFileName(String originalFileName)
    {
        this.originalFileName = originalFileName;
    }

    public String getOriginalFileName()
    {
        return originalFileName;
    }
    public void setFileCreateTime(Date fileCreateTime)
    {
        this.fileCreateTime = fileCreateTime;
    }

    public Date getFileCreateTime()
    {
        return fileCreateTime;
    }
    public void setFileEndTime(Date fileEndTime)
    {
        this.fileEndTime = fileEndTime;
    }

    public Date getFileEndTime()
    {
        return fileEndTime;
    }
    public void setCreateName(String createName)
    {
        this.createName = createName;
    }

    public String getCreateName()
    {
        return createName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("instanceId", getInstanceId())
            .append("fileName", getFileName())
            .append("url", getUrl())
            .append("originalFileName", getOriginalFileName())
            .append("fileCreateTime", getFileCreateTime())
            .append("fileEndTime", getFileEndTime())
            .append("createName", getCreateName())
            .append("createBy", getCreateBy())
            .toString();
    }
}
