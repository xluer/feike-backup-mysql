package com.ruoyi.feike.domain.vo;


import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;


/**
 * feike对象 h_basic_information
 *
 * @author zmh
 * @date 2022-07-05
 */

public class BasicInformationVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerNameEN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerNameCN;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerCodeFromWFS;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerCodeFromManufacturer;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String province;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date incorporationDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date joinSectorDealerNetworkDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long registeredCapital;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long paidupCapital;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String shareholders;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal share;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String gm;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer relatedWorkingExperience;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String notes;

    private String groupNameEN;
    private String groupNameCN;

    public String getGroupNameEN() {
        return groupNameEN;
    }

    public void setGroupNameEN(String groupNameEN) {
        this.groupNameEN = groupNameEN;
    }

    public String getGroupNameCN() {
        return groupNameCN;
    }

    public void setGroupNameCN(String groupNameCN) {
        this.groupNameCN = groupNameCN;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** 优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)； */
    @Excel(name = "优先级New application(4) &gt; Change guarantee conditions(3) &gt; Limit amount/ratio change(2) &gt; Limit expiry date extension(1)；")
    private Long priority;

    /** 触发新增修改后存主键ID */
    @Excel(name = "触发新增修改后存主键ID")
    private String priorityId;

    private String dealerId;

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setDealerNameEN(String dealerNameEN)
    {
        this.dealerNameEN = dealerNameEN;
    }

    public String getDealerNameEN()
    {
        return dealerNameEN;
    }
    public void setDealerNameCN(String dealerNameCN)
    {
        this.dealerNameCN = dealerNameCN;
    }

    public String getDealerNameCN()
    {
        return dealerNameCN;
    }
    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public String getSector()
    {
        return sector;
    }
    public void setDealerCodeFromWFS(String dealerCodeFromWFS)
    {
        this.dealerCodeFromWFS = dealerCodeFromWFS;
    }

    public String getDealerCodeFromWFS()
    {
        return dealerCodeFromWFS;
    }
    public void setDealerCodeFromManufacturer(String dealerCodeFromManufacturer)
    {
        this.dealerCodeFromManufacturer = dealerCodeFromManufacturer;
    }

    public String getDealerCodeFromManufacturer()
    {
        return dealerCodeFromManufacturer;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setIncorporationDate(Date incorporationDate)
    {
        this.incorporationDate = incorporationDate;
    }

    public Date getIncorporationDate()
    {
        return incorporationDate;
    }
    public void setJoinSectorDealerNetworkDate(Date joinSectorDealerNetworkDate)
    {
        this.joinSectorDealerNetworkDate = joinSectorDealerNetworkDate;
    }

    public Date getJoinSectorDealerNetworkDate()
    {
        return joinSectorDealerNetworkDate;
    }
    public void setRegisteredCapital(Long registeredCapital)
    {
        this.registeredCapital = registeredCapital;
    }

    public Long getRegisteredCapital()
    {
        return registeredCapital;
    }
    public void setPaidupCapital(Long paidupCapital)
    {
        this.paidupCapital = paidupCapital;
    }

    public Long getPaidupCapital()
    {
        return paidupCapital;
    }
    public void setShareholders(String shareholders)
    {
        this.shareholders = shareholders;
    }

    public String getShareholders()
    {
        return shareholders;
    }
    public void setShare(BigDecimal share)
    {
        this.share = share;
    }

    public BigDecimal getShare()
    {
        return share;
    }
    public void setGm(String gm)
    {
        this.gm = gm;
    }

    public String getGm()
    {
        return gm;
    }
    public void setRelatedWorkingExperience(Integer relatedWorkingExperience)
    {
        this.relatedWorkingExperience = relatedWorkingExperience;
    }

    public Integer getRelatedWorkingExperience()
    {
        return relatedWorkingExperience;
    }
    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId()
    {
        return instanceId;

    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("dealerNameEN", getDealerNameEN())
                .append("dealerNameCN", getDealerNameCN())
                .append("sector", getSector())
                .append("dealerCodeFromWFS", getDealerCodeFromWFS())
                .append("dealerCodeFromManufacturer", getDealerCodeFromManufacturer())
                .append("province", getProvince())
                .append("incorporationDate", getIncorporationDate())
                .append("joinSectorDealerNetworkDate", getJoinSectorDealerNetworkDate())
                .append("registeredCapital", getRegisteredCapital())
                .append("paidupCapital", getPaidupCapital())
                .append("shareholders", getShareholders())
                .append("share", getShare())
                .append("gm", getGm())
                .append("relatedWorkingExperience", getRelatedWorkingExperience())
                .append("instanceId", getInstanceId())
                .append("priority", getPriority())
                .append("priorityId", getPriorityId())
                .append("dealerId", getDealerId())
                .append("groupNameEN", getGroupNameEN())
                .append("groupNameCN", getGroupNameCN())
                .toString();

    }
}
