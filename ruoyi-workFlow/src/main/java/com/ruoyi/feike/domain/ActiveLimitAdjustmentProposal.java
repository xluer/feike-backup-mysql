package com.ruoyi.feike.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * feike对象 h_active_limit_adjustment_proposal
 * 
 * @author zmh
 * @date 2022-07-05
 */
public class ActiveLimitAdjustmentProposal extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String instanceId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dealerName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sector;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long currentPermanentLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long currentActiveLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private BigDecimal currentCashDeposit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long currentOs;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long proposedActiveLimit;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long activeLimitChange;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long amountOfMargin;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDealerId(String dealerId) 
    {
        this.dealerId = dealerId;
    }

    public String getDealerId() 
    {
        return dealerId;
    }
    public void setInstanceId(String instanceId) 
    {
        this.instanceId = instanceId;
    }

    public String getInstanceId() 
    {
        return instanceId;
    }
    public void setDealerName(String dealerName) 
    {
        this.dealerName = dealerName;
    }

    public String getDealerName() 
    {
        return dealerName;
    }
    public void setSector(String sector) 
    {
        this.sector = sector;
    }

    public String getSector() 
    {
        return sector;
    }
    public void setCurrentPermanentLimit(Long currentPermanentLimit) 
    {
        this.currentPermanentLimit = currentPermanentLimit;
    }

    public Long getCurrentPermanentLimit() 
    {
        return currentPermanentLimit;
    }
    public void setCurrentActiveLimit(Long currentActiveLimit) 
    {
        this.currentActiveLimit = currentActiveLimit;
    }

    public Long getCurrentActiveLimit() 
    {
        return currentActiveLimit;
    }
    public void setCurrentCashDeposit(BigDecimal currentCashDeposit) 
    {
        this.currentCashDeposit = currentCashDeposit;
    }

    public BigDecimal getCurrentCashDeposit() 
    {
        return currentCashDeposit;
    }
    public void setCurrentOs(Long currentOs) 
    {
        this.currentOs = currentOs;
    }

    public Long getCurrentOs() 
    {
        return currentOs;
    }
    public void setProposedActiveLimit(Long proposedActiveLimit) 
    {
        this.proposedActiveLimit = proposedActiveLimit;
    }

    public Long getProposedActiveLimit() 
    {
        return proposedActiveLimit;
    }
    public void setActiveLimitChange(Long activeLimitChange) 
    {
        this.activeLimitChange = activeLimitChange;
    }

    public Long getActiveLimitChange() 
    {
        return activeLimitChange;
    }
    public void setAmountOfMargin(Long amountOfMargin) 
    {
        this.amountOfMargin = amountOfMargin;
    }

    public Long getAmountOfMargin() 
    {
        return amountOfMargin;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dealerId", getDealerId())
            .append("instanceId", getInstanceId())
            .append("dealerName", getDealerName())
            .append("sector", getSector())
            .append("currentPermanentLimit", getCurrentPermanentLimit())
            .append("currentActiveLimit", getCurrentActiveLimit())
            .append("currentCashDeposit", getCurrentCashDeposit())
            .append("currentOs", getCurrentOs())
            .append("proposedActiveLimit", getProposedActiveLimit())
            .append("activeLimitChange", getActiveLimitChange())
            .append("amountOfMargin", getAmountOfMargin())
            .toString();
    }
}
