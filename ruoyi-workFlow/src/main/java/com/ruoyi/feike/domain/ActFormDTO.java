package com.ruoyi.feike.domain;

import lombok.*;

import java.io.Serializable;

/**
 * @author lss
 * @version 1.0
 * @description: 流程审批意见DTO
 * @date 2022/8/5 11:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ActFormDTO  implements Serializable {
    private static final long serialVersionUID = 1L;


    private String controlId;

    private String controlType;

    private String controlLable;

    private String controlIsParam;

    private String controlValue;

    private String controlDefault;
}
