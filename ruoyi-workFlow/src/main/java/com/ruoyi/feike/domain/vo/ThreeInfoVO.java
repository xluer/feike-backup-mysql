package com.ruoyi.feike.domain.vo;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;
import java.util.Map;

/**
 * @author 神
 * @date 2022年08月09日 16:58
 */
public class ThreeInfoVO extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String DealerCode;
    private List<Map<String,Object>> CreditInfo;

    public String getDealerCode() {
        return DealerCode;
    }

    public void setDealerCode(String dealerCode) {
        DealerCode = dealerCode;
    }

    public List<Map<String, Object>> getCreditInfo() {
        return CreditInfo;
    }

    public void setCreditInfo(List<Map<String, Object>> creditInfo) {
        CreditInfo = creditInfo;
    }

    @Override
    public String toString() {
        return "ThreeInfoVO{" +
                "DealerCode='" + DealerCode + '\'' +
                ", CreditInfo=" + CreditInfo +
                '}';
    }
}
