package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.TemporaryConditionFollowUp;
import com.ruoyi.feike.service.ITemporaryConditionFollowUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-12
 */
@RestController
@RequestMapping("/feike/up")
public class TemporaryConditionFollowUpController extends BaseController
{
    @Autowired
    private ITemporaryConditionFollowUpService temporaryConditionFollowUpService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:up:list')")
    @GetMapping("/list")
    public TableDataInfo list(TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        startPage();
        List<TemporaryConditionFollowUp> list = temporaryConditionFollowUpService.selectTemporaryConditionFollowUpList(temporaryConditionFollowUp);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:up:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        List<TemporaryConditionFollowUp> list = temporaryConditionFollowUpService.selectTemporaryConditionFollowUpList(temporaryConditionFollowUp);
        ExcelUtil<TemporaryConditionFollowUp> util = new ExcelUtil<TemporaryConditionFollowUp>(TemporaryConditionFollowUp.class);
        return util.exportExcel(list, "up");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:up:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(temporaryConditionFollowUpService.selectTemporaryConditionFollowUpById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:up:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        return toAjax(temporaryConditionFollowUpService.insertTemporaryConditionFollowUp(temporaryConditionFollowUp));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:up:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        return toAjax(temporaryConditionFollowUpService.updateTemporaryConditionFollowUp(temporaryConditionFollowUp));
    }

    /**
     * 删除feike
     */
    // @PreAuthorize("@ss.hasPermi('feike:up:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(temporaryConditionFollowUpService.deleteTemporaryConditionFollowUpByIds(ids));
    }

    /**
     * 新增经销商明细表
     */
    @PreAuthorize("@ss.hasPermi('feike:up:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(@RequestBody Map<String, Object> resultMap) {
        try {
            return toAjax(temporaryConditionFollowUpService.saveTcfu(resultMap));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("新增失败,原因是:"+e.toString());
        }
    }
}
