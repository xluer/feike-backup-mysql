package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.ParkingLocationRegistration;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.service.IFileuploadService;
import com.ruoyi.feike.service.IParkingLocationRegistrationService;
import com.ruoyi.feike.service.IParkingLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * feikeController
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
@RestController
@RequestMapping("/feike/registration")
public class ParkingLocationRegistrationController extends BaseController
{
    @Autowired
    private IParkingLocationRegistrationService parkingLocationRegistrationService;

    @Autowired
    private IParkingLocationService parkingLocationService;

    @Autowired
    private IFileuploadService fileuploadService;

    /**
     * 查询feike列表
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:list')")
    @GetMapping("/list")
    public TableDataInfo list(ParkingLocationRegistration parkingLocationRegistration)
    {
        startPage();
        List<ParkingLocationRegistration> list = parkingLocationRegistrationService.selectParkingLocationRegistrationList(parkingLocationRegistration);
        return getDataTable(list);
    }

    /**
     * 查询feike列表
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:list')")
    @GetMapping("/listAll")
    public TableDataInfo listAll(AnnualReviewy annualReviewy, ParkingLocationRegistration parkingLocationRegistration)
    {
        startPage();
        List<AnnualReviewyHistoryVO> list = parkingLocationRegistrationService.selectParkingLocationRegistrationListAll(annualReviewy,parkingLocationRegistration);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ParkingLocationRegistration parkingLocationRegistration)
    {
        List<ParkingLocationRegistration> list = parkingLocationRegistrationService.selectParkingLocationRegistrationList(parkingLocationRegistration);
        ExcelUtil<ParkingLocationRegistration> util = new ExcelUtil<ParkingLocationRegistration>(ParkingLocationRegistration.class);
        return util.exportExcel(list, "registration");
    }

    /**
     * 获取feike详细信息
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(parkingLocationRegistrationService.selectParkingLocationRegistrationById(id));
    }

    /**
     * 获取feike详细信息
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:query')")
    @GetMapping(value = "/ByInstanceId/{instanceId}")
    public AjaxResult getInfoByInstanceId(@PathVariable("instanceId") String instanceId)
    {
        return AjaxResult.success(parkingLocationRegistrationService.selectParkingLocationRegistrationByInstanceId(instanceId));
    }

    /**
     * 查询 h_parking_location 列表信息
     */
    @GetMapping("/getList")
    public TableDataInfo getList()
    {
        startPage();
        List<Map> list = parkingLocationService.selectParkingLocationLists();
        return getDataTable(list);
    }

    /**
     *
     * @return
     */
    @PostMapping("/getFlagByDealerNameAndType")
    public AjaxResult getFlagByDealerNameAndType(@RequestBody Map map)
    {
        return AjaxResult.success(parkingLocationService.getFlagByDealerNameAndType(map));
    }

    /**
     * 根据group和dealer获取详细信息
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:query')")
    @PostMapping(value = "/getInfoGroupAndDealer")
    public AjaxResult getInfoByGroupIdAndDealerId(@RequestBody Map<String,Object> map)
    {
        return AjaxResult.success(parkingLocationRegistrationService.selectParkingLocationRegistration(map));
    }

    /**
     * 新增feike
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Map<String,Object> map)
    {
        return toAjax(parkingLocationRegistrationService.insertParkingLocationRegistrationMap(map));
    }

    /**
     * 修改feike
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ParkingLocationRegistration parkingLocationRegistration)
    {
        return toAjax(parkingLocationRegistrationService.updateParkingLocationRegistration(parkingLocationRegistration));
    }

    /**
     * 删除feike
     */
    // @PreAuthorize("@ss.hasPermi('feike:registration:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(parkingLocationRegistrationService.deleteParkingLocationRegistrationByIds(ids));
    }
}
