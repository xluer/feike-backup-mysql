package com.ruoyi.feike.controller;

import java.util.List;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.Plm;
import com.ruoyi.feike.service.IPlmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * plm导入表Controller
 *
 * @author ruoyi
 * @date 2022-07-21
 */
@RestController
@RequestMapping("/feike/plm")
public class PlmController extends BaseController
{
    @Autowired
    private IPlmService plmService;
    @Autowired
    private TokenService tokenService;

    /**
     * 查询plm导入表列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Plm plm)
    {
        startPage();
        List<Plm> list = plmService.selectPlmList(plm);
        return getDataTable(list);
    }

    /**
     * 导出plm导入表列表
     */
    @Log(title = "plm导入表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Plm plm)
    {
        List<Plm> list = plmService.selectPlmList(plm);
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        return util.exportExcel(list, "plm");
    }

    /**
     * 获取plm导入表详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(plmService.selectPlmById(id));
    }

    /**
     * 新增plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Plm plm)
    {
        return toAjax(plmService.insertPlm(plm));
    }

    /**
     * 修改plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Plm plm)
    {
        return toAjax(plmService.updatePlm(plm));
    }

    /**
     * 删除plm导入表
     */
    @Log(title = "plm导入表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(plmService.deletePlmByIds(ids));
    }

    @Log(title = "导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        List<Plm> userList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = plmService.importUser(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate()
    {
        ExcelUtil<Plm> util = new ExcelUtil<Plm>(Plm.class);
        return util.importTemplateExcel("用户数据");
    }
}
