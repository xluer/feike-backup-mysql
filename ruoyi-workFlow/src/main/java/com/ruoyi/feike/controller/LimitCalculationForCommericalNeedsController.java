package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.LimitCalculationForCommericalNeeds;
import com.ruoyi.feike.service.ILimitCalculationForCommericalNeedsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-11
 */
@RestController
@RequestMapping("/feike/needs")
public class LimitCalculationForCommericalNeedsController extends BaseController
{
    @Autowired
    private ILimitCalculationForCommericalNeedsService limitCalculationForCommericalNeedsService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:list')")
    @GetMapping("/list")
    public TableDataInfo list(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        startPage();
        List<LimitCalculationForCommericalNeeds> list = limitCalculationForCommericalNeedsService.selectLimitCalculationForCommericalNeedsList(limitCalculationForCommericalNeeds);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        List<LimitCalculationForCommericalNeeds> list = limitCalculationForCommericalNeedsService.selectLimitCalculationForCommericalNeedsList(limitCalculationForCommericalNeeds);
        ExcelUtil<LimitCalculationForCommericalNeeds> util = new ExcelUtil<LimitCalculationForCommericalNeeds>(LimitCalculationForCommericalNeeds.class);
        return util.exportExcel(list, "needs");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(limitCalculationForCommericalNeedsService.selectLimitCalculationForCommericalNeedsById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        return toAjax(limitCalculationForCommericalNeedsService.insertLimitCalculationForCommericalNeeds(limitCalculationForCommericalNeeds));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        return toAjax(limitCalculationForCommericalNeedsService.updateLimitCalculationForCommericalNeeds(limitCalculationForCommericalNeeds));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:needs:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(limitCalculationForCommericalNeedsService.deleteLimitCalculationForCommericalNeedsByIds(ids));
    }
}
