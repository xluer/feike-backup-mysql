package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DealerSectorContractgroup;
import com.ruoyi.feike.service.IDealerSectorContractgroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 合同Controller
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@RestController
@RequestMapping("/feike/contractgroup")
public class DealerSectorContractgroupController extends BaseController
{
    @Autowired
    private IDealerSectorContractgroupService dealerSectorContractgroupService;

    /**
     * 查询合同列表
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealerSectorContractgroup dealerSectorContractgroup)
    {
        startPage();
        List<DealerSectorContractgroup> list = dealerSectorContractgroupService.selectDealerSectorContractgroupList(dealerSectorContractgroup);
        return getDataTable(list);
    }

    /**
     * 导出合同列表
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:export')")
    @Log(title = "合同", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerSectorContractgroup dealerSectorContractgroup)
    {
        List<DealerSectorContractgroup> list = dealerSectorContractgroupService.selectDealerSectorContractgroupList(dealerSectorContractgroup);
        ExcelUtil<DealerSectorContractgroup> util = new ExcelUtil<DealerSectorContractgroup>(DealerSectorContractgroup.class);
        return util.exportExcel(list, "contractgroup");
    }

    /**
     * 获取合同详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dealerSectorContractgroupService.selectDealerSectorContractgroupById(id));
    }

    /**
     * 新增合同
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:add')")
    @Log(title = "合同", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerSectorContractgroup dealerSectorContractgroup)
    {
        return toAjax(dealerSectorContractgroupService.insertDealerSectorContractgroup(dealerSectorContractgroup));
    }

    /**
     * 修改合同
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:edit')")
    @Log(title = "合同", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerSectorContractgroup dealerSectorContractgroup)
    {
        return toAjax(dealerSectorContractgroupService.updateDealerSectorContractgroup(dealerSectorContractgroup));
    }

    /**
     * 删除合同
     */
    @PreAuthorize("@ss.hasPermi('feike:contractgroup:remove')")
    @Log(title = "合同", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dealerSectorContractgroupService.deleteDealerSectorContractgroupByIds(ids));
    }
}
