package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import com.ruoyi.feike.service.IDealerInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * feikeController
 *
 * @author ybw
 * @date 2022-07-12
 */
@RestController
@RequestMapping("/feike/informationMaintain")
public class DealerInformationController extends BaseController
{
    @Autowired
    private IDealerInformationService dealerInformationService;

    /**
     * 查询feike列表
     */
    @GetMapping("/list")
    public TableDataInfo list(DealerInformation dealerInformation)
    {
        startPage();
        List<DealerInformation> list = dealerInformationService.selectDealerInformationList(dealerInformation);
        return getDataTable(list);
    }

    /**
     * 查询根据dealer分组后的列表
     */
    @GetMapping("/listnew")
    public TableDataInfo listnew(DealerInformation dealerInformation)
    {
        startPage();
        List<DealerInformation> list = dealerInformationService.selectDealerInformationListnew(dealerInformation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerInformation dealerInformation)
    {
        List<DealerInformation> list = dealerInformationService.selectDealerInformationList(dealerInformation);
        ExcelUtil<DealerInformation> util = new ExcelUtil<DealerInformation>(DealerInformation.class);
        return util.exportExcel(list, "informationMaintain");
    }

    /**
     * 获取feike详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dealerInformationService.selectDealerInformationById(id));
    }

    /**
     * 根据dealername获取feike详细信息
     */
    @PostMapping(value = "/list")
    public AjaxResult list(@RequestBody Map<String,Object> map)
    {
        System.out.println("1111");

        DealerformationVO dealerformationVO = dealerInformationService.selectDealerInformationByname(map);
        return AjaxResult.success(dealerformationVO);
    }

    @PostMapping(value = "/listByName")
    public TableDataInfo listByName(@RequestBody List<DealerInformation> params)
    {
        List<DealerformationVO> list = dealerInformationService.listByName(params);
        return getDataTable(list);
    }

    /**
     * 新增feike
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerInformation dealerInformation)
    {
        return toAjax(dealerInformationService.insertDealerInformation(dealerInformation));
    }

    /**
     * 修改feike
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerformationVO dealerInformation)
    {
        return toAjax(dealerInformationService.updateDealerInformationnew(dealerInformation));
    }

    /**
     * 删除feike
     */
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dealerInformationService.deleteDealerInformationByIds(ids));
    }

    @GetMapping(value = "/getGroups")
    public AjaxResult getGroups()
    {
        return AjaxResult.success(dealerInformationService.getGroups());
    }

    @GetMapping(value = "/getDealers")
    public AjaxResult getDealers(String groupName)
    {
        return AjaxResult.success(dealerInformationService.getDealers(groupName));
    }

}
