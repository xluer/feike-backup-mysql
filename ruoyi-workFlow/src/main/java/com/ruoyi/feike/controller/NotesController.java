package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.Notes;
import com.ruoyi.feike.service.INotesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author lsn
 * @date 2022-07-06
 */
@RestController
@RequestMapping("/feike/notes")
public class NotesController extends BaseController
{
    @Autowired
    private INotesService notesService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:list')")
    @GetMapping("/list")
    public TableDataInfo list(Notes notes)
    {
        startPage();
        List<Notes> list = notesService.selectNotesList(notes);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Notes notes)
    {
        List<Notes> list = notesService.selectNotesList(notes);
        ExcelUtil<Notes> util = new ExcelUtil<Notes>(Notes.class);
        return util.exportExcel(list, "notes");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(notesService.selectNotesById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Notes notes)
    {
        return toAjax(notesService.insertNotes(notes));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Notes notes)
    {
        return toAjax(notesService.updateNotes(notes));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:notes:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(notesService.deleteNotesByIds(ids));
    }
}
