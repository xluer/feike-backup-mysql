package com.ruoyi.feike.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.EmailUtil;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;
import com.ruoyi.feike.service.IDealerNegativeInformationCheckService;
import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * feikeController
 *
 * @author zmh
 * @date 2022-07-12
 */
@RestController
@RequestMapping("/feike/check")
public class DealerNegativeInformationCheckController extends BaseController {
    @Autowired
    private IDealerNegativeInformationCheckService dealerNegativeInformationCheckService;

    @Value("${spring.mail.from}") // 从application.yml配置文件中获取
    private String from; //  发送发邮箱地址

    @Autowired
    private JavaMailSender mailSender;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:check:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealerNegativeInformationCheck dealerNegativeInformationCheck) {
        startPage();
        List<DealerNegativeInformationCheck> list = dealerNegativeInformationCheckService.selectDealerNegativeInformationCheckList(dealerNegativeInformationCheck);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:check:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerNegativeInformationCheck dealerNegativeInformationCheck) {
        List<DealerNegativeInformationCheck> list = dealerNegativeInformationCheckService.selectDealerNegativeInformationCheckList(dealerNegativeInformationCheck);
        ExcelUtil<DealerNegativeInformationCheck> util = new ExcelUtil<DealerNegativeInformationCheck>(DealerNegativeInformationCheck.class);
        return util.exportExcel(list, "check");
    }

    /**
     * 获取feike详细信息
     */
    // @PreAuthorize("@ss.hasPermi('feike:check:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(dealerNegativeInformationCheckService.selectDealerNegativeInformationCheckById(id));
    }

    /**
     * 根据id获取h_dealer_negative_information_check详细信息
     */
//    @PreAuthorize("@ss.hasPermi('feike:check:list')")
//    @GetMapping(value = "/listDnic")
//    @ResponseBody
//    public TableDataInfo listDnic(@RequestBody Map<String, Object> resultMap) {
//        startPage();
//        List<DealerNegativeInformationCheck> list = dealerNegativeInformationCheckService.selectDealerNegativeInformationCheckListId(resultMap);
//        return getDataTable(list);
//    }


    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:check:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerNegativeInformationCheck dealerNegativeInformationCheck) {
        return toAjax(dealerNegativeInformationCheckService.insertDealerNegativeInformationCheck(dealerNegativeInformationCheck));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:check:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerNegativeInformationCheck dealerNegativeInformationCheck) {
        return toAjax(dealerNegativeInformationCheckService.updateDealerNegativeInformationCheck(dealerNegativeInformationCheck));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:check:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(dealerNegativeInformationCheckService.deleteDealerNegativeInformationCheckByIds(ids));
    }

    /**
     * 新增经销商明细表
     */
    @PreAuthorize("@ss.hasPermi('feike:check:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(@RequestBody Map<String, Object> resultMap) {
        try {
            return toAjax(dealerNegativeInformationCheckService.saveDnic(resultMap));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("新增失败,原因是:"+e.toString());
        }
    }

    /**
     * 发送邮件
     */
    @PostMapping("/sendmail")
    @ResponseBody
    public AjaxResult sendmail(@RequestBody Map<String, Object> params) throws IOException {
        try {
            // 测试文本邮件发送（无附件）

            String to = params.get("to").toString();
            String title = params.get("title").toString();
            String content = params.get("content").toString();
//            String to = "1354720990@qq.com";
//            String title = "文本邮件发送测试";
//            String content = "文本邮件发送测试";

            //纯文本方式调用
            //new EmailUtil(from,mailSender).sendMessage(to, title, content);
            //带附件方式调用
            new EmailUtil(from,mailSender).sendMessageCarryFiles(to, title, content,null);

            return AjaxResult.success();
        }catch (Exception e){
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }

    }

    /**
     * 上传文件
     */
    @PostMapping("/uploadfiles")
    @ResponseBody
    public AjaxResult addSave(@RequestParam("file") MultipartFile file, @RequestParam("fileName") String fileName, FileInfo fileInfo) throws IOException
    {
        // 上传文件路径
        String filePath = RuoYiConfig.getUploadPath();
        // 上传并返回新文件名称
        String fileNewName = FileUploadUtils.upload(filePath, file);
        //fileInfo.setFilePath(fileName);
        HashMap<String, Object> map = new HashMap<>();
        map.put("fileNewName",fileNewName);
        return toAjax(1);
    }
}
