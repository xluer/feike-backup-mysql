package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DealerGroup;
import com.ruoyi.feike.service.IDealerGroupService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 *
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/group")
public class DealerGroupController extends BaseController
{
    @Autowired
    private IDealerGroupService dealerGroupService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:group:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealerGroup dealerGroup)
    {
        startPage();
        List<DealerGroup> list = dealerGroupService.selectDealerGroupList(dealerGroup);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:group:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerGroup dealerGroup)
    {
        List<DealerGroup> list = dealerGroupService.selectDealerGroupList(dealerGroup);
        ExcelUtil<DealerGroup> util = new ExcelUtil<DealerGroup>(DealerGroup.class);
        return util.exportExcel(list, "group");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:group:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dealerGroupService.selectDealerGroupById(id));
    }
    /**
     * 获取feike详细信息
     */
    // @PreAuthorize("@ss.hasPermi('feike:group:query')")
    @GetMapping(value = "/getByInstanceId/{instanceId}")
    public AjaxResult getInfoByInstanceId(@PathVariable("instanceId") String instanceId)
    {
        return AjaxResult.success(dealerGroupService.selectDealerGroupByByInstanceId(instanceId));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:group:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerGroup dealerGroup)
    {
        return toAjax(dealerGroupService.insertDealerGroup(dealerGroup));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:group:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerGroup dealerGroup)
    {
        return toAjax(dealerGroupService.updateDealerGroup(dealerGroup));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:group:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dealerGroupService.deleteDealerGroupByIds(ids));
    }
}
