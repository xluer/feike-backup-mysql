package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DealercodeCount;
import com.ruoyi.feike.service.IDealercodeCountService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * countController
 * 
 * @author ybw
 * @date 2022-08-09
 */
@RestController
@RequestMapping("/feike/count")
public class DealercodeCountController extends BaseController
{
    @Autowired
    private IDealercodeCountService dealercodeCountService;

    /**
     * 查询count列表
     */
    @PreAuthorize("@ss.hasPermi('feike:count:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealercodeCount dealercodeCount)
    {
        startPage();
        List<DealercodeCount> list = dealercodeCountService.selectDealercodeCountList(dealercodeCount);
        return getDataTable(list);
    }

    /**
     * 导出count列表
     */
    @PreAuthorize("@ss.hasPermi('feike:count:export')")
    @Log(title = "count", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealercodeCount dealercodeCount)
    {
        List<DealercodeCount> list = dealercodeCountService.selectDealercodeCountList(dealercodeCount);
        ExcelUtil<DealercodeCount> util = new ExcelUtil<DealercodeCount>(DealercodeCount.class);
        return util.exportExcel(list, "count");
    }

    /**
     * 获取count详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:count:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dealercodeCountService.selectDealercodeCountById(id));
    }

    /**
     * 新增count
     */
    @PreAuthorize("@ss.hasPermi('feike:count:add')")
    @Log(title = "count", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealercodeCount dealercodeCount)
    {
        return toAjax(dealercodeCountService.insertDealercodeCount(dealercodeCount));
    }

    /**
     * 修改count
     */
    @PreAuthorize("@ss.hasPermi('feike:count:edit')")
    @Log(title = "count", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealercodeCount dealercodeCount)
    {
        return toAjax(dealercodeCountService.updateDealercodeCount(dealercodeCount));
    }

    /**
     * 删除count
     */
    @PreAuthorize("@ss.hasPermi('feike:count:remove')")
    @Log(title = "count", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dealercodeCountService.deleteDealercodeCountByIds(ids));
    }
}
