package com.ruoyi.feike.controller;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.msg.ObjectRestResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.CreditCondition;
import com.ruoyi.feike.service.ICreditConditionService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author ybw
 * @date 2022-07-12
 */
@RestController
@RequestMapping("/feike/CreditCondition")
public class CreditConditionController extends BaseController
{
    @Autowired
    private ICreditConditionService creditConditionService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:list')")
    @GetMapping("/list")
    public TableDataInfo list(CreditCondition creditCondition)
    {
        startPage();
        List<CreditCondition> list = creditConditionService.selectCreditConditionList(creditCondition);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(CreditCondition creditCondition)
    {
        List<CreditCondition> list = creditConditionService.selectCreditConditionList(creditCondition);
        ExcelUtil<CreditCondition> util = new ExcelUtil<CreditCondition>(CreditCondition.class);
        return util.exportExcel(list, "resource");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(creditConditionService.selectCreditConditionById(id));
    }
    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CreditCondition creditCondition)
    {
        return toAjax(creditConditionService.insertCreditCondition(creditCondition));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CreditCondition creditCondition)
    {
        return toAjax(creditConditionService.updateCreditCondition(creditCondition));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(creditConditionService.deleteCreditConditionByIds(ids));
    }

    /**
     * 新增经销商明细表
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult add(@RequestBody Map<String, Object> resultMap) {
        try {
            return toAjax(creditConditionService.saveCreditcs(resultMap));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("新增失败,原因是:"+e.toString());
        }
    }
}
