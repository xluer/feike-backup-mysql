package com.ruoyi.feike.controller;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.ContractUploadUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.feike.domain.vo.BasicContractVO;
import com.ruoyi.framework.config.ServerConfig;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.BasicContract;
import com.ruoyi.feike.service.IBasicContractService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 合同信息Controller
 *
 * @author ybw
 * @date 2022-07-19
 */
@RestController
@RequestMapping("/feike/contract")
public class BasicContractController extends BaseController
{
    @Autowired
    private IBasicContractService basicContractService;
    @Autowired
    private ServerConfig serverConfig;

    /**
     * 查询合同信息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(BasicContract basicContract)
    {
        startPage();
        List<BasicContract> list = basicContractService.selectBasicContractList(basicContract);
        return getDataTable(list);
    }

    /**
     * 查询合同信息列表并根据合同名称主机厂分组
     */
    @GetMapping("/listgroupby")
    public TableDataInfo listgroupby(BasicContract basicContract)
    {
        startPage();
        List<BasicContract> list = basicContractService.selectBasicContractListgroupby(basicContract);
        return getDataTable(list);
    }

    /**
     * 导出合同信息列表
     */
    @Log(title = "合同信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BasicContract basicContract)
    {
        List<BasicContract> list = basicContractService.selectBasicContractList(basicContract);
        ExcelUtil<BasicContract> util = new ExcelUtil<BasicContract>(BasicContract.class);
        return util.exportExcel(list, "contract");
    }

    /**
     * 获取合同信息详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        System.out.println("11111111");
        return AjaxResult.success(basicContractService.selectBasicContractById(id));
    }

    /**
     * 新增合同信息
     */
    @Log(title = "合同信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BasicContract basicContract)
    {
        return toAjax(basicContractService.insertBasicContract(basicContract));
    }

    /**
     * 修改合同信息
     */
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BasicContract basicContract)
    {
        return toAjax(basicContractService.updateBasicContract(basicContract));
    }

    /**
     * 删除合同信息
     */
    @Log(title = "合同信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(basicContractService.deleteBasicContractByIds(ids));
    }

    /**
     * 根据优先级、主机厂、授信类型查询对应合同
     */
    @RequestMapping(value = "/listBasicContract",method = RequestMethod.GET)
    @ResponseBody
    public TableDataInfo listBasicContract(@RequestBody Map<String, Object> resultMap){
        List<BasicContract> list = basicContractService.listBasicContract(resultMap);
        return getDataTable(list);
    }

//    /**
//     * 维护合同信息表--下载
//     */
//    @GetMapping(value = "/{contractLocation}")
//    public void downloadTheFile(@PathVariable("contractLocation") String contractLocation){
//        String filePath = RuoYiConfig.getUploadPath();
//        System.out.println("1111"+filePath);
//        System.out.println(contractLocation);
//    }

    /**
     * 合同上传请求
     */
    @PostMapping("/common/upload")
    public AjaxResult uploadFile(MultipartFile file, String sector, String contractname) throws Exception
    {
        try
        {
            // 上传文件路径
            String filePath = RuoYiConfig.getProfile()+"/template/"+sector;
            // 上传并返回新文件名称
//            contractname = "123";
            String fileName = ContractUploadUtils.upload(filePath, file,contractname);
            String url = serverConfig.getUrl() + fileName;
            String originalFilename = file.getOriginalFilename();       // 文件的中文名
            System.out.println("fileName=="+fileName);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            ajax.put("originalFilename", originalFilename);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * 修改分类后的合同信息
     */
    @Log(title = "合同信息", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult editgroupby(@RequestBody BasicContractVO basicContractVO)
    {
        System.out.println("1111");
        return toAjax(basicContractService.updateBasicContractgroupby(basicContractVO));
    }

}
