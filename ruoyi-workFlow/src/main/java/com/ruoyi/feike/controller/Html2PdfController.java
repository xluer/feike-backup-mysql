package com.ruoyi.feike.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.spire.pdf.graphics.PdfMargins;
import com.spire.pdf.htmlconverter.qt.HtmlConverter;
import com.spire.pdf.htmlconverter.qt.Size;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author 神
 * @date 2022年07月27日 9:38
 *
 * 打印pdf（表单页转pdf）
 */
@RestController
@RequestMapping("/api/html2pdf")
public class Html2PdfController {

    @PostMapping("/toPdf")
    public AjaxResult htmlToPdf(@RequestBody Map<String,Object> map, HttpServletResponse response) {
        //定义需要转换的HTML
        // String url = "https://www.baidu.com/";
        String url = map.get("url").toString();

        String formkey=map.get("formkey").toString();
        // String formkey="123456789";

        //创建年月日文件夹
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String res = simpleDateFormat.format(new Date());
        String year = res.substring(0, 4);   //2022
        String month = res.substring(4, 6);  //07
        String day = res.substring(6);      //08

        //转换后的结果文档（结果文档保存在Java项目程序文件下）
        String fileName = "doc"+ File.separator+year+File.separator+month+File.separator+day+File.separator+formkey+".pdf";
        System.out.println("fileName=="+fileName);

        String projectDir = System.getProperty("user.dir");  // 获取项目路径
        String templatePath = projectDir+"\\"+fileName;      // 完整路径
        System.out.println("projectDir=="+projectDir);
        System.out.println("templatePath=="+templatePath);

        File file = new File(templatePath);
        byte[] data = null;
        FileInputStream input=null;
        if (file.exists()) {
            try {
                input= new FileInputStream(file);
                data = new byte[input.available()];
                input.read(data);
                response.getOutputStream().write(data);
            } catch (Exception e) {
                System.out.println("pdf文件处理异常：" + e);
            }finally{
                try {
                    if(input!=null){
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            //解压后的插件本地地址（这里是把插件包放在了Java项目文件夹下，也可以自定义其他本地路径）
            String pluginPath = projectDir+"\\plugins";
            HtmlConverter.setPluginPath(pluginPath);

            //调用方法转换到PDF并设置PDF尺寸
            HtmlConverter.convert(url, fileName, true, 1000, new Size(700f, 800f), new PdfMargins(0));
            try {
                input= new FileInputStream(file);
                data = new byte[input.available()];
                input.read(data);
                response.getOutputStream().write(data);
            } catch (Exception e) {
                System.out.println("pdf文件处理异常：" + e);
            }finally{
                try {
                    if(input!=null){
                        input.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("fileName", fileName);
        ajax.put("url", "url");
        return ajax;
    }
}
