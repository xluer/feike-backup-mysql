package com.ruoyi.feike.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.feike.domain.DepositDecreaseDTO;
import com.ruoyi.feike.domain.NewApplicationDTO;
import com.ruoyi.feike.service.IDepositDecreaseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lss
 * @version 1.0
 * @description: 经销商申请审批流程控制器
 * @date 2022/8/3 13:23
 */
@RestController
@RequestMapping("/feike")
@Slf4j
public class DepositDecreaseController {

    @Autowired
    private IDepositDecreaseService depositDecreaseService;

    /**
    * @description: 降低保证金比例申请审批流程接口
    * @param:
    * @return:
    * @author
    * @date: 2022/8/3 13:26
    */

    @PostMapping("/depositDecrease")
    public AjaxResult list(@RequestBody NewApplicationDTO newApplicationDTO)
    {
        if (newApplicationDTO==null){
            return AjaxResult.error("数据错误!");
        }
        depositDecreaseService.insertActApprove(newApplicationDTO);
        return AjaxResult.success();
    }


    /**
     * @description: ODS数据查询接口
     * @param:
     * @return:
     * @author lss
     * @date: 2022/8/11 15:20
     */

    @PostMapping("/getForeignInfo")
    public AjaxResult getForeignInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) {
        List<String> titleList = new ArrayList<>();
        titleList.add("Dealer");
        titleList.add("Sector");
        int[] titleArr = new int[3];
        List<Map<String, Object>> list = depositDecreaseService.getForignList(depositDecreaseDTO);
        Map<String, Object> titleMap = list.get(0);
        int num = 0;
        for (Object str : titleMap.keySet()) {
            if (!"DEALER".equals(String.valueOf(str)) && !"BRAND".equals(String.valueOf(str))) {
                titleArr[num] = Integer.parseInt(str.toString());
                num++;
            }
        }
        //按照年份由小到大冒泡排序
        for (int i = 0; i < titleArr.length - 1; i++) {
            for (int j = 0; j < titleArr.length - 1 - i; j++) {
                if (titleArr[j] > titleArr[j + 1]) {
                    int temp = titleArr[j];
                    titleArr[j] = titleArr[j + 1];
                    titleArr[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < titleArr.length; i++) {
            titleList.add(String.valueOf(titleArr[i]));
        }
        Map<String, Object> map = new HashMap<>();
        map.put("title", titleList);
        map.put("result", list);
        return AjaxResult.success(map);
    }


    @PostMapping("/getCreditInfo")
    public AjaxResult getCreditInfo(@RequestBody DepositDecreaseDTO depositDecreaseDTO) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("DealerCode", depositDecreaseDTO.getDealerCodeArr());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost("http://10.226.182.86:8081/CreditLoanSuspenseAPIUAT7/CreditInfo");
        httpPost.addHeader("Content-Type", "application/json;charset=UTF-8");
        StringEntity entity = new StringEntity(jsonObject.toString(), "utf-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding("UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json, text/plain, */*");
        httpPost.setHeader("Accept-Encoding", "gzip, deflate");
        httpPost.setHeader("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6");
        httpPost.setHeader("Connection", "keep-alive");
        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38");
        Map<String,Object> objectList=new HashMap<>();
        try {
            HttpResponse response = httpClient.execute(httpPost);
            String responseStr=EntityUtils.toString(response.getEntity());
            log.info("WFS外部接口数据返回结果=[{}]",responseStr);
            objectList=JSONObject.parseObject(responseStr,Map.class);
            log.info("WFS外部接口数据返回JSON结果=[{}]",JSON.toJSONString(responseStr));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.success(objectList);
    }

}
