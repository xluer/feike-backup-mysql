package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.Fileupload;
import com.ruoyi.feike.service.IFileuploadService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * FileuploadController
 *
 * @author zmh
 * @date 2022-08-03
 */
@RestController
@RequestMapping("/feike/fileupload")
public class FileuploadController extends BaseController
{
    @Autowired
    private IFileuploadService fileuploadService;

    /**
     * 查询Fileupload列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Fileupload fileupload)
    {
        startPage();
        List<Fileupload> list = fileuploadService.selectFileuploadList(fileupload);
        return getDataTable(list);
    }

    /**
     * 导出Fileupload列表
     */
    @Log(title = "Fileupload", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Fileupload fileupload)
    {
        List<Fileupload> list = fileuploadService.selectFileuploadList(fileupload);
        ExcelUtil<Fileupload> util = new ExcelUtil<Fileupload>(Fileupload.class);
        return util.exportExcel(list, "fileupload");
    }

    /**
     * 获取Fileupload详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(fileuploadService.selectFileuploadById(id));
    }

    /**
     * 新增Fileupload
     */
    @Log(title = "Fileupload", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Fileupload fileupload)
    {
        return toAjax(fileuploadService.insertFileupload(fileupload));
    }

    /**
     * 修改Fileupload
     */
    @Log(title = "Fileupload", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Fileupload fileupload)
    {
        return toAjax(fileuploadService.updateFileupload(fileupload));
    }

    /**
     * 删除Fileupload
     */
    @Log(title = "Fileupload", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(fileuploadService.deleteFileuploadByIds(ids));
    }

    /**
     * 删除Fileupload
     */
    @Log(title = "Fileupload", businessType = BusinessType.DELETE)
    @DeleteMapping("/delFileUrl")
    public AjaxResult delFileUrl(String fileName)
    {
        return toAjax(fileuploadService.delFileUrl(fileName));
    }

}
