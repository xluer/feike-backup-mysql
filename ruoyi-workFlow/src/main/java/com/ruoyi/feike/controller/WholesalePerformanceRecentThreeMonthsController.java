package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.WholesalePerformanceRecentThreeMonths;
import com.ruoyi.feike.service.IWholesalePerformanceRecentThreeMonthsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/months")
public class WholesalePerformanceRecentThreeMonthsController extends BaseController
{
    @Autowired
    private IWholesalePerformanceRecentThreeMonthsService wholesalePerformanceRecentThreeMonthsService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:months:list')")
    @GetMapping("/list")
    public TableDataInfo list(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        startPage();
        List<WholesalePerformanceRecentThreeMonths> list = wholesalePerformanceRecentThreeMonthsService.selectWholesalePerformanceRecentThreeMonthsList(wholesalePerformanceRecentThreeMonths);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:months:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        List<WholesalePerformanceRecentThreeMonths> list = wholesalePerformanceRecentThreeMonthsService.selectWholesalePerformanceRecentThreeMonthsList(wholesalePerformanceRecentThreeMonths);
        ExcelUtil<WholesalePerformanceRecentThreeMonths> util = new ExcelUtil<WholesalePerformanceRecentThreeMonths>(WholesalePerformanceRecentThreeMonths.class);
        return util.exportExcel(list, "months");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:months:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(wholesalePerformanceRecentThreeMonthsService.selectWholesalePerformanceRecentThreeMonthsById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:months:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        return toAjax(wholesalePerformanceRecentThreeMonthsService.insertWholesalePerformanceRecentThreeMonths(wholesalePerformanceRecentThreeMonths));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:months:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths)
    {
        return toAjax(wholesalePerformanceRecentThreeMonthsService.updateWholesalePerformanceRecentThreeMonths(wholesalePerformanceRecentThreeMonths));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:months:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(wholesalePerformanceRecentThreeMonthsService.deleteWholesalePerformanceRecentThreeMonthsByIds(ids));
    }
}
