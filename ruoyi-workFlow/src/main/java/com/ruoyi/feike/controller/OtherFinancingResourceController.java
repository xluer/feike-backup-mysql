package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.OtherFinancingResource;
import com.ruoyi.feike.service.IOtherFinancingResourceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * feikeController
 * 
 * @author zmh
 * @date 2022-07-11
 */
@RestController
@RequestMapping("/feike/resource")
public class OtherFinancingResourceController extends BaseController
{
    @Autowired
    private IOtherFinancingResourceService otherFinancingResourceService;

    /**
     * 查询feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:list')")
    @GetMapping("/list")
    public TableDataInfo list(OtherFinancingResource otherFinancingResource)
    {
        startPage();
        List<OtherFinancingResource> list = otherFinancingResourceService.selectOtherFinancingResourceList(otherFinancingResource);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:export')")
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(OtherFinancingResource otherFinancingResource)
    {
        List<OtherFinancingResource> list = otherFinancingResourceService.selectOtherFinancingResourceList(otherFinancingResource);
        ExcelUtil<OtherFinancingResource> util = new ExcelUtil<OtherFinancingResource>(OtherFinancingResource.class);
        return util.exportExcel(list, "resource");
    }

    /**
     * 获取feike详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(otherFinancingResourceService.selectOtherFinancingResourceById(id));
    }

    /**
     * 新增feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:add')")
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OtherFinancingResource otherFinancingResource)
    {
        return toAjax(otherFinancingResourceService.insertOtherFinancingResource(otherFinancingResource));
    }

    /**
     * 修改feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:edit')")
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OtherFinancingResource otherFinancingResource)
    {
        return toAjax(otherFinancingResourceService.updateOtherFinancingResource(otherFinancingResource));
    }

    /**
     * 删除feike
     */
    @PreAuthorize("@ss.hasPermi('feike:resource:remove')")
    @Log(title = "feike", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(otherFinancingResourceService.deleteOtherFinancingResourceByIds(ids));
    }
}
