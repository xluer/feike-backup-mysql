package com.ruoyi.feike.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.DealerSectorLimitflag;
import com.ruoyi.feike.service.IDealerSectorLimitflagService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * FlagController
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@RestController
@RequestMapping("/feike/limitflag")
public class DealerSectorLimitflagController extends BaseController
{
    @Autowired
    private IDealerSectorLimitflagService dealerSectorLimitflagService;

    /**
     * 查询Flag列表
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:list')")
    @GetMapping("/list")
    public TableDataInfo list(DealerSectorLimitflag dealerSectorLimitflag)
    {
        startPage();
        List<DealerSectorLimitflag> list = dealerSectorLimitflagService.selectDealerSectorLimitflagList(dealerSectorLimitflag);
        return getDataTable(list);
    }

    /**
     * 导出Flag列表
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:export')")
    @Log(title = "Flag", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DealerSectorLimitflag dealerSectorLimitflag)
    {
        List<DealerSectorLimitflag> list = dealerSectorLimitflagService.selectDealerSectorLimitflagList(dealerSectorLimitflag);
        ExcelUtil<DealerSectorLimitflag> util = new ExcelUtil<DealerSectorLimitflag>(DealerSectorLimitflag.class);
        return util.exportExcel(list, "limitflag");
    }

    /**
     * 获取Flag详细信息
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dealerSectorLimitflagService.selectDealerSectorLimitflagById(id));
    }

    /**
     * 新增Flag
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:add')")
    @Log(title = "Flag", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DealerSectorLimitflag dealerSectorLimitflag)
    {
        return toAjax(dealerSectorLimitflagService.insertDealerSectorLimitflag(dealerSectorLimitflag));
    }

    /**
     * 修改Flag
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:edit')")
    @Log(title = "Flag", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DealerSectorLimitflag dealerSectorLimitflag)
    {
        return toAjax(dealerSectorLimitflagService.updateDealerSectorLimitflag(dealerSectorLimitflag));
    }

    /**
     * 删除Flag
     */
    @PreAuthorize("@ss.hasPermi('feike:limitflag:remove')")
    @Log(title = "Flag", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dealerSectorLimitflagService.deleteDealerSectorLimitflagByIds(ids));
    }
}
