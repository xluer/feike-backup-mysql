package com.ruoyi.feike.controller;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.feike.domain.vo.ParkingLocationVO;
import com.ruoyi.framework.web.service.TokenService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.service.IParkingLocationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * feikeController
 *
 * @author zmh
 * @date 2022-07-05
 */
@RestController
@RequestMapping("/feike/location")
public class ParkingLocationController extends BaseController
{
    @Autowired
    private IParkingLocationService parkingLocationService;
    @Autowired
    private TokenService tokenService;

    /**
     * 查询feike列表
     */
    @GetMapping("/list")
    public TableDataInfo list(ParkingLocation parkingLocation)
    {
        startPage();
        List<ParkingLocation> list = parkingLocationService.selectParkingLocationList(parkingLocation);
        return getDataTable(list);
    }

    /**
     * 连接dealerinformation查询feike列表
     */
    @GetMapping("/list/anddealer")
    public TableDataInfo list(ParkingLocationVO parkingLocation)
    {
        startPage();
        List<ParkingLocationVO> list = parkingLocationService.selectParkingLocationListANDplm(parkingLocation);
        return getDataTable(list);
    }

    /**
     * 导出feike列表
     */
    @Log(title = "feike", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ParkingLocation parkingLocation)
    {
        List<ParkingLocation> list = parkingLocationService.selectParkingLocationList(parkingLocation);
        ExcelUtil<ParkingLocation> util = new ExcelUtil<ParkingLocation>(ParkingLocation.class);
        return util.exportExcel(list, "location");
    }

    /**
     * 获取feike详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(parkingLocationService.selectParkingLocationById(id));
    }

    /**
     * 新增feike
     */
    @Log(title = "feike", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ParkingLocation parkingLocation)
    {
        return toAjax(parkingLocationService.insertParkingLocation(parkingLocation));
    }

    /**
     * 修改feike
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ParkingLocation parkingLocation)
    {
        return toAjax(parkingLocationService.updateParkingLocation(parkingLocation));
    }

    /**
     * 删除feike
     */
    @Log(title = "feike", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(parkingLocationService.deleteParkingLocationByIds(ids));
    }

    @Log(title = "导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<ParkingLocation> util = new ExcelUtil<ParkingLocation>(ParkingLocation.class);
        List<ParkingLocation> userList = util.importExcel(file.getInputStream());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String operName = loginUser.getUsername();
        String message = parkingLocationService.importUser(userList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate()
    {
        ExcelUtil<ParkingLocation> util = new ExcelUtil<ParkingLocation>(ParkingLocation.class);
        return util.importTemplateExcel("用户数据");
    }

    /**
     * 根据规则修改status状态
     */
    @Log(title = "feike", businessType = BusinessType.UPDATE)
    @RequestMapping( value = "/updateParkingLocationStatus", method = RequestMethod.PUT)
    @ResponseBody
    public AjaxResult updateParkingLocationStatus()
    {
        try {
            return toAjax(parkingLocationService.updateParkingLocationStatus());
        }catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.toString());
        }
    }
}
