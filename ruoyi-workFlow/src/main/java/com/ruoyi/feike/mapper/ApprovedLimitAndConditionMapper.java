package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;

/**
 * feikeMapper接口
 * 
 * @author ybw
 * @date 2022-07-12
 */
public interface ApprovedLimitAndConditionMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ApprovedLimitAndCondition selectApprovedLimitAndConditionById(String id);

    /**
     * 查询feike列表
     * 
     * @param approvedLimitAndCondition feike
     * @return feike集合
     */
    public List<ApprovedLimitAndCondition> selectApprovedLimitAndConditionList(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 新增feike
     * 
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    public int insertApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 修改feike
     * 
     * @param approvedLimitAndCondition feike
     * @return 结果
     */
    public int updateApprovedLimitAndCondition(ApprovedLimitAndCondition approvedLimitAndCondition);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteApprovedLimitAndConditionById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteApprovedLimitAndConditionByIds(String[] ids);

    /**
     * 根据dealer(经销商)和sector(品牌)为一组进行分组，去除重复的字段
     *
     * @param approvedLimitAndCondition feike
     * @return feike集合
     */
    public List<ApprovedLimitAndCondition> selectApprovedLimitAndConditionListgroupby(ApprovedLimitAndCondition approvedLimitAndCondition);
}
