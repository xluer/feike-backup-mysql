package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.Securities;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-11
 */
public interface SecuritiesMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Securities selectSecuritiesById(String id);

    /**
     * 查询feike列表
     * 
     * @param securities feike
     * @return feike集合
     */
    public List<Securities> selectSecuritiesList(Securities securities);

    /**
     * 新增feike
     * 
     * @param securities feike
     * @return 结果
     */
    public int insertSecurities(Securities securities);

    /**
     * 修改feike
     * 
     * @param securities feike
     * @return 结果
     */
    public int updateSecurities(Securities securities);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteSecuritiesById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSecuritiesByIds(String[] ids);

    List<Securities> selectSecuritiesByInstanceId(String instanceId);

    int deleteSecuritiesByInstanceIds(String[] ids);

    int deleteSecuritiesByInstanceId(String instanceId);
}
