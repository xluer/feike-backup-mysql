package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.Plm;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * plm导入表Mapper接口
 *
 * @author ruoyi
 * @date 2022-07-21
 */
public interface PlmMapper
{
    /**
     * 查询plm导入表
     *
     * @param id plm导入表ID
     * @return plm导入表
     */
    public Plm selectPlmById(String id);

    /**
     * 查询plm导入表列表
     *
     * @param plm plm导入表
     * @return plm导入表集合
     */
    public List<Plm> selectPlmList(Plm plm);

    /**
     * 新增plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    public int insertPlm(Plm plm);

    /**
     * 修改plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    public int updatePlm(Plm plm);

    /**
     * 删除plm导入表
     *
     * @param id plm导入表ID
     * @return 结果
     */
    public int deletePlmById(String id);

    /**
     * 批量删除plm导入表
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePlmByIds(String[] ids);

    public int deletePlmBy();

    Plm selectPlmListByDealerName(String dealernamecn);

    List<Plm> selectPlmLists();

    Map getFlagByDealerName(@Param("dealerName") String dealerName);
}
