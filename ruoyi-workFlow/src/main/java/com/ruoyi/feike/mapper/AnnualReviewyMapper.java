package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.DepositDecreaseDTO;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.domain.vo.AnnualReviewyVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
public interface AnnualReviewyMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public AnnualReviewy selectAnnualReviewyById(String id);

    /**
     * 查询feike列表
     *
     * @param annualReviewy feike
     * @return feike集合
     */
    public List<AnnualReviewy> selectAnnualReviewyList(AnnualReviewy annualReviewy);

    /**
     * 新增feike
     *
     * @param annualReviewy feike
     * @return 结果
     */
    public int insertAnnualReviewy(AnnualReviewy annualReviewy);

    /**
     * 修改feike
     *
     * @param annualReviewy feike
     * @return 结果
     */
    public int updateAnnualReviewy(AnnualReviewy annualReviewy);
    public int updateAnnualReviewyByInstanceId(AnnualReviewy annualReviewy);


    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAnnualReviewyByIds(String[] ids);

    AnnualReviewy selectAnnualReviewyByInstanceId(String instanceId);

    List<AnnualReviewy> selectAnnualReviewyListByWorkflowLeaveAndDeptId(@Param("annualReviewy") AnnualReviewy annualReviewy,@Param("deptId") Long deptId);

    List<AnnualReviewy> selectAnnualReviewyByIds(String[] ids);

    int selectCountByCreatenameAndStatus(@Param("username")String username,@Param("status")String status);

    List<AnnualReviewy> selectAnnualReviewyByInstanceIds(List<String> instanceIds);

	int closeUpdateAnnualReviewyObj(@Param("instanceId")String instanceId,@Param("username")String username);

    int openUpdateAnnualReviewyObj(String username);

    List<AnnualReviewyHistoryVO> selectAnnualReviewyListBySelf(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("basicInformation")BasicInformation basicInformation, @Param("deptId")Long deptId);

    List<Map<String,Object>> getForeignDsoList(DepositDecreaseDTO depositDecreaseDTO);

    List<Map<String,Object>> getForeignAingList(DepositDecreaseDTO depositDecreaseDTO);

}
