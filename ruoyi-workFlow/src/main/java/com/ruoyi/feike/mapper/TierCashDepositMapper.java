package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.TierCashDeposit;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.domain.vo.TierCashDepositVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public interface TierCashDepositMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public TierCashDeposit selectTierCashDepositById(String id);

    /**
     * 查询feike列表
     * 
     * @param tierCashDeposit feike
     * @return feike集合
     */
    public List<TierCashDeposit> selectTierCashDepositList(TierCashDeposit tierCashDeposit);

    /**
     * 新增feike
     * 
     * @param tierCashDeposit feike
     * @return 结果
     */
    public int insertTierCashDeposit(TierCashDeposit tierCashDeposit);

    /**
     * 修改feike
     * 
     * @param tierCashDeposit feike
     * @return 结果
     */
    public int updateTierCashDeposit(TierCashDeposit tierCashDeposit);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteTierCashDepositById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTierCashDepositByIds(String[] ids);

    List<AnnualReviewyHistoryVO> selectTierCashDepositListBySelf(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("tierCashDeposit")TierCashDeposit tierCashDeposit, @Param("deptId")Long deptId);

    TierCashDeposit selectTiierCashDepositByDealerName(String dealerName);

    List<TierCashDepositVO> selectTierCashDepositLists();
}

