package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealerSectorContractgroup;

/**
 * 合同Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public interface DealerSectorContractgroupMapper 
{
    /**
     * 查询合同
     * 
     * @param id 合同ID
     * @return 合同
     */
    public DealerSectorContractgroup selectDealerSectorContractgroupById(String id);

    /**
     * 查询合同列表
     * 
     * @param dealerSectorContractgroup 合同
     * @return 合同集合
     */
    public List<DealerSectorContractgroup> selectDealerSectorContractgroupList(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 新增合同
     * 
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    public int insertDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 修改合同
     * 
     * @param dealerSectorContractgroup 合同
     * @return 结果
     */
    public int updateDealerSectorContractgroup(DealerSectorContractgroup dealerSectorContractgroup);

    /**
     * 删除合同
     * 
     * @param id 合同ID
     * @return 结果
     */
    public int deleteDealerSectorContractgroupById(String id);

    /**
     * 批量删除合同
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerSectorContractgroupByIds(String[] ids);
}
