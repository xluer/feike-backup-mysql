package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.ActiveLimitAdjustmentProposal;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface ActiveLimitAdjustmentProposalMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ActiveLimitAdjustmentProposal selectActiveLimitAdjustmentProposalById(String id);

    /**
     * 查询feike列表
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return feike集合
     */
    public List<ActiveLimitAdjustmentProposal> selectActiveLimitAdjustmentProposalList(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal);

    /**
     * 新增feike
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return 结果
     */
    public int insertActiveLimitAdjustmentProposal(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal);

    /**
     * 修改feike
     * 
     * @param activeLimitAdjustmentProposal feike
     * @return 结果
     */
    public int updateActiveLimitAdjustmentProposal(ActiveLimitAdjustmentProposal activeLimitAdjustmentProposal);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteActiveLimitAdjustmentProposalById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteActiveLimitAdjustmentProposalByIds(String[] ids);
}
