package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.CreditCondition;

/**
 * feikeMapper接口
 * 
 * @author ybw
 * @date 2022-07-12
 */
public interface CreditConditionMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public CreditCondition selectCreditConditionById(String id);

    /**
     * 查询feike列表
     * 
     * @param creditCondition feike
     * @return feike集合
     */
    public List<CreditCondition> selectCreditConditionList(CreditCondition creditCondition);

    /**
     * 新增feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    public int insertCreditCondition(CreditCondition creditCondition);

    /**
     * 修改feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    public int updateCreditCondition(CreditCondition creditCondition);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteCreditConditionById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCreditConditionByIds(String[] ids);
}
