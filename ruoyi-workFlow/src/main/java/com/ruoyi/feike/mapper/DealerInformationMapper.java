package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.vo.DealerformationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 *
 * @author ybw
 * @date 2022-07-12
 */
public interface DealerInformationMapper
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerInformation selectDealerInformationById(String id);

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike集合
     */
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation);

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int insertDealerInformation(DealerInformation dealerInformation);

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformation(DealerInformation dealerInformation);

    /**
     * 修改dealername相同的全部数据feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformationnew(DealerformationVO dealerInformation);

    /**
     * 删除feike
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerInformationById(String id);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerInformationByIds(String[] ids);

    public List<DealerInformation> getGroups();
    public List<DealerInformation> getDealers(String groupName);

    /**
     * 根据dealername查询feike
     *
     * @param  dealerName
     * @return feike
     */
    public List<DealerInformation> selectDealerInformationByname(String dealerName);

    public List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation);

    DealerInformation selectDealerInformationByDealerNameAndType(String dealernamecn);

    List<Map> selectDealerInformationDistinctALL();

    List<DealerInformation> selectDealerInformationByDealerName(String dealernamecn);

    List<DealerInformation> listByName(List<DealerInformation> params);

    List getInfoListByDealerName(@Param("dealerName") String dealerName);
}
