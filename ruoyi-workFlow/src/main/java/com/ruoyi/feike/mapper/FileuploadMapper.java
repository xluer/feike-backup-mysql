package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.Fileupload;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-07-22
 */
public interface FileuploadMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Fileupload selectFileuploadById(String id);

    public List<Fileupload> selectFileuploadByInstanceId(String instanceId);

    /**
     * 查询feike列表
     * 
     * @param fileupload feike
     * @return feike集合
     */
    public List<Fileupload> selectFileuploadList(Fileupload fileupload);

    /**
     * 新增feike
     * 
     * @param fileupload feike
     * @return 结果
     */
    public int insertFileupload(Fileupload fileupload);

    /**
     * 修改feike
     * 
     * @param fileupload feike
     * @return 结果
     */
    public int updateFileupload(Fileupload fileupload);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteFileuploadById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFileuploadByIds(String[] ids);

    List<Fileupload> selectFileuploadListByInstanceId(String instanceId);


}
