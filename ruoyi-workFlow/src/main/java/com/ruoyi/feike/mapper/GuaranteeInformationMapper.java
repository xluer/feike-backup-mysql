package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.GuaranteeInformation;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-14
 */
public interface GuaranteeInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public GuaranteeInformation selectGuaranteeInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param guaranteeInformation feike
     * @return feike集合
     */
    public List<GuaranteeInformation> selectGuaranteeInformationList(GuaranteeInformation guaranteeInformation);

    /**
     * 新增feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    public int insertGuaranteeInformation(GuaranteeInformation guaranteeInformation);

    /**
     * 修改feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    public int updateGuaranteeInformation(GuaranteeInformation guaranteeInformation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteGuaranteeInformationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGuaranteeInformationByIds(String[] ids);
}
