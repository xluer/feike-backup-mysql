package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.domain.ParkingLocationRegistration;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public interface ParkingLocationRegistrationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ParkingLocationRegistration selectParkingLocationRegistrationById(String id);

    /**
     * 查询feike列表
     * 
     * @param parkingLocationRegistration feike
     * @return feike集合
     */
    public List<ParkingLocationRegistration> selectParkingLocationRegistrationList(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 新增feike
     * 
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    public int insertParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 修改feike
     * 
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    public int updateParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteParkingLocationRegistrationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteParkingLocationRegistrationByIds(String[] ids);

    List<AnnualReviewyHistoryVO> selectParkingLocationRegistrationListBySelf(@Param("annualReviewy")AnnualReviewy annualReviewy, @Param("parkingLocationRegistration")ParkingLocationRegistration parkingLocationRegistration, @Param("deptId")Long deptId);

    ParkingLocation selectParkingLocationRegistrationByGroupIdAndDealerId(@Param("groupId")String groupId, @Param("dealerId")String dealerId);

    ParkingLocation selectParkingLocationRegistrationByGroupNameAndDealerName(@Param("groupName")String groupName, @Param("dealerName")String dealerName);

    List<ParkingLocation> selectParkingLocationRegistrationByDealerName(@Param("dealerName")String dealerName);

    List<ParkingLocationRegistration> selectParkingLocationRegistrationByInstancsId(@Param("instanceId")String instanceId);
}
