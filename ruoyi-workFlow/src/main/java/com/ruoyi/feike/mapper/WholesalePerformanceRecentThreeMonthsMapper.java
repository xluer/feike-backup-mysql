package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.WholesalePerformanceRecentThreeMonths;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface WholesalePerformanceRecentThreeMonthsMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public WholesalePerformanceRecentThreeMonths selectWholesalePerformanceRecentThreeMonthsById(String id);

    /**
     * 查询feike列表
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return feike集合
     */
    public List<WholesalePerformanceRecentThreeMonths> selectWholesalePerformanceRecentThreeMonthsList(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 新增feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    public int insertWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 修改feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    public int updateWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteWholesalePerformanceRecentThreeMonthsById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWholesalePerformanceRecentThreeMonthsByIds(String[] ids);

    int deleteWholesalePerformanceRecentThreeMonthsByInstanceIds(String[] ids);

    int deleteWholesalePerformanceRecentThreeMonthsByInstanceId(String instanceId);
}
