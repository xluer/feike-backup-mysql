package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.BalanceInformation;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface BalanceInformationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public BalanceInformation selectBalanceInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param balanceInformation feike
     * @return feike集合
     */
    public List<BalanceInformation> selectBalanceInformationList(BalanceInformation balanceInformation);

    /**
     * 新增feike
     * 
     * @param balanceInformation feike
     * @return 结果
     */
    public int insertBalanceInformation(BalanceInformation balanceInformation);

    /**
     * 修改feike
     * 
     * @param balanceInformation feike
     * @return 结果
     */
    public int updateBalanceInformation(BalanceInformation balanceInformation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteBalanceInformationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBalanceInformationByIds(String[] ids);
}
