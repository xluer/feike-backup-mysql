package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface DealerNegativeInformationCheckMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public DealerNegativeInformationCheck selectDealerNegativeInformationCheckById(String id);

    /**
     * 查询feike列表
     * 
     * @param dealerNegativeInformationCheck feike
     * @return feike集合
     */
    public List<DealerNegativeInformationCheck> selectDealerNegativeInformationCheckList(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 新增feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    public int insertDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 修改feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    public int updateDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerNegativeInformationCheckById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerNegativeInformationCheckByIds(String[] ids);
}
