package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.OtherFinancingResource;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-11
 */
public interface OtherFinancingResourceMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public OtherFinancingResource selectOtherFinancingResourceById(String id);

    /**
     * 查询feike列表
     * 
     * @param otherFinancingResource feike
     * @return feike集合
     */
    public List<OtherFinancingResource> selectOtherFinancingResourceList(OtherFinancingResource otherFinancingResource);

    /**
     * 新增feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    public int insertOtherFinancingResource(OtherFinancingResource otherFinancingResource);

    /**
     * 修改feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    public int updateOtherFinancingResource(OtherFinancingResource otherFinancingResource);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteOtherFinancingResourceById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOtherFinancingResourceByIds(String[] ids);

    List<OtherFinancingResource> selectOtherFinancingResourceByInstanceId(String instanceId);

    int deleteOtherFinancingResourceByInstanceIds(String[] ids);

    int deleteOtherFinancingResourceByInstanceId(String instanceId);
}
