package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.domain.vo.ParkingLocationVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface ParkingLocationMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ParkingLocation selectParkingLocationById(String id);

    /**
     * 查询feike列表
     * 
     * @param parkingLocation feike
     * @return feike集合
     */
    public List<ParkingLocation> selectParkingLocationList(ParkingLocation parkingLocation);

    /**
     * 新增feike
     * 
     * @param parkingLocation feike
     * @return 结果
     */
    public int insertParkingLocation(ParkingLocation parkingLocation);

    /**
     * 修改feike
     * 
     * @param parkingLocation feike
     * @return 结果
     */
    public int updateParkingLocation(ParkingLocation parkingLocation);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteParkingLocationById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteParkingLocationByIds(String[] ids);

    /**
     * 查询twoNdTierDeposit>0的数据
     *
     * @param parkingLocation feike
     * @return feike集合
     */
    public List<ParkingLocation> selectParkingLocationListgreaterThanZero(ParkingLocation parkingLocation);

    /**
     * 连接dealerinformation查询
     * @param parkingLocation
     * @return
     */
    public List<ParkingLocationVO> selectParkingLocationListANDplm(ParkingLocationVO parkingLocation);

    public int deletePklBy();

    List<ParkingLocation> selectParkingLocationByInstancsId(String instanceId);

    List<ParkingLocation> selectParkingLocationLists();

    public Map<String,Object> selectParkingLocationCountAndSumByType(@Param("dealernamecn") String dealernamecn, @Param("type") String type, @Param("status") String status);

    List<ParkingLocation> selectParkingLocationListAll();

    public Map getFlagByDealerNameAndType(@Param("dealerName")String dealerName, @Param("type")String type);

    ParkingLocation selectParkingLocationByDealerAndParkingLocation(@Param("dealerNameCn")String dealerNameCn, @Param("parkingLocation")String parkingLocation);
}
