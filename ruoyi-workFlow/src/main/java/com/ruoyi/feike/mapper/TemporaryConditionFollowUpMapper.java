package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.TemporaryConditionFollowUp;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-11
 */
public interface TemporaryConditionFollowUpMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public TemporaryConditionFollowUp selectTemporaryConditionFollowUpById(String id);

    /**
     * 查询feike列表
     * 
     * @param temporaryConditionFollowUp feike
     * @return feike集合
     */
    public List<TemporaryConditionFollowUp> selectTemporaryConditionFollowUpList(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 新增feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    public int insertTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 修改feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    public int updateTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteTemporaryConditionFollowUpById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTemporaryConditionFollowUpByIds(String[] ids);

    List<TemporaryConditionFollowUp> selectTemporaryConditionFollowUpByInstanceId(String instanceId);

    int deleteTemporaryConditionFollowUpByInstanceIds(String[] ids);

    int deleteTemporaryConditionFollowUpByInstanceId(String instanceId);
}
