package com.ruoyi.feike.mapper;

import com.ruoyi.feike.domain.ApprovedLimitAndCondition;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;

import java.util.List;

/**
 * feikeMapper接口
 * 
 * @author zmh
 * @date 2022-07-18
 */
public interface ProposalByCommericalAndMarketingMapper 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public ProposalByCommericalAndMarketing selectProposalByCommericalAndMarketingById(String id);

    /**
     * 查询feike列表
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return feike集合
     */
    public List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingList(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 新增feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    public int insertProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 修改feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    public int updateProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    /**
     * 删除feike
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteProposalByCommericalAndMarketingById(String id);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProposalByCommericalAndMarketingByIds(String[] ids);

    List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingByInstanceId(String instanceId);

    int deleteProposalByCommericalAndMarketingByInstanceIds(String[] ids);

    int deleteProposalByCommericalAndMarketingByInstanceId(String instanceId);

    /**
     * 根据dealer(经销商)和sector(品牌)为一组进行分组，去除重复的字段
     *
     * @param proposalByCommericalAndMarketing feike
     * @return feike集合
     */
    public List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingListgroupby(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);

    public List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingListgroupbyTwo(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing);
}
