package com.ruoyi.feike.mapper;

import java.util.List;
import com.ruoyi.feike.domain.DealerSectorLimitflag;

/**
 * FlagMapper接口
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public interface DealerSectorLimitflagMapper 
{
    /**
     * 查询Flag
     * 
     * @param id FlagID
     * @return Flag
     */
    public DealerSectorLimitflag selectDealerSectorLimitflagById(String id);

    /**
     * 查询Flag列表
     * 
     * @param dealerSectorLimitflag Flag
     * @return Flag集合
     */
    public List<DealerSectorLimitflag> selectDealerSectorLimitflagList(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 新增Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    public int insertDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 修改Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    public int updateDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 删除Flag
     * 
     * @param id FlagID
     * @return 结果
     */
    public int deleteDealerSectorLimitflagById(String id);

    /**
     * 批量删除Flag
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDealerSectorLimitflagByIds(String[] ids);
}
