package com.ruoyi.feike.instener;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.domain.ParkingLocationRegistration;
import com.ruoyi.feike.domain.TierCashDeposit;
import com.ruoyi.feike.mapper.ParkingLocationMapper;
import com.ruoyi.feike.mapper.ParkingLocationRegistrationMapper;
import com.ruoyi.feike.mapper.TierCashDepositMapper;
import com.ruoyi.feike.service.IAnnualReviewyService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class StartDMEndStateListener implements ExecutionListener {

    @Autowired
    private ParkingLocationRegistrationMapper parkingLocationRegistrationMapper;

    @Autowired
    private ParkingLocationMapper parkingLocationMapper;

    // @Autowired
    // private RepositoryService repositoryService;

    private Expression state;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        AnnualReviewy annualReviewy = new AnnualReviewy();
        String instanceId = delegateExecution.getProcessInstanceBusinessKey();
        String processInstanceBusinessKey = delegateExecution.getProcessInstanceBusinessKey();
        String processDefinitionId = delegateExecution.getProcessDefinitionId();
        // String processInstanceId = delegateExecution.getProcessInstanceId();
        // ProcessInstance processInstance = SpringUtils.getBean(RuntimeService.class).createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();

        ProcessDefinition processDefinition = SpringUtils.getBean(RepositoryService.class).getProcessDefinition(processDefinitionId);
        String key = processDefinition.getKey();
        if (!StringUtils.isEmpty(key) && "parkingLocationregistration".equals(key)){ // 做新增操作
            System.out.println("进入监听器新增");
            List<ParkingLocationRegistration> parkingLocationRegistrations = SpringUtils.getBean(ParkingLocationRegistrationMapper.class).selectParkingLocationRegistrationByInstancsId(instanceId);
            for (ParkingLocationRegistration parkingLocationRegistration : parkingLocationRegistrations) {
                ParkingLocation parkingLocation = new ParkingLocation();
                parkingLocation.setDealernamecn(parkingLocationRegistration.getDealerName());
                // parkingLocation.setParkinglocation(parkingLocationRegistration.getParkinglocation());
                parkingLocation.setParkinglocation(parkingLocationRegistration.getProvinceCN());
                parkingLocation.setType(parkingLocationRegistration.getType());
                parkingLocation.setEffectivedate(parkingLocationRegistration.getStartDate());
                parkingLocation.setDuedate(parkingLocationRegistration.getEndDate());
                parkingLocation.setVin(parkingLocationRegistration.getVin());
                parkingLocation.setDistancefrom4s(parkingLocationRegistration.getDistanceFrom4s());
                parkingLocation.setTel(parkingLocationRegistration.getTel());
                parkingLocation.setTwondtierdeposit(parkingLocationRegistration.getTwondtierdeposit());
                parkingLocation.setNotes(parkingLocationRegistration.getNotes());
                parkingLocation.setStatus("0"); //开启
                parkingLocation.setInstanceId(parkingLocationRegistration.getInstanceId());
                parkingLocation.setId(IdUtils.simpleUUID());
                SpringUtils.getBean(ParkingLocationMapper.class).insertParkingLocation(parkingLocation);
            }
        }
        if (!StringUtils.isEmpty(key) && "tierCashDeposit".equals(processInstanceBusinessKey)){  // 做修改操作
            // 对 ParkingLocation表 的 Dealer Code和2nd-tier deposit gap 做更新操作      根据经销商名称和位置做查询
            TierCashDeposit tierCashDeposit = SpringUtils.getBean(TierCashDepositMapper.class).selectTierCashDepositById(instanceId);
            ParkingLocation parkingLocation = SpringUtils.getBean(ParkingLocationMapper.class).selectParkingLocationByDealerAndParkingLocation(tierCashDeposit.getDealerNameCn(), tierCashDeposit.getParkingLocation());
            if(parkingLocation!=null){
                parkingLocation.setDealercode(tierCashDeposit.getDealerCode());
                parkingLocation.setTierDepositGap(tierCashDeposit.getTierDepositGap());
                SpringUtils.getBean(ParkingLocationMapper.class).updateParkingLocation(parkingLocation);
            }
        }

        annualReviewy.setInstanceId(delegateExecution.getProcessInstanceBusinessKey());
        annualReviewy.setState(state.getValue(delegateExecution).toString());
        SpringUtils.getBean(IAnnualReviewyService.class).updateAnnualReviewyByInstanceId(annualReviewy);
    }
}
