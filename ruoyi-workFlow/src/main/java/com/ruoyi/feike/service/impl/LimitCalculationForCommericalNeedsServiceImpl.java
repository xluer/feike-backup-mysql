package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.LimitCalculationForCommericalNeedsMapper;
import com.ruoyi.feike.domain.LimitCalculationForCommericalNeeds;
import com.ruoyi.feike.service.ILimitCalculationForCommericalNeedsService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-11
 */
@Service
public class LimitCalculationForCommericalNeedsServiceImpl implements ILimitCalculationForCommericalNeedsService 
{
    @Autowired
    private LimitCalculationForCommericalNeedsMapper limitCalculationForCommericalNeedsMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public LimitCalculationForCommericalNeeds selectLimitCalculationForCommericalNeedsById(String id)
    {
        return limitCalculationForCommericalNeedsMapper.selectLimitCalculationForCommericalNeedsById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param limitCalculationForCommericalNeeds feike
     * @return feike
     */
    @Override
    public List<LimitCalculationForCommericalNeeds> selectLimitCalculationForCommericalNeedsList(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        return limitCalculationForCommericalNeedsMapper.selectLimitCalculationForCommericalNeedsList(limitCalculationForCommericalNeeds);
    }

    /**
     * 新增feike
     * 
     * @param limitCalculationForCommericalNeeds feike
     * @return 结果
     */
    @Override
    public int insertLimitCalculationForCommericalNeeds(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        return limitCalculationForCommericalNeedsMapper.insertLimitCalculationForCommericalNeeds(limitCalculationForCommericalNeeds);
    }

    /**
     * 修改feike
     * 
     * @param limitCalculationForCommericalNeeds feike
     * @return 结果
     */
    @Override
    public int updateLimitCalculationForCommericalNeeds(LimitCalculationForCommericalNeeds limitCalculationForCommericalNeeds)
    {
        return limitCalculationForCommericalNeedsMapper.updateLimitCalculationForCommericalNeeds(limitCalculationForCommericalNeeds);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteLimitCalculationForCommericalNeedsByIds(String[] ids)
    {
        return limitCalculationForCommericalNeedsMapper.deleteLimitCalculationForCommericalNeedsByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteLimitCalculationForCommericalNeedsById(String id)
    {
        return limitCalculationForCommericalNeedsMapper.deleteLimitCalculationForCommericalNeedsById(id);
    }
}
