package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-12
 */
public interface IDealerNegativeInformationCheckService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public DealerNegativeInformationCheck selectDealerNegativeInformationCheckById(String id);

    /**
     * 查询feike列表
     * 
     * @param dealerNegativeInformationCheck feike
     * @return feike集合
     */
    public List<DealerNegativeInformationCheck> selectDealerNegativeInformationCheckList(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 新增feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    public int insertDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 修改feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    public int updateDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDealerNegativeInformationCheckByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerNegativeInformationCheckById(String id);

    /**
     * 新增经销商明细表
     *
     * @param  resultMap
     * @return 结果
     */
    public int saveDnic(Map<String, Object> resultMap);
}
