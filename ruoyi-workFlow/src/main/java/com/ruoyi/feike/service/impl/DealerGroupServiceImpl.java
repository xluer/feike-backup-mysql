package com.ruoyi.feike.service.impl;

import java.util.List;

import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.mapper.BasicInformationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerGroupMapper;
import com.ruoyi.feike.domain.DealerGroup;
import com.ruoyi.feike.service.IDealerGroupService;

/**
 * feikeService业务层处理
 *
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class DealerGroupServiceImpl implements IDealerGroupService
{
    @Autowired
    private DealerGroupMapper dealerGroupMapper;
    @Autowired
    private BasicInformationMapper basicInformationMapper;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public DealerGroup selectDealerGroupById(String id)
    {
        return dealerGroupMapper.selectDealerGroupById(id);
    }
    @Override
    public List<BasicInformation> selectDealerGroupByByInstanceId(String instanceId)
    {
        return basicInformationMapper.selectDealerGroupByByInstanceId(instanceId);
    }

    /**
     * 查询feike列表
     *
     * @param dealerGroup feike
     * @return feike
     */
    @Override
    public List<DealerGroup> selectDealerGroupList(DealerGroup dealerGroup)
    {
        return dealerGroupMapper.selectDealerGroupList(dealerGroup);
    }

    /**
     * 新增feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    @Override
    public int insertDealerGroup(DealerGroup dealerGroup)
    {
        return dealerGroupMapper.insertDealerGroup(dealerGroup);
    }

    /**
     * 修改feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    @Override
    public int updateDealerGroup(DealerGroup dealerGroup)
    {
        return dealerGroupMapper.updateDealerGroup(dealerGroup);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerGroupByIds(String[] ids)
    {
        return dealerGroupMapper.deleteDealerGroupByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerGroupById(String id)
    {
        return dealerGroupMapper.deleteDealerGroupById(id);
    }
}
