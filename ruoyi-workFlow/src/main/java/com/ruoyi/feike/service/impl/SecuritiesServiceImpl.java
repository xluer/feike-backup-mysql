package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.SecuritiesMapper;
import com.ruoyi.feike.domain.Securities;
import com.ruoyi.feike.service.ISecuritiesService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-11
 */
@Service
public class SecuritiesServiceImpl implements ISecuritiesService 
{
    @Autowired
    private SecuritiesMapper securitiesMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public Securities selectSecuritiesById(String id)
    {
        return securitiesMapper.selectSecuritiesById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param securities feike
     * @return feike
     */
    @Override
    public List<Securities> selectSecuritiesList(Securities securities)
    {
        return securitiesMapper.selectSecuritiesList(securities);
    }

    /**
     * 新增feike
     * 
     * @param securities feike
     * @return 结果
     */
    @Override
    public int insertSecurities(Securities securities)
    {
        return securitiesMapper.insertSecurities(securities);
    }

    /**
     * 修改feike
     * 
     * @param securities feike
     * @return 结果
     */
    @Override
    public int updateSecurities(Securities securities)
    {
        return securitiesMapper.updateSecurities(securities);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteSecuritiesByIds(String[] ids)
    {
        return securitiesMapper.deleteSecuritiesByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteSecuritiesById(String id)
    {
        return securitiesMapper.deleteSecuritiesById(id);
    }
}
