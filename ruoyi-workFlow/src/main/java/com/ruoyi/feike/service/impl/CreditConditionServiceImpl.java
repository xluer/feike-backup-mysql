package com.ruoyi.feike.service.impl;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.CreditConditionMapper;
import com.ruoyi.feike.domain.CreditCondition;
import com.ruoyi.feike.service.ICreditConditionService;

/**
 * feikeService业务层处理
 * 
 * @author ybw
 * @date 2022-07-12
 */
@Service
public class CreditConditionServiceImpl implements ICreditConditionService 
{
    @Autowired
    private CreditConditionMapper creditConditionMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public CreditCondition selectCreditConditionById(String id)
    {
        return creditConditionMapper.selectCreditConditionById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param creditCondition feike
     * @return feike
     */
    @Override
    public List<CreditCondition> selectCreditConditionList(CreditCondition creditCondition)
    {
        return creditConditionMapper.selectCreditConditionList(creditCondition);
    }

    /**
     * 新增feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    @Override
    public int insertCreditCondition(CreditCondition creditCondition)
    {
        return creditConditionMapper.insertCreditCondition(creditCondition);
    }

    /**
     * 修改feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    @Override
    public int updateCreditCondition(CreditCondition creditCondition)
    {
        return creditConditionMapper.updateCreditCondition(creditCondition);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteCreditConditionByIds(String[] ids)
    {
        return creditConditionMapper.deleteCreditConditionByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteCreditConditionById(String id)
    {
        return creditConditionMapper.deleteCreditConditionById(id);
    }

    /**
     * 新增feike
     *
     * @param resultMap
     * @return 结果
     */
    public int saveCreditcs(Map<String, Object> resultMap){
        try {
            List<CreditCondition> hcc = JSONObject.parseArray(JSON.toJSONString(resultMap.get("hcc")), CreditCondition.class);
            String dealerId = (String) resultMap.get("dealerId");
            String dealerName = (String) resultMap.get("dealerName");
            if (StringUtils.isEmpty(hcc)) {
                throw new BaseException("表数据不能为空！");
            }
            for(CreditCondition alacs : hcc ){
                String id = IdUtils.simpleUUID();
                alacs.setId(id);
                alacs.setDealerid(dealerId);
                alacs.setDealername(dealerName);
                creditConditionMapper.insertCreditCondition(alacs);
            }
            return 1;
        }catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.toString());
        }
    }
}
