package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DetailInformationMapper;
import com.ruoyi.feike.domain.DetailInformation;
import com.ruoyi.feike.service.IDetailInformationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-06
 */
@Service
public class DetailInformationServiceImpl implements IDetailInformationService 
{
    @Autowired
    private DetailInformationMapper detailInformationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public DetailInformation selectDetailInformationById(String id)
    {
        return detailInformationMapper.selectDetailInformationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param detailInformation feike
     * @return feike
     */
    @Override
    public List<DetailInformation> selectDetailInformationList(DetailInformation detailInformation)
    {
        return detailInformationMapper.selectDetailInformationList(detailInformation);
    }

    /**
     * 新增feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    @Override
    public int insertDetailInformation(DetailInformation detailInformation)
    {
        return detailInformationMapper.insertDetailInformation(detailInformation);
    }

    /**
     * 修改feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    @Override
    public int updateDetailInformation(DetailInformation detailInformation)
    {
        return detailInformationMapper.updateDetailInformation(detailInformation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDetailInformationByIds(String[] ids)
    {
        return detailInformationMapper.deleteDetailInformationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDetailInformationById(String id)
    {
        return detailInformationMapper.deleteDetailInformationById(id);
    }
}
