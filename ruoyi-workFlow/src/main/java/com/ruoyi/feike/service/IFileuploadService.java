package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.Fileupload;

import java.util.List;

/**
 * feikeService接口
 *
 * @author ruoyi
 * @date 2022-07-22
 */
public interface IFileuploadService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public Fileupload selectFileuploadById(String id);

    public List<Fileupload> selectFileuploadByInstanceId(String instanceId);

    /**
     * 查询feike列表
     *
     * @param fileupload feike
     * @return feike集合
     */
    public List<Fileupload> selectFileuploadList(Fileupload fileupload);

    /**
     * 新增feike
     *
     * @param fileupload feike
     * @return 结果
     */
    public int insertFileupload(Fileupload fileupload);

    /**
     * 修改feike
     *
     * @param fileupload feike
     * @return 结果
     */
    public int updateFileupload(Fileupload fileupload);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteFileuploadByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteFileuploadById(String id);

    List<Fileupload> selectFileuploadListByInstanceId(String instanceId);

    public int delFileUrl(String fileName);
}
