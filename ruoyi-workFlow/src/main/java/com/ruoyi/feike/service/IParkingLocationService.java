package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.domain.vo.ParkingLocationVO;

import java.util.List;
import java.util.Map;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-07-05
 */
public interface IParkingLocationService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public ParkingLocation selectParkingLocationById(String id);

    /**
     * 查询feike列表
     *
     * @param parkingLocation feike
     * @return feike集合
     */
    public List<ParkingLocation> selectParkingLocationList(ParkingLocation parkingLocation);

    public List<ParkingLocationVO> selectParkingLocationListANDplm(ParkingLocationVO parkingLocation);

    /**
     * 新增feike
     *
     * @param parkingLocation feike
     * @return 结果
     */
    public int insertParkingLocation(ParkingLocation parkingLocation);

    /**
     * 修改feike
     *
     * @param parkingLocation feike
     * @return 结果
     */
    public int updateParkingLocation(ParkingLocation parkingLocation);

    /**
     * 根据业务规则修改status状态
     *
     * @param
     * @return 结果
     */
    public int updateParkingLocationStatus();

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteParkingLocationByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteParkingLocationById(String id);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<ParkingLocation> userList, Boolean isUpdateSupport, String operName);

    List<Map> selectParkingLocationLists();

    Map getFlagByDealerNameAndType(Map map);
}
