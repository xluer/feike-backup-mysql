package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.OtherFinancingResourceMapper;
import com.ruoyi.feike.domain.OtherFinancingResource;
import com.ruoyi.feike.service.IOtherFinancingResourceService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-11
 */
@Service
public class OtherFinancingResourceServiceImpl implements IOtherFinancingResourceService 
{
    @Autowired
    private OtherFinancingResourceMapper otherFinancingResourceMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public OtherFinancingResource selectOtherFinancingResourceById(String id)
    {
        return otherFinancingResourceMapper.selectOtherFinancingResourceById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param otherFinancingResource feike
     * @return feike
     */
    @Override
    public List<OtherFinancingResource> selectOtherFinancingResourceList(OtherFinancingResource otherFinancingResource)
    {
        return otherFinancingResourceMapper.selectOtherFinancingResourceList(otherFinancingResource);
    }

    /**
     * 新增feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    @Override
    public int insertOtherFinancingResource(OtherFinancingResource otherFinancingResource)
    {
        return otherFinancingResourceMapper.insertOtherFinancingResource(otherFinancingResource);
    }

    /**
     * 修改feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    @Override
    public int updateOtherFinancingResource(OtherFinancingResource otherFinancingResource)
    {
        return otherFinancingResourceMapper.updateOtherFinancingResource(otherFinancingResource);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteOtherFinancingResourceByIds(String[] ids)
    {
        return otherFinancingResourceMapper.deleteOtherFinancingResourceByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteOtherFinancingResourceById(String id)
    {
        return otherFinancingResourceMapper.deleteOtherFinancingResourceById(id);
    }
}
