package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DetailInformation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-06
 */
public interface IDetailInformationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public DetailInformation selectDetailInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param detailInformation feike
     * @return feike集合
     */
    public List<DetailInformation> selectDetailInformationList(DetailInformation detailInformation);

    /**
     * 新增feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    public int insertDetailInformation(DetailInformation detailInformation);

    /**
     * 修改feike
     * 
     * @param detailInformation feike
     * @return 结果
     */
    public int updateDetailInformation(DetailInformation detailInformation);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDetailInformationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteDetailInformationById(String id);
}
