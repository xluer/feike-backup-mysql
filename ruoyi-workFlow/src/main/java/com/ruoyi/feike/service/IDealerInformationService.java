package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.domain.vo.DealerformationVO;

/**
 * feikeService接口
 *
 * @author ybw
 * @date 2022-07-12
 */
public interface IDealerInformationService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerInformation selectDealerInformationById(String id);

    DealerformationVO selectDealerInformationByname(Map<String, Object> map);

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike集合
     */
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation);

    List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation);

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int insertDealerInformation(DealerInformation dealerInformation);

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    public int updateDealerInformation(DealerInformation dealerInformation);

    public int updateDealerInformationnew(DealerformationVO dealerInformation);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDealerInformationByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerInformationById(String id);

    public List<DealerInformation> getGroups();
    public List<DealerInformation> getDealers(String groupName);

    List<DealerformationVO> listByName(List<DealerInformation> params);
}
