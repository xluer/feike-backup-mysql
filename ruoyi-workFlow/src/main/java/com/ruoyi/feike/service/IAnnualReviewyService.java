package com.ruoyi.feike.service;

import com.github.pagehelper.Page;
import com.ruoyi.activiti.domain.dto.ActTaskDTO;
import com.ruoyi.activiti.domain.vo.SearchVo;
import com.ruoyi.common.core.page.CustomPage;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.domain.vo.AnnualReviewyVO;

import java.util.List;
import java.util.Map;

/**
 * feikeService接口
 *
 * @author ruoyi
 * @date 2022-07-07
 */
public interface IAnnualReviewyService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public Map<String,Object> selectAnnualReviewyById(String id);

    /**
     * 查询feike列表
     *
     * @param annualReviewy feike
     * @return feike集合
     */
    public List<AnnualReviewyHistoryVO> selectAnnualReviewyList(AnnualReviewy annualReviewy, BasicInformation basicInformation);

    /**
     * 新增feike
     *
     * @param map feike
     * @return 结果
     */
    public int insertAnnualReviewy(Map<String, Object> map);

    /**
     * 修改feike
     *
     * @param map feike
     * @return 结果
     */
    public int updateAnnualReviewy(Map<String, Object> map);
    public int updateAnnualReviewyByInstanceId(AnnualReviewy annualReviewy);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteAnnualReviewyById(String id);

    List<AnnualReviewy> selectAnnualReviewyAndTaskNameList(AnnualReviewy annualReviewy);

    int startDM(Map<String, Object> map);

    int startPLM(Map<String, Object> map);

    int dmSelect(Map<String, Object> map);

    Map<String,Object> selectAnnualReviewyByInstanceId(String instanceId);

    Map<String,Object> selectMailInfoById(String id);

    Map<String,Object> selectMailInfo(Map<String, Object> map);

    Map<String,Object> selectMailInfoByInstanceId(String instanceId);

    int updateMain(Map<String, Object> map);

    DealerNegativeInformationCheck getMain(String taskId);

    List<AnnualReviewyVO> selectAnnualReviewyVOList(AnnualReviewy annualReviewy);

    Page<ActTaskDTO> selectProcessDefinitionSearchList(SearchVo searchVo, PageDomain pageDomain);

    CustomPage indexData();

    int closeUpdateAnnualReviewyObj(String instanceId,String username);

    int openUpdateAnnualReviewyObj(String username);

    List getInfoListByDealerName(String dealerName);
}
