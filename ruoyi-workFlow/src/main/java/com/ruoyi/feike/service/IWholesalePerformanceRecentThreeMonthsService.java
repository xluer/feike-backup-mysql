package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.WholesalePerformanceRecentThreeMonths;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-05
 */
public interface IWholesalePerformanceRecentThreeMonthsService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public WholesalePerformanceRecentThreeMonths selectWholesalePerformanceRecentThreeMonthsById(String id);

    /**
     * 查询feike列表
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return feike集合
     */
    public List<WholesalePerformanceRecentThreeMonths> selectWholesalePerformanceRecentThreeMonthsList(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 新增feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    public int insertWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 修改feike
     * 
     * @param wholesalePerformanceRecentThreeMonths feike
     * @return 结果
     */
    public int updateWholesalePerformanceRecentThreeMonths(WholesalePerformanceRecentThreeMonths wholesalePerformanceRecentThreeMonths);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteWholesalePerformanceRecentThreeMonthsByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteWholesalePerformanceRecentThreeMonthsById(String id);
}
