package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.GuaranteeInformationMapper;
import com.ruoyi.feike.domain.GuaranteeInformation;
import com.ruoyi.feike.service.IGuaranteeInformationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-14
 */
@Service
public class GuaranteeInformationServiceImpl implements IGuaranteeInformationService 
{
    @Autowired
    private GuaranteeInformationMapper guaranteeInformationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public GuaranteeInformation selectGuaranteeInformationById(String id)
    {
        return guaranteeInformationMapper.selectGuaranteeInformationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param guaranteeInformation feike
     * @return feike
     */
    @Override
    public List<GuaranteeInformation> selectGuaranteeInformationList(GuaranteeInformation guaranteeInformation)
    {
        return guaranteeInformationMapper.selectGuaranteeInformationList(guaranteeInformation);
    }

    /**
     * 新增feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    @Override
    public int insertGuaranteeInformation(GuaranteeInformation guaranteeInformation)
    {
        return guaranteeInformationMapper.insertGuaranteeInformation(guaranteeInformation);
    }

    /**
     * 修改feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    @Override
    public int updateGuaranteeInformation(GuaranteeInformation guaranteeInformation)
    {
        return guaranteeInformationMapper.updateGuaranteeInformation(guaranteeInformation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteGuaranteeInformationByIds(String[] ids)
    {
        return guaranteeInformationMapper.deleteGuaranteeInformationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteGuaranteeInformationById(String id)
    {
        return guaranteeInformationMapper.deleteGuaranteeInformationById(id);
    }
}
