package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.feike.domain.CreditCondition;

/**
 * feikeService接口
 * 
 * @author ybw
 * @date 2022-07-12
 */
public interface ICreditConditionService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public CreditCondition selectCreditConditionById(String id);

    /**
     * 查询feike列表
     * 
     * @param creditCondition feike
     * @return feike集合
     */
    public List<CreditCondition> selectCreditConditionList(CreditCondition creditCondition);

    /**
     * 新增feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    public int insertCreditCondition(CreditCondition creditCondition);

    /**
     * 修改feike
     * 
     * @param creditCondition feike
     * @return 结果
     */
    public int updateCreditCondition(CreditCondition creditCondition);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteCreditConditionByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteCreditConditionById(String id);

    /**
     * 新增feike
     *
     * @param resultMap
     * @return 结果
     */
    public int saveCreditcs(Map<String, Object> resultMap);
}
