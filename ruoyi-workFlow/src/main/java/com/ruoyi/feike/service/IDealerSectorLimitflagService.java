package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DealerSectorLimitflag;

/**
 * FlagService接口
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
public interface IDealerSectorLimitflagService 
{
    /**
     * 查询Flag
     * 
     * @param id FlagID
     * @return Flag
     */
    public DealerSectorLimitflag selectDealerSectorLimitflagById(String id);

    /**
     * 查询Flag列表
     * 
     * @param dealerSectorLimitflag Flag
     * @return Flag集合
     */
    public List<DealerSectorLimitflag> selectDealerSectorLimitflagList(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 新增Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    public int insertDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 修改Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    public int updateDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag);

    /**
     * 批量删除Flag
     * 
     * @param ids 需要删除的FlagID
     * @return 结果
     */
    public int deleteDealerSectorLimitflagByIds(String[] ids);

    /**
     * 删除Flag信息
     * 
     * @param id FlagID
     * @return 结果
     */
    public int deleteDealerSectorLimitflagById(String id);
}
