package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.SalesPerformanceAndTarget;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-07-11
 */
public interface ISalesPerformanceAndTargetService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public SalesPerformanceAndTarget selectSalesPerformanceAndTargetById(String id);

    /**
     * 查询feike列表
     *
     * @param salesPerformanceAndTarget feike
     * @return feike集合
     */
    public List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetList(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 新增feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    public int insertSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 修改feike
     *
     * @param salesPerformanceAndTarget feike
     * @return 结果
     */
    public int updateSalesPerformanceAndTarget(SalesPerformanceAndTarget salesPerformanceAndTarget);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteSalesPerformanceAndTargetByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteSalesPerformanceAndTargetById(String id);

    public List<SalesPerformanceAndTarget> selectSalesPerformanceAndTargetByInstanceId(String instanceId);
}
