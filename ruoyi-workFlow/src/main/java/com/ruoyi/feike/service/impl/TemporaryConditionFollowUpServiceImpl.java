package com.ruoyi.feike.service.impl;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.TemporaryConditionFollowUpMapper;
import com.ruoyi.feike.domain.TemporaryConditionFollowUp;
import com.ruoyi.feike.service.ITemporaryConditionFollowUpService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-12
 */
@Service
public class TemporaryConditionFollowUpServiceImpl implements ITemporaryConditionFollowUpService 
{
    @Autowired
    private TemporaryConditionFollowUpMapper temporaryConditionFollowUpMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public TemporaryConditionFollowUp selectTemporaryConditionFollowUpById(String id)
    {
        return temporaryConditionFollowUpMapper.selectTemporaryConditionFollowUpById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param temporaryConditionFollowUp feike
     * @return feike
     */
    @Override
    public List<TemporaryConditionFollowUp> selectTemporaryConditionFollowUpList(TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        return temporaryConditionFollowUpMapper.selectTemporaryConditionFollowUpList(temporaryConditionFollowUp);
    }

    /**
     * 新增feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    @Override
    public int insertTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        return temporaryConditionFollowUpMapper.insertTemporaryConditionFollowUp(temporaryConditionFollowUp);
    }

    /**
     * 修改feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    @Override
    public int updateTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp)
    {
        return temporaryConditionFollowUpMapper.updateTemporaryConditionFollowUp(temporaryConditionFollowUp);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteTemporaryConditionFollowUpByIds(String[] ids)
    {
        return temporaryConditionFollowUpMapper.deleteTemporaryConditionFollowUpByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteTemporaryConditionFollowUpById(String id)
    {
        return temporaryConditionFollowUpMapper.deleteTemporaryConditionFollowUpById(id);
    }

    /**
     * 新增temporaryConditionFollowUp
     *
     * @param resultMap
     * @return 结果
     */
    @Override
    public int saveTcfu(Map<String, Object> resultMap){
        try {
            List<TemporaryConditionFollowUp> htcfu = JSONObject.parseArray(JSON.toJSONString(resultMap.get("htcfu")), TemporaryConditionFollowUp.class);
            String dealerId = (String) resultMap.get("dealerId");
            String dealerName = (String) resultMap.get("dealerName");
            if (StringUtils.isEmpty(htcfu)) {
                throw new BaseException("表数据不能为空！");
            }
            for(TemporaryConditionFollowUp alacs : htcfu ){
                String id = IdUtils.simpleUUID();
                alacs.setId(id);
                alacs.setDealerId(dealerId);
                alacs.setDealername(dealerName);
                temporaryConditionFollowUpMapper.insertTemporaryConditionFollowUp(alacs);
            }
            return 1;
        }catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.toString());
        }
    }
}
