package com.ruoyi.feike.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.feike.domain.DepositDecreaseDTO;
import com.ruoyi.feike.domain.NewApplicationDTO;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;
import java.util.Map;


/**
 * @author lss
 * @version 1.0
 * @description: 新经销商申请接口类
 * @date 2022/8/2 15:07
 */
public interface IDepositDecreaseService {
    
    /**
    * @description: 新增降低保证金比例申请审批流程接口
    * @param: newApplicationDTO
    * @return: AjaxResult
    * @author lss
    * @date: 2022/8/2 15:15
    */
    AjaxResult insertActApprove(NewApplicationDTO newApplicationDTO);

    List<Map<String,Object>> getForignList(DepositDecreaseDTO depositDecreaseDTO);
}
