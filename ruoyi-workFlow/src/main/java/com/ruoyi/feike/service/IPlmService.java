package com.ruoyi.feike.service;

import java.util.List;

import com.ruoyi.feike.domain.ParkingLocation;
import com.ruoyi.feike.domain.Plm;

/**
 * plm导入表Service接口
 *
 * @author ruoyi
 * @date 2022-07-21
 */
public interface IPlmService
{
    /**
     * 查询plm导入表
     *
     * @param id plm导入表ID
     * @return plm导入表
     */
    public Plm selectPlmById(String id);

    /**
     * 查询plm导入表列表
     *
     * @param plm plm导入表
     * @return plm导入表集合
     */
    public List<Plm> selectPlmList(Plm plm);

    /**
     * 新增plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    public int insertPlm(Plm plm);

    /**
     * 修改plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    public int updatePlm(Plm plm);

    /**
     * 批量删除plm导入表
     *
     * @param ids 需要删除的plm导入表ID
     * @return 结果
     */
    public int deletePlmByIds(String[] ids);

    /**
     * 删除plm导入表信息
     *
     * @param id plm导入表ID
     * @return 结果
     */
    public int deletePlmById(String id);

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<Plm> userList, Boolean isUpdateSupport, String operName);
}
