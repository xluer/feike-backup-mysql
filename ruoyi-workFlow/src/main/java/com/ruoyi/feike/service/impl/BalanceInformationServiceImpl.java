package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.BalanceInformationMapper;
import com.ruoyi.feike.domain.BalanceInformation;
import com.ruoyi.feike.service.IBalanceInformationService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-05
 */
@Service
public class BalanceInformationServiceImpl implements IBalanceInformationService 
{
    @Autowired
    private BalanceInformationMapper balanceInformationMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public BalanceInformation selectBalanceInformationById(String id)
    {
        return balanceInformationMapper.selectBalanceInformationById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param balanceInformation feike
     * @return feike
     */
    @Override
    public List<BalanceInformation> selectBalanceInformationList(BalanceInformation balanceInformation)
    {
        return balanceInformationMapper.selectBalanceInformationList(balanceInformation);
    }

    /**
     * 新增feike
     * 
     * @param balanceInformation feike
     * @return 结果
     */
    @Override
    public int insertBalanceInformation(BalanceInformation balanceInformation)
    {
        return balanceInformationMapper.insertBalanceInformation(balanceInformation);
    }

    /**
     * 修改feike
     * 
     * @param balanceInformation feike
     * @return 结果
     */
    @Override
    public int updateBalanceInformation(BalanceInformation balanceInformation)
    {
        return balanceInformationMapper.updateBalanceInformation(balanceInformation);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteBalanceInformationByIds(String[] ids)
    {
        return balanceInformationMapper.deleteBalanceInformationByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteBalanceInformationById(String id)
    {
        return balanceInformationMapper.deleteBalanceInformationById(id);
    }
}
