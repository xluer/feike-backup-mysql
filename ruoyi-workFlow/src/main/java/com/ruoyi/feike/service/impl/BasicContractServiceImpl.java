package com.ruoyi.feike.service.impl;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.vo.BasicContractVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.BasicContractMapper;
import com.ruoyi.feike.domain.BasicContract;
import com.ruoyi.feike.service.IBasicContractService;

/**
 * 合同信息Service业务层处理
 *
 * @author ybw
 * @date 2022-07-19
 */
@Service
public class BasicContractServiceImpl implements IBasicContractService
{
    @Autowired
    private BasicContractMapper basicContractMapper;

    /**
     * 查询合同信息
     *
     * @param id 合同信息ID
     * @return 合同信息
     */
    @Override
    public BasicContract selectBasicContractById(String id)
    {
        return basicContractMapper.selectBasicContractById(id);
    }

    /**
     * 查询合同信息列表
     *
     * @param basicContract 合同信息
     * @return 合同信息
     */
    @Override
    public List<BasicContract> selectBasicContractList(BasicContract basicContract)
    {
        return basicContractMapper.selectBasicContractList(basicContract);
    }

    /**
     * 查询合同信息列表并根据合同名称主机厂分组
     *
     * @param basicContract 合同信息
     * @return 合同信息
     */
    @Override
    public List<BasicContract> selectBasicContractListgroupby(BasicContract basicContract)
    {
        return basicContractMapper.selectBasicContractListgroupby(basicContract);
    }

    /**
     * 新增合同信息
     *
     * @param basicContract 合同信息
     * @return 结果
     */
    @Override
    public int insertBasicContract(BasicContract basicContract)
    {
        String location = "/profile/template/"+basicContract.getSector()+"/"+basicContract.getContractname()+".docx";
        basicContract.setId(IdUtils.simpleUUID());
        basicContract.setContractlocation(location);
        return basicContractMapper.insertBasicContract(basicContract);
    }

    /**
     * 修改合同信息
     *
     * @param basicContract 合同信息
     * @return 结果
     */
    @Override
    public int updateBasicContract(BasicContract basicContract)
    {
        return basicContractMapper.updateBasicContract(basicContract);
    }

    /**
     * 批量删除合同信息
     *
     * @param ids 需要删除的合同信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractByIds(String[] ids)
    {
        return basicContractMapper.deleteBasicContractByIds(ids);
    }

    /**
     * 删除合同信息信息
     *
     * @param id 合同信息ID
     * @return 结果
     */
    @Override
    public int deleteBasicContractById(String id)
    {
        return basicContractMapper.deleteBasicContractById(id);
    }

    /**
     * 根据优先级、主机厂、授信类型查询对应合同查询合同信息列表
     *
     * @param resultMap
     * @return 合同信息
     */
    @Override
    public List<BasicContract> listBasicContract(Map<String, Object> resultMap)
    {
        return basicContractMapper.listBasicContract(resultMap);
    }

    /**
     * 修改分类后的合同信息
     *
     * @param basicContractVO
     * @return 结果
     */
    @Override
    public int updateBasicContractgroupby(BasicContractVO basicContractVO)
    {
//        BasicContract basiccontract = JSONObject.parseObject(JSON.toJSONString(resultMap.get("basiccontract")),BasicContract.class);
//        String contractLocations = (String) resultMap.get("contractLocations");
//        BasicContractVO basicContractVO = new BasicContractVO();
//        BeanUtils.copyProperties();
        return basicContractMapper.updateBasicContractgroupby(basicContractVO);
    }
}
