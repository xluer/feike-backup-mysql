package com.ruoyi.feike.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.*;
import com.ruoyi.feike.mapper.AnnualReviewyMapper;
import com.ruoyi.feike.service.*;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author lss
 * @version 1.0
 * @description: 新增降低保证金比例申请审批流程接口
 * @date 2022/8/2 16:13
 */
@Service
public class DepositDecreaseServiceImpl implements IDepositDecreaseService {

    private static final String INSTEANCEID = "depositDecrease";

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProcessRuntime processRuntime;

    @Autowired
    private IBasicInformationService basicInformationService;

    @Autowired
    private IDealerNegativeInformationCheckService dealerNegativeInformationCheckService;

    @Autowired
    private ILimitCalculationForCommericalNeedsService limitCalculationForCommericalNeedsService;

    @Autowired
    private IOtherFinancingResourceService otherFinancingResourceService;

    @Autowired
    private ITemporaryConditionFollowUpService temporaryConditionFollowUpService;

    @Autowired
    private ISecuritiesService securitiesService;

    @Autowired
    private AnnualReviewyMapper annualReviewyMapper;
    /**
     * @param newApplicationDTO
     * @description: 新经销商申请审批流程接口
     * @param: newApplicationDTO
     * @return: AjaxResult
     * @author lss
     * @date: 2022/8/2 15:15
     */
    @Override
    public AjaxResult insertActApprove(NewApplicationDTO newApplicationDTO) {

        String processDefinitionKey = newApplicationDTO.getProcessDefinitionKey();
        //生成流程task
        String instanceId = IdUtils.simpleUUID();
        Deployment deployment = repositoryService.createDeployment().key(newApplicationDTO.getInstanceId()).deploy();
//        String title = SecurityUtils.getLoginUser().getUser().getNickName()+"-"+deployment.getName();
        String title = SecurityUtils.getLoginUser().getUser().getNickName()+"-"+newApplicationDTO.getInstanceName();
        Authentication.setAuthenticatedUserId(String.valueOf(SecurityUtils.getLoginUser().getUser().getUserId()));
        ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder.start().withProcessDefinitionKey(newApplicationDTO.getInstanceId()).withName(title).withBusinessKey(instanceId).build());
        //查询任务信息
        Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        TaskQuery query = taskService.createTaskQuery().taskCandidateOrAssigned(SecurityUtils.getUsername()).active();
        List<Task> todoList = query.list();//获取申请人的待办任务列表
        for (Task tmp : todoList) {
            if (tmp.getProcessInstanceId().equals(processInstance.getId())) {
                task = tmp;//获取当前流程实例，当前申请人的待办任务
                break;
            }
        }
        Map<String,Object> map=new HashMap<>();
        HashMap<String, Object> variables = new HashMap<String, Object>();
        for (ActFormDTO form : newApplicationDTO.getActForm()) {
            variables.put(String.valueOf(form.getControlId()), form.getControlValue());
        }
        variables.put("toUnderwriter", 0);
        taskService.complete(task.getId(), variables);

        //插入基本信息
        int basicInfoFlag=1;
        if (StringUtils.isNotEmpty(newApplicationDTO.getDealername())){
            String sectorStr=null;
            for (BasicInformationDTO basicInformation:newApplicationDTO.getDealername()){
                basicInformation.setId(IdUtils.simpleUUID());
                if (StringUtils.isNotEmpty(basicInformation.getSector())){
                    for (String sector:basicInformation.getSector()){
                        sectorStr+=sector+",";
                    }
                    sectorStr.substring(0,sectorStr.length()-1);
                    basicInformation.setSectorStr(sectorStr);
                }
                basicInfoFlag=basicInformationService.insertBasicInformationByDto(basicInformation);
            }
        }
        //经销商信息
        int dealerInfoFlag=1;
        if (basicInfoFlag==1&& StringUtils.isNotEmpty(newApplicationDTO.getDealerInformationList())){
            for (DealerNegativeInformationCheck dealerNegativeInformationCheck:newApplicationDTO.getDealerInformationList()){
                dealerNegativeInformationCheck.setId(IdUtils.simpleUUID());
                dealerInfoFlag=dealerNegativeInformationCheckService.insertDealerNegativeInformationCheck(dealerNegativeInformationCheck);
            }
        }
        //销售业绩及目标
        int salstargetFlag=1;
        if (dealerInfoFlag==1&&StringUtils.isNotEmpty(newApplicationDTO.getLimit_calculation_for_commerical_needs())){
            for (LimitCalculationForCommericalNeeds limitCalculationForCommericalNeedsm:newApplicationDTO.getLimit_calculation_for_commerical_needs()){
                limitCalculationForCommericalNeedsm.setId(IdUtils.simpleUUID());
                salstargetFlag=limitCalculationForCommericalNeedsService.insertLimitCalculationForCommericalNeeds(limitCalculationForCommericalNeedsm);
            }
        }
        //经销商其他相关贷款信息
        int otherFinancFlag=1;
        if (salstargetFlag==1&&StringUtils.isNotEmpty(newApplicationDTO.getOther_financing_resource())){
            for (OtherFinancingResource otherFinancingResource:newApplicationDTO.getOther_financing_resource()){
                otherFinancingResource.setId(IdUtils.simpleUUID());
                otherFinancFlag=otherFinancingResourceService.insertOtherFinancingResource(otherFinancingResource);
            }
        }
        //经销商创建相关信息
        int temporaryFlag=1;
        if (otherFinancFlag==1&&StringUtils.isNotEmpty(newApplicationDTO.getTemporaryConditionFollowUpList())){
            for (TemporaryConditionFollowUp temporaryConditionFollowUp:newApplicationDTO.getTemporaryConditionFollowUpList()){
                temporaryConditionFollowUp.setId(IdUtils.simpleUUID());
                temporaryFlag=temporaryConditionFollowUpService.insertTemporaryConditionFollowUp(temporaryConditionFollowUp);
            }
        }
        //担保信息
        int securityFlag=1;
        if (temporaryFlag==1&&StringUtils.isNotEmpty(newApplicationDTO.getTemporaryConditionFollowUpList())){
            for ( Securities securities:newApplicationDTO.getSecuritieList()){
                securities.setId(IdUtils.simpleUUID());
                securityFlag=securitiesService.insertSecurities(securities);
            }
        }
        AnnualReviewy annualReviewy = new AnnualReviewy();
        annualReviewy.setId(IdUtils.simpleUUID());
        annualReviewy.setTitle(title);
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        annualReviewy.setInstanceId(instanceId);
        annualReviewy.setState("0");
        annualReviewy.setType(newApplicationDTO.getInstanceId());
        annualReviewy.setCreateName(SecurityUtils.getNickName());
        annualReviewy.setCreateBy(SecurityUtils.getUsername());
        annualReviewy.setCreateTime(DateUtils.getNowDate());
        annualReviewyMapper.insertAnnualReviewy(annualReviewy);
        return AjaxResult.success("添加新经销商申请成功!");
    }

    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public List<Map<String, Object>> getForignList(DepositDecreaseDTO depositDecreaseDTO) {
        String[] yearMonthArr=this.getPreYearMonth();
        depositDecreaseDTO.setYearMonth(yearMonthArr);
        depositDecreaseDTO.setMinYearMonth(yearMonthArr[0]);
        depositDecreaseDTO.setMaxYearMonth(yearMonthArr[2]);
        String pivotStr="";
        String fieldStr="";
        List<Map<String,Object>> resultList=new ArrayList<>();
        if ("0".equals(depositDecreaseDTO.getIsDso())){
            pivotStr="pivot ( MAX(DSO) for PERIOD in (";
            for (int i=0;i<yearMonthArr.length;i++){
                pivotStr+=yearMonthArr[i]+",";
                fieldStr+= "DECODE(\""+yearMonthArr[i]+"\","+null+",0,"+"\""+yearMonthArr[i]+"\") AS \""+yearMonthArr[i]+"\",";
            }
            pivotStr=pivotStr.substring(0,pivotStr.length()-1);
            fieldStr=fieldStr.substring(0,fieldStr.length()-1);
            pivotStr+="))";
            depositDecreaseDTO.setPivotStr(pivotStr);
            depositDecreaseDTO.setFieldStr(fieldStr);
            resultList=annualReviewyMapper.getForeignDsoList(depositDecreaseDTO);
        }else{
            pivotStr="pivot ( MAX(AGING) for PERIOD in (";
            for (int i=0;i<yearMonthArr.length;i++){
                pivotStr+=yearMonthArr[i]+",";
                fieldStr+= "DECODE(\""+yearMonthArr[i]+"\","+null+",0,"+"\""+yearMonthArr[i]+"\") AS \""+yearMonthArr[i]+"\",";
            }
            pivotStr=pivotStr.substring(0,pivotStr.length()-1);
            fieldStr=fieldStr.substring(0,fieldStr.length()-1);
            pivotStr+="))";
            depositDecreaseDTO.setPivotStr(pivotStr);
            depositDecreaseDTO.setFieldStr(fieldStr);
            resultList=annualReviewyMapper.getForeignAingList(depositDecreaseDTO);
        }
        return resultList;
    }

    /**
    * @description: 获取当前年月往前推两个月月份
    * @param:
    * @return:
    * @author lss
    * @date: 2022/8/11 10:42
    */
    private String[] getPreYearMonth(){
        String[] last12Months = new String[3];
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1); //要先+1,才能把本月的算进去
        cal.set(Calendar.DATE,1);
        for(int i=0; i<3; i++){
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1); //逐次往前推1个月
            if (cal.get(Calendar.MONTH)+1<10){
                last12Months[2-i] = cal.get(Calendar.YEAR)+ "0" + (cal.get(Calendar.MONTH)+1);
            }else {
                last12Months[2-i] = String.valueOf(cal.get(Calendar.YEAR))+ String.valueOf(cal.get(Calendar.MONTH)+1);
            }
        }
        return last12Months;
    }
}
