package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.GuaranteeInformation;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-14
 */
public interface IGuaranteeInformationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public GuaranteeInformation selectGuaranteeInformationById(String id);

    /**
     * 查询feike列表
     * 
     * @param guaranteeInformation feike
     * @return feike集合
     */
    public List<GuaranteeInformation> selectGuaranteeInformationList(GuaranteeInformation guaranteeInformation);

    /**
     * 新增feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    public int insertGuaranteeInformation(GuaranteeInformation guaranteeInformation);

    /**
     * 修改feike
     * 
     * @param guaranteeInformation feike
     * @return 结果
     */
    public int updateGuaranteeInformation(GuaranteeInformation guaranteeInformation);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteGuaranteeInformationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteGuaranteeInformationById(String id);
}
