package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.OtherFinancingResource;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-11
 */
public interface IOtherFinancingResourceService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public OtherFinancingResource selectOtherFinancingResourceById(String id);

    /**
     * 查询feike列表
     * 
     * @param otherFinancingResource feike
     * @return feike集合
     */
    public List<OtherFinancingResource> selectOtherFinancingResourceList(OtherFinancingResource otherFinancingResource);

    /**
     * 新增feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    public int insertOtherFinancingResource(OtherFinancingResource otherFinancingResource);

    /**
     * 修改feike
     * 
     * @param otherFinancingResource feike
     * @return 结果
     */
    public int updateOtherFinancingResource(OtherFinancingResource otherFinancingResource);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteOtherFinancingResourceByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteOtherFinancingResourceById(String id);
}
