package com.ruoyi.feike.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.CustomException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.ParkingLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.PlmMapper;
import com.ruoyi.feike.domain.Plm;
import com.ruoyi.feike.service.IPlmService;

/**
 * plm导入表Service业务层处理
 *
 * @author ruoyi
 * @date 2022-07-21
 */
@Service
public class PlmServiceImpl implements IPlmService
{
    private static final Logger log = LoggerFactory.getLogger(ParkingLocationServiceImpl.class);
    @Autowired
    private PlmMapper plmMapper;

    /**
     * 查询plm导入表
     *
     * @param id plm导入表ID
     * @return plm导入表
     */
    @Override
    public Plm selectPlmById(String id)
    {
        return plmMapper.selectPlmById(id);
    }

    /**
     * 查询plm导入表列表
     *
     * @param plm plm导入表
     * @return plm导入表
     */
    @Override
    public List<Plm> selectPlmList(Plm plm)
    {
        List<Plm> plms = plmMapper.selectPlmList(plm);
        for (Plm plm1 : plms){
            if(StringUtils.isNotEmpty(plm1.getMonth())) {
                String month = plm1.getMonth();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM");
                try {
                    Date parse = simpleDateFormat.parse(month);
                    String format = simpleDateFormat1.format(parse);
                    plm1.setMonth(format);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return plms;
    }

    /**
     * 新增plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    @Override
    public int insertPlm(Plm plm)
    {
        return plmMapper.insertPlm(plm);
    }

    /**
     * 修改plm导入表
     *
     * @param plm plm导入表
     * @return 结果
     */
    @Override
    public int updatePlm(Plm plm)
    {
        return plmMapper.updatePlm(plm);
    }

    /**
     * 批量删除plm导入表
     *
     * @param ids 需要删除的plm导入表ID
     * @return 结果
     */
    @Override
    public int deletePlmByIds(String[] ids)
    {
        return plmMapper.deletePlmByIds(ids);
    }

    /**
     * 删除plm导入表信息
     *
     * @param id plm导入表ID
     * @return 结果
     */
    @Override
    public int deletePlmById(String id)
    {
        return plmMapper.deletePlmById(id);
    }

    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<Plm> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new CustomException("导入经销商信息数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        // 导入前清表一次
        plmMapper.deletePlmBy();
        for (Plm stu : userList)
        {
            try
            {
                stu.setId(IdUtils.simpleUUID());
                this.insertPlm(stu);
                successNum++;
                successMsg.append("<br/>" + successNum + "、经销商 " + stu.getDealernamecn() + " 导入成功");

            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、经销商 " + stu.getDealernamecn() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
