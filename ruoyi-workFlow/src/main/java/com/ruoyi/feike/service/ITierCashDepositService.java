package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.TierCashDeposit;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;
import com.ruoyi.feike.domain.vo.ParkingLocationVO1;

import java.util.List;
import java.util.Map;

/**
 * feikeService接口
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public interface ITierCashDepositService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Map<String, Object> selectTierCashDepositById(String id);

    public Map<String, Object> selectTierCashDepositByInstanceId(String instanceId);

    /**
     * 查询feike列表
     * 
     * @param tierCashDeposit feike
     * @return feike集合
     */
    public List<TierCashDeposit> selectTierCashDepositList(TierCashDeposit tierCashDeposit);

    /**
     * 新增feike
     * 
     * @param tierCashDeposit feike
     * @return 结果
     */
    public int insertTierCashDeposit(TierCashDeposit tierCashDeposit);

    /**
     * 修改feike
     * 
     * @param tierCashDeposit feike
     * @return 结果
     */
    public int updateTierCashDeposit(TierCashDeposit tierCashDeposit);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteTierCashDepositByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteTierCashDepositById(String id);


    List<AnnualReviewyHistoryVO> selectTierCashDepositListAll(AnnualReviewy annualReviewy, TierCashDeposit tierCashDeposit);

    Map<String, Object> selectTierCashDepositBy(Map<String, Object> map);

    List<TierCashDeposit> selectTierCashDepositByDralerName(String dralerName);


    List<ParkingLocationVO1> selectTierCashDepositLists();
}
