package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.AnnualReviewy;
import com.ruoyi.feike.domain.ParkingLocationRegistration;
import com.ruoyi.feike.domain.vo.AnnualReviewyHistoryVO;

import java.util.List;
import java.util.Map;

/**
 * feikeService接口
 * 
 * @author ruoyi
 * @date 2022-08-02
 */
public interface IParkingLocationRegistrationService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Map<String, Object> selectParkingLocationRegistrationById(String id);

    public Map<String, Object> selectParkingLocationRegistrationByInstanceId(String instanceId);

    /**
     * 查询feike列表
     * 
     * @param parkingLocationRegistration feike
     * @return feike集合
     */
    public List<ParkingLocationRegistration> selectParkingLocationRegistrationList(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 新增feike
     * 
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    public int insertParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 修改feike
     * 
     * @param parkingLocationRegistration feike
     * @return 结果
     */
    public int updateParkingLocationRegistration(ParkingLocationRegistration parkingLocationRegistration);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteParkingLocationRegistrationByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteParkingLocationRegistrationById(String id);


    public List<AnnualReviewyHistoryVO> selectParkingLocationRegistrationListAll(AnnualReviewy annualReviewy, ParkingLocationRegistration parkingLocationRegistration);

    Map<String, Object> selectParkingLocationRegistration(Map<String, Object> map);


    public int insertParkingLocationRegistrationMap(Map<String, Object> map);

}
