package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.BasicContract;
import com.ruoyi.feike.domain.vo.BasicContractVO;

/**
 * 合同信息Service接口
 *
 * @author ybw
 * @date 2022-07-19
 */
public interface IBasicContractService
{
    /**
     * 查询合同信息
     *
     * @param id 合同信息ID
     * @return 合同信息
     */
    public BasicContract selectBasicContractById(String id);

    /**
     * 查询合同信息列表
     *
     * @param basicContract 合同信息
     * @return 合同信息集合
     */
    public List<BasicContract> selectBasicContractList(BasicContract basicContract);

    public List<BasicContract> selectBasicContractListgroupby(BasicContract basicContract);

    /**
     * 新增合同信息
     *
     * @param basicContract 合同信息
     * @return 结果
     */
    public int insertBasicContract(BasicContract basicContract);

    /**
     * 修改合同信息
     *
     * @param basicContract 合同信息
     * @return 结果
     */
    public int updateBasicContract(BasicContract basicContract);

    public int updateBasicContractgroupby(BasicContractVO basicContractVO);

    /**
     * 批量删除合同信息
     *
     * @param ids 需要删除的合同信息ID
     * @return 结果
     */
    public int deleteBasicContractByIds(String[] ids);

    /**
     * 删除合同信息信息
     *
     * @param id 合同信息ID
     * @return 结果
     */
    public int deleteBasicContractById(String id);

    /**
     * 根据优先级、主机厂、授信类型查询对应合同
     *
     * @param resultMap
     * @return 合同信息集合
     */
    public List<BasicContract> listBasicContract(Map<String, Object> resultMap);
}
