package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.ProposalByCommericalAndMarketingMapper;
import com.ruoyi.feike.domain.ProposalByCommericalAndMarketing;
import com.ruoyi.feike.service.IProposalByCommericalAndMarketingService;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-18
 */
@Service
public class ProposalByCommericalAndMarketingServiceImpl implements IProposalByCommericalAndMarketingService 
{
    @Autowired
    private ProposalByCommericalAndMarketingMapper proposalByCommericalAndMarketingMapper;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public ProposalByCommericalAndMarketing selectProposalByCommericalAndMarketingById(String id)
    {
        return proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return feike
     */
    @Override
    public List<ProposalByCommericalAndMarketing> selectProposalByCommericalAndMarketingList(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        return proposalByCommericalAndMarketingMapper.selectProposalByCommericalAndMarketingList(proposalByCommericalAndMarketing);
    }

    /**
     * 新增feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    @Override
    public int insertProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        return proposalByCommericalAndMarketingMapper.insertProposalByCommericalAndMarketing(proposalByCommericalAndMarketing);
    }

    /**
     * 修改feike
     * 
     * @param proposalByCommericalAndMarketing feike
     * @return 结果
     */
    @Override
    public int updateProposalByCommericalAndMarketing(ProposalByCommericalAndMarketing proposalByCommericalAndMarketing)
    {
        return proposalByCommericalAndMarketingMapper.updateProposalByCommericalAndMarketing(proposalByCommericalAndMarketing);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteProposalByCommericalAndMarketingByIds(String[] ids)
    {
        return proposalByCommericalAndMarketingMapper.deleteProposalByCommericalAndMarketingByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteProposalByCommericalAndMarketingById(String id)
    {
        return proposalByCommericalAndMarketingMapper.deleteProposalByCommericalAndMarketingById(id);
    }
}
