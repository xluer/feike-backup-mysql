package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.Securities;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-11
 */
public interface ISecuritiesService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public Securities selectSecuritiesById(String id);

    /**
     * 查询feike列表
     * 
     * @param securities feike
     * @return feike集合
     */
    public List<Securities> selectSecuritiesList(Securities securities);

    /**
     * 新增feike
     * 
     * @param securities feike
     * @return 结果
     */
    public int insertSecurities(Securities securities);

    /**
     * 修改feike
     * 
     * @param securities feike
     * @return 结果
     */
    public int updateSecurities(Securities securities);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteSecuritiesByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteSecuritiesById(String id);
}
