package com.ruoyi.feike.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.feike.domain.TemporaryConditionFollowUp;

/**
 * feikeService接口
 * 
 * @author zmh
 * @date 2022-07-12
 */
public interface ITemporaryConditionFollowUpService 
{
    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    public TemporaryConditionFollowUp selectTemporaryConditionFollowUpById(String id);

    /**
     * 查询feike列表
     * 
     * @param temporaryConditionFollowUp feike
     * @return feike集合
     */
    public List<TemporaryConditionFollowUp> selectTemporaryConditionFollowUpList(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 新增feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    public int insertTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 修改feike
     * 
     * @param temporaryConditionFollowUp feike
     * @return 结果
     */
    public int updateTemporaryConditionFollowUp(TemporaryConditionFollowUp temporaryConditionFollowUp);

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteTemporaryConditionFollowUpByIds(String[] ids);

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    public int deleteTemporaryConditionFollowUpById(String id);

    /**
     * 新增经销商明细表
     *
     * @param  resultMap
     * @return 结果
     */
    public int saveTcfu(Map<String, Object> resultMap);
}
