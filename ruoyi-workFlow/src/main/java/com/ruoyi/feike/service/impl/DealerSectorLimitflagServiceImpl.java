package com.ruoyi.feike.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerSectorLimitflagMapper;
import com.ruoyi.feike.domain.DealerSectorLimitflag;
import com.ruoyi.feike.service.IDealerSectorLimitflagService;

/**
 * FlagService业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-25
 */
@Service
public class DealerSectorLimitflagServiceImpl implements IDealerSectorLimitflagService 
{
    @Autowired
    private DealerSectorLimitflagMapper dealerSectorLimitflagMapper;

    /**
     * 查询Flag
     * 
     * @param id FlagID
     * @return Flag
     */
    @Override
    public DealerSectorLimitflag selectDealerSectorLimitflagById(String id)
    {
        return dealerSectorLimitflagMapper.selectDealerSectorLimitflagById(id);
    }

    /**
     * 查询Flag列表
     * 
     * @param dealerSectorLimitflag Flag
     * @return Flag
     */
    @Override
    public List<DealerSectorLimitflag> selectDealerSectorLimitflagList(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.selectDealerSectorLimitflagList(dealerSectorLimitflag);
    }

    /**
     * 新增Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    @Override
    public int insertDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.insertDealerSectorLimitflag(dealerSectorLimitflag);
    }

    /**
     * 修改Flag
     * 
     * @param dealerSectorLimitflag Flag
     * @return 结果
     */
    @Override
    public int updateDealerSectorLimitflag(DealerSectorLimitflag dealerSectorLimitflag)
    {
        return dealerSectorLimitflagMapper.updateDealerSectorLimitflag(dealerSectorLimitflag);
    }

    /**
     * 批量删除Flag
     * 
     * @param ids 需要删除的FlagID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorLimitflagByIds(String[] ids)
    {
        return dealerSectorLimitflagMapper.deleteDealerSectorLimitflagByIds(ids);
    }

    /**
     * 删除Flag信息
     * 
     * @param id FlagID
     * @return 结果
     */
    @Override
    public int deleteDealerSectorLimitflagById(String id)
    {
        return dealerSectorLimitflagMapper.deleteDealerSectorLimitflagById(id);
    }
}
