package com.ruoyi.feike.service;

import com.ruoyi.feike.domain.BasicInformation;
import com.ruoyi.feike.domain.DealerGroup;

import java.util.List;

/**
 * feikeService接口
 *
 * @author zmh
 * @date 2022-07-05
 */
public interface IDealerGroupService
{
    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    public DealerGroup selectDealerGroupById(String id);
    public List<BasicInformation> selectDealerGroupByByInstanceId(String instanceId);

    /**
     * 查询feike列表
     *
     * @param dealerGroup feike
     * @return feike集合
     */
    public List<DealerGroup> selectDealerGroupList(DealerGroup dealerGroup);

    /**
     * 新增feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    public int insertDealerGroup(DealerGroup dealerGroup);

    /**
     * 修改feike
     *
     * @param dealerGroup feike
     * @return 结果
     */
    public int updateDealerGroup(DealerGroup dealerGroup);

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    public int deleteDealerGroupByIds(String[] ids);

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    public int deleteDealerGroupById(String id);
}
