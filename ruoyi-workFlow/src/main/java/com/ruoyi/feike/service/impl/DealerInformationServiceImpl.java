package com.ruoyi.feike.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ruoyi.feike.domain.vo.DealerformationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerInformationMapper;
import com.ruoyi.feike.domain.DealerInformation;
import com.ruoyi.feike.service.IDealerInformationService;

/**
 * feikeService业务层处理
 *
 * @author ybw
 * @date 2022-07-12
 */
@Service
public class DealerInformationServiceImpl implements IDealerInformationService
{
    @Autowired
    private DealerInformationMapper dealerInformationMapper;

    /**
     * 查询feike
     *
     * @param id feikeID
     * @return feike
     */
    @Override
    public DealerInformation selectDealerInformationById(String id)
    {
        return dealerInformationMapper.selectDealerInformationById(id);
    }

    /**
     * 根据dealername查
     *
     * @param map
     * @return feike
     */
    @Override
    public DealerformationVO selectDealerInformationByname(Map<String, Object> map)
    {
        String dealerName = (String)map.get("dealername");
        DealerformationVO dealerformationVO = new DealerformationVO();
        List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByname(dealerName);
        dealerformationVO.setId(dealerInformations.get(0).getId());
        dealerformationVO.setDealerNameEN(dealerInformations.get(0).getEnName());
        dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
        dealerformationVO.setIndividualGuarantor(dealerInformations.get(0).getIndividualGuarantor());
        dealerformationVO.setCorporateGuarantor(dealerInformations.get(0).getCorporateGuarantor());
        dealerformationVO.setShareholderStructure(dealerInformations.get(0).getShareholderStructure());
        ArrayList<String> sectors = new ArrayList<>();
        ArrayList<String> dealerCodes = new ArrayList<>();
        for (int i1 = 0; i1 < dealerInformations.size(); i1++) {
            String make = dealerInformations.get(i1).getMake();
            String dealerCode = dealerInformations.get(i1).getDealerCode();
            sectors.add(make);
            dealerCodes.add(dealerCode);
        }
        dealerformationVO.setSector(sectors);
        dealerformationVO.setDealerCodeFromWFS(dealerCodes);
        return dealerformationVO;
    }

    /**
     * 查询feike列表
     *
     * @param dealerInformation feike
     * @return feike
     */
    @Override
    public List<DealerInformation> selectDealerInformationList(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationList(dealerInformation);
    }

    @Override
    public List<DealerInformation> selectDealerInformationListnew(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.selectDealerInformationListnew(dealerInformation);
    }

    /**
     * 新增feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    public int insertDealerInformation(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.insertDealerInformation(dealerInformation);
    }

    /**
     * 修改feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    public int updateDealerInformation(DealerInformation dealerInformation)
    {
        return dealerInformationMapper.updateDealerInformation(dealerInformation);
    }

    /**
     * 修改dealername相同的全部数据feike
     *
     * @param dealerInformation feike
     * @return 结果
     */
    @Override
    public int updateDealerInformationnew(DealerformationVO dealerInformation)
    {
        return dealerInformationMapper.updateDealerInformationnew(dealerInformation);
    }

    /**
     * 批量删除feike
     *
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerInformationByIds(String[] ids)
    {
        return dealerInformationMapper.deleteDealerInformationByIds(ids);
    }

    /**
     * 删除feike信息
     *
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerInformationById(String id)
    {
        return dealerInformationMapper.deleteDealerInformationById(id);
    }

    @Override
    public List<DealerInformation> getGroups() {
        return dealerInformationMapper.getGroups();
    }
    @Override
    public List<DealerInformation> getDealers(String groupName) {
        return dealerInformationMapper.getDealers(groupName);
    }

    @Override
    public List<DealerformationVO> listByName(List<DealerInformation> params) {
        List<DealerformationVO> dealerformationVOS = new ArrayList<>();
        for (int i = 0; i < params.size(); i++) {
            DealerformationVO dealerformationVO = new DealerformationVO();
            String dealerName = params.get(i).getDealerName();
            List<DealerInformation> dealerInformations = dealerInformationMapper.selectDealerInformationByname(dealerName);
            dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
            dealerformationVO.setDealerNameEN(dealerInformations.get(0).getEnName());
            dealerformationVO.setDealerNameCN(dealerInformations.get(0).getDealerName());
            dealerformationVO.setIndividualGuarantor(dealerInformations.get(0).getIndividualGuarantor());
            dealerformationVO.setCorporateGuarantor(dealerInformations.get(0).getCorporateGuarantor());
            ArrayList<String> sectors = new ArrayList<>();
            ArrayList<String> dealerCodes = new ArrayList<>();
            for (int i1 = 0; i1 < dealerInformations.size(); i1++) {
                String make = dealerInformations.get(i1).getMake();
                String dealerCode = dealerInformations.get(i1).getDealerCode();
                sectors.add(make);
                dealerCodes.add(dealerCode);
            }
            dealerformationVO.setSector(sectors);
            dealerformationVO.setDealerCodeFromWFS(dealerCodes);
            dealerformationVOS.add(dealerformationVO);
        }
        return dealerformationVOS;
    }
}
