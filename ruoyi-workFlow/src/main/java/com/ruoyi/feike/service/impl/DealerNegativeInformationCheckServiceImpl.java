package com.ruoyi.feike.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.StatusReturnCodeConstant;
import com.ruoyi.common.exception.BaseException;
import com.ruoyi.common.msg.ObjectRestResponse;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.feike.domain.ApprovedLimitAndCondition;
import com.ruoyi.feike.domain.CreditCondition;
import com.ruoyi.feike.service.IApprovedLimitAndConditionService;
import com.ruoyi.feike.service.ICreditConditionService;
import com.ruoyi.feike.service.ITemporaryConditionFollowUpService;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.feike.mapper.DealerNegativeInformationCheckMapper;
import com.ruoyi.feike.domain.DealerNegativeInformationCheck;
import com.ruoyi.feike.service.IDealerNegativeInformationCheckService;
import org.springframework.transaction.annotation.Transactional;

/**
 * feikeService业务层处理
 * 
 * @author zmh
 * @date 2022-07-12
 */
@Service
public class DealerNegativeInformationCheckServiceImpl implements IDealerNegativeInformationCheckService 
{
    @Autowired
    private DealerNegativeInformationCheckMapper dealerNegativeInformationCheckMapper;
    @Autowired
    private IApprovedLimitAndConditionService iApprovedLimitAndConditionService;
    @Autowired
    private ICreditConditionService iCreditConditionService;
    @Autowired
    private ITemporaryConditionFollowUpService iTemporaryConditionFollowUpService;
    @Autowired
    private ProcessRuntime processRuntime;

    /**
     * 查询feike
     * 
     * @param id feikeID
     * @return feike
     */
    @Override
    public DealerNegativeInformationCheck selectDealerNegativeInformationCheckById(String id)
    {
        return dealerNegativeInformationCheckMapper.selectDealerNegativeInformationCheckById(id);
    }

    /**
     * 查询feike列表
     * 
     * @param dealerNegativeInformationCheck feike
     * @return feike
     */
    @Override
    public List<DealerNegativeInformationCheck> selectDealerNegativeInformationCheckList(DealerNegativeInformationCheck dealerNegativeInformationCheck)
    {
        return dealerNegativeInformationCheckMapper.selectDealerNegativeInformationCheckList(dealerNegativeInformationCheck);
    }

    /**
     * 新增feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    @Override
    public int insertDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck)
    {
        return dealerNegativeInformationCheckMapper.insertDealerNegativeInformationCheck(dealerNegativeInformationCheck);
    }

    /**
     * 修改feike
     * 
     * @param dealerNegativeInformationCheck feike
     * @return 结果
     */
    @Override
    public int updateDealerNegativeInformationCheck(DealerNegativeInformationCheck dealerNegativeInformationCheck)
    {
        return dealerNegativeInformationCheckMapper.updateDealerNegativeInformationCheck(dealerNegativeInformationCheck);
    }

    /**
     * 批量删除feike
     * 
     * @param ids 需要删除的feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerNegativeInformationCheckByIds(String[] ids)
    {
        return dealerNegativeInformationCheckMapper.deleteDealerNegativeInformationCheckByIds(ids);
    }

    /**
     * 删除feike信息
     * 
     * @param id feikeID
     * @return 结果
     */
    @Override
    public int deleteDealerNegativeInformationCheckById(String id)
    {
        return dealerNegativeInformationCheckMapper.deleteDealerNegativeInformationCheckById(id);
    }

    /**
     * 新增feike
     *
     * @param resultMap
     * @return 结果
     */
    @Override
    @Transactional
    public int saveDnic(Map<String, Object> resultMap)
    {
        try {
            DealerNegativeInformationCheck hdnic = JSONObject.parseObject(JSON.toJSONString(resultMap.get("hdnic")),DealerNegativeInformationCheck.class);
//            List<ApprovedLimitAndCondition> halac = JSONObject.parseArray(JSON.toJSONString(resultMap.get("halac")), ApprovedLimitAndCondition.class);
//            List<CreditCondition> hcc = JSONObject.parseArray(JSON.toJSONString(resultMap.get("hcc")), CreditCondition.class);
            String dealerId = (String) resultMap.get("dealerId");
            String dealerName = (String) resultMap.get("dealerName");
            if (StringUtils.isNull(hdnic)) {
                throw new BaseException("表数据不能为空！");
            }
            String id = IdUtils.simpleUUID();
            String instanceId = IdUtils.simpleUUID();
            hdnic.setId(id);
            hdnic.setDealerid(dealerId);
            hdnic.setDealername(dealerName);

            ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder
                    .start()
                    .withProcessDefinitionKey("Process_a")
                    .withName(SecurityUtils.getLoginUser().getUser().getNickName()+"的信审")
                    .withBusinessKey(instanceId)
//                    .withVariable("deptLeader",join)
                    .build());

            resultMap.put("instanceId",instanceId);
            hdnic.setInstanceId(instanceId);

//            iApprovedLimitAndConditionService.saveAlac(resultMap);
            iCreditConditionService.saveCreditcs(resultMap);
            iTemporaryConditionFollowUpService.saveTcfu(resultMap);
            return dealerNegativeInformationCheckMapper.insertDealerNegativeInformationCheck(hdnic);
        }catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(e.toString());
        }
    }
}
