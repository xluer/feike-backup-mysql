package com.ruoyi.feike.service;

import java.util.List;
import com.ruoyi.feike.domain.DealercodeCount;

/**
 * countService接口
 * 
 * @author ybw
 * @date 2022-08-09
 */
public interface IDealercodeCountService 
{
    /**
     * 查询count
     * 
     * @param id countID
     * @return count
     */
    public DealercodeCount selectDealercodeCountById(Long id);

    /**
     * 查询count列表
     * 
     * @param dealercodeCount count
     * @return count集合
     */
    public List<DealercodeCount> selectDealercodeCountList(DealercodeCount dealercodeCount);

    /**
     * 新增count
     * 
     * @param dealercodeCount count
     * @return 结果
     */
    public int insertDealercodeCount(DealercodeCount dealercodeCount);

    /**
     * 修改count
     * 
     * @param dealercodeCount count
     * @return 结果
     */
    public int updateDealercodeCount(DealercodeCount dealercodeCount);

    /**
     * 批量删除count
     * 
     * @param ids 需要删除的countID
     * @return 结果
     */
    public int deleteDealercodeCountByIds(Long[] ids);

    /**
     * 删除count信息
     * 
     * @param id countID
     * @return 结果
     */
    public int deleteDealercodeCountById(Long id);
}
