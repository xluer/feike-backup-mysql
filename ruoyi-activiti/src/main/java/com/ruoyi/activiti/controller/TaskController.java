package com.ruoyi.activiti.controller;


import com.github.pagehelper.Page;
import com.ruoyi.activiti.domain.dto.ActTaskDTO;
import com.ruoyi.activiti.domain.dto.ActWorkflowFormDataDTO;
import com.ruoyi.activiti.domain.model.AnnualReviewyTask;
import com.ruoyi.activiti.domain.vo.SearchVo;
import com.ruoyi.activiti.service.IActTaskService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.SecurityUtils;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/task")
public class TaskController extends BaseController {
    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private TaskService taskService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private IActTaskService actTaskService;



    //获取我的代办任务
    @GetMapping(value = "/list")
    public TableDataInfo getTasks() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        System.out.println("pageDomain=="+pageDomain.toString());
        Page<ActTaskDTO> hashMaps = actTaskService.selectProcessDefinitionList(pageDomain);
        System.out.println("封装后的结果集=="+getDataTable(hashMaps).toString());
         return getDataTable(hashMaps);
    }

    //查询已处理任务列表
    @GetMapping(value = "/handleList")
    public TableDataInfo getHandleTasks() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        System.out.println("pageDomain=="+pageDomain.toString());
        Page<ActTaskDTO> hashMaps = actTaskService.selectProcessDefinitionHandleList(pageDomain);
        return getDataTable(hashMaps);
    }

    //条件查询--搜索按钮方法
    @PostMapping(value = "/search")
    public TableDataInfo getPageInfo(@RequestBody SearchVo searchVo) {
        PageDomain pageDomain = TableSupport.buildPageRequest();

        Page<ActTaskDTO> hashMaps = actTaskService.selectProcessDefinitionSearchList(searchVo,pageDomain);
        return getDataTable(hashMaps);

    }


    /**
     * 查询已处理任务列表。
     *
     * @return 已处理任务列表
     */
    @GetMapping(value = "/yibanList")
    public List<AnnualReviewyTask> queryDoneTasks() {
        String username = SecurityUtils.getUsername(); //guanyu
        List<HistoricTaskInstance> taskList  = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(username)
                .finished()
                .list();
        System.out.println("已办列表--taskList=="+taskList.toString());
        List<AnnualReviewyTask> leaveTasks = new ArrayList<>();
        for (HistoricTaskInstance task : taskList) {
            AnnualReviewyTask annualReviewyTask = new AnnualReviewyTask();
            annualReviewyTask.setTaskId(task.getId());
            annualReviewyTask.setName(task.getName());
            annualReviewyTask.setProcessDefinitionId(task.getProcessDefinitionId());
            leaveTasks.add(annualReviewyTask);
        }
        return leaveTasks;
    }

    //渲染表单
    @GetMapping(value = "/formDataShow/{taskID}")
    public AjaxResult formDataShow(@PathVariable("taskID") String taskID) {

        return AjaxResult.success(actTaskService.formDataShow(taskID));
    }

    //保存表单
    @PostMapping(value = "/formDataSave/{taskID}")
    public AjaxResult formDataSave(@PathVariable("taskID") String taskID,
                                   @RequestBody   List<ActWorkflowFormDataDTO> formData ) throws ParseException {
        return toAjax(actTaskService.formDataSave(taskID, formData));

    }

    @GetMapping(value = "/getById/{taskID}")
    public AjaxResult getById(@PathVariable("taskID") String taskID) throws ParseException {
        return AjaxResult.success(actTaskService.getById(taskID));
    }

}
