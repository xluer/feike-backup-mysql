

package com.ruoyi.activiti.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.util.StringUtil;
import com.ruoyi.activiti.domain.ActRuIdentitylink;
import com.ruoyi.activiti.domain.ActWorkflowFormData;
import com.ruoyi.activiti.domain.dto.ActTaskDTO;
import com.ruoyi.activiti.domain.dto.ActWorkflowFormDataDTO;
import com.ruoyi.activiti.domain.vo.SearchVo;
import com.ruoyi.activiti.mapper.ActRuIdentitylinkMapper;
import com.ruoyi.activiti.service.IActTaskService;
import com.ruoyi.activiti.service.IActWorkflowFormDataService;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.utils.EmailUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysPostMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricVariableUpdate;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ActTaskServiceImpl implements IActTaskService {

    @Autowired
    private RepositoryService repositoryService;

    @Resource
    private TaskService taskService;

    @Autowired
    private TaskRuntime taskRuntime;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private IActWorkflowFormDataService actWorkflowFormDataService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ISysUserService userService;

    @Value("${spring.mail.from}") // 从application.yml配置文件中获取
    private String from; //  发送发邮箱地址

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private ActRuIdentitylinkMapper actRuIdentitylinkMapper;

    @Autowired
    private SysPostMapper sysPostMapper;

    @Autowired
    private SysUserMapper sysUserMapper;


    @Override
    public Page<ActTaskDTO> selectProcessDefinitionList(PageDomain pageDomain) {
        Page<ActTaskDTO> list = new Page<ActTaskDTO>();
        org.activiti.api.runtime.shared.query.Page<Task> pageTasks = taskRuntime.tasks(Pageable.of((pageDomain.getPageNum() - 1) * pageDomain.getPageSize(), pageDomain.getPageSize()));
        List<Task> tasks = pageTasks.getContent();
        int totalItems = pageTasks.getTotalItems();
        list.setTotal(totalItems);
        if (totalItems != 0) {
            Set<String> processInstanceIdIds = tasks.parallelStream()
                                                    .map(t -> t.getProcessInstanceId())
                                                    .collect(Collectors.toSet());
            List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
                                                                      .processInstanceIds(processInstanceIdIds)
                                                                      .list();
            System.out.println("processInstanceList==" + processInstanceList.toString());
            List<ActTaskDTO> actTaskDTOS = tasks.stream()
                                                .map(t -> new ActTaskDTO(t, processInstanceList.parallelStream()
                                                                                               .filter(pi -> t.getProcessInstanceId()
                                                                                                              .equals(pi.getId()))
                                                                                               .findAny()
                                                                                               .get()))
                                                .collect(Collectors.toList());
            System.out.println("actTaskDTOS==" + actTaskDTOS.toString());
            list.addAll(actTaskDTOS);

        }
        System.out.println("获取到的结果集==" + list.toString());
        return list;
    }

    //获取已办列表
    @Override
    public Page<ActTaskDTO> selectProcessDefinitionHandleList(PageDomain pageDomain) {
        Page<ActTaskDTO> list = new Page<ActTaskDTO>();
        org.activiti.api.runtime.shared.query.Page<Task> pageTasks = taskRuntime.tasks(Pageable.of((pageDomain.getPageNum() - 1) * pageDomain.getPageSize(), pageDomain.getPageSize()));
        List<Task> tasks = pageTasks.getContent();
        int totalItems = pageTasks.getTotalItems();
        list.setTotal(totalItems);
        if (totalItems != 0) {
            Set<String> processInstanceIdIds = tasks.parallelStream()
                                                    .map(t -> t.getProcessInstanceId())
                                                    .collect(Collectors.toSet());
            List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
                                                                      .processInstanceIds(processInstanceIdIds)
                                                                      .list();
            List<ActTaskDTO> actTaskDTOS = tasks.stream()
                                                .map(t -> new ActTaskDTO(t, processInstanceList.parallelStream()
                                                                                               .filter(pi -> t.getProcessInstanceId()
                                                                                                              .equals(pi.getId()))
                                                                                               .findAny()
                                                                                               .get()))
                                                .collect(Collectors.toList());
            list.addAll(actTaskDTOS);

        }
        return list;
    }

    //根据条件搜索
    @Override
    public Page<ActTaskDTO> selectProcessDefinitionSearchList(SearchVo searchVo, PageDomain pageDomain) {
        pageDomain.setPageNum(1);
        pageDomain.setPageSize(20);
        Page<ActTaskDTO> list = new Page<ActTaskDTO>();
        // org.activiti.api.runtime.shared.query.Page<Task> pageTasks = taskRuntime.tasks(Pageable.of((pageDomain.getPageNum() - 1) * pageDomain.getPageSize(), pageDomain.getPageSize()));
        org.activiti.api.runtime.shared.query.Page<Task> pageTasks = taskRuntime.tasks(Pageable.of(0, 10));

        List<Task> tasks = pageTasks.getContent();
        int totalItems = pageTasks.getTotalItems();
        System.out.println("tasks==" + tasks.toString());
        //tasks==[TaskImpl{id='0ae97a6b-fd10-11ec-926c-60f26273afc4', owner='null', assignee='null', name='信审勾选填写两个问题', description='null', createdDate=Wed Jul 06 17:43:08 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:1:425e7506-fcd6-11ec-a3fa-60f26273afc4', processInstanceId='0ae95357-fd10-11ec-926c-60f26273afc4', parentTaskId='null', formKey='null', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='2111e147-00d4-11ed-af01-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Mon Jul 11 12:44:20 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:3:c7084174-fdce-11ec-8bfc-60f26273afc4', processInstanceId='21058533-00d4-11ed-af01-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='34972e69-01d8-11ed-b55f-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Tue Jul 12 19:46:02 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='annualVerification:2:559720dd-01d6-11ed-8ad6-005056c00008', processInstanceId='348fb455-01d8-11ed-b55f-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='37042e6f-00fd-11ed-8e56-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Mon Jul 11 17:38:26 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:3:c7084174-fdce-11ec-8bfc-60f26273afc4', processInstanceId='36fd298b-00fd-11ed-8e56-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='373fdc70-fd22-11ec-926c-60f26273afc4', owner='null', assignee='null', name='信审勾选填写两个问题', description='null', createdDate=Wed Jul 06 19:53:13 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:2:d46b6825-fd12-11ec-926c-60f26273afc4', processInstanceId='373fb55c-fd22-11ec-926c-60f26273afc4', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='423e044c-0354-11ed-ad6c-005056c00008', owner='null', assignee='null', name='部门领导审批', description='null', createdDate=Thu Jul 14 17:06:34 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='leave:1:d437a1dd-f5c9-11ec-ad82-60f26273afc4', processInstanceId='423d6806-0354-11ed-ad6c-005056c00008', parentTaskId='null', formKey='deptLeaderVerify', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='6a946ed6-fcdc-11ec-a3fa-60f26273afc4', owner='null', assignee='null', name='部门领导审批', description='null', createdDate=Wed Jul 06 11:33:35 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='leave:1:d437a1dd-f5c9-11ec-ad82-60f26273afc4', processInstanceId='6a93d28b-fcdc-11ec-a3fa-60f26273afc4', parentTaskId='null', formKey='deptLeaderVerify', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='6a946ed7-fcdc-11ec-a3fa-60f26273afc4', owner='null', assignee='null', name='部门领导审批', description='null', createdDate=Wed Jul 06 11:33:35 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='leave:1:d437a1dd-f5c9-11ec-ad82-60f26273afc4', processInstanceId='6a93d28a-fcdc-11ec-a3fa-60f26273afc4', parentTaskId='null', formKey='deptLeaderVerify', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='91632331-fd10-11ec-926c-60f26273afc4', owner='null', assignee='null', name='信审勾选填写两个问题', description='null', createdDate=Wed Jul 06 17:46:54 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:1:425e7506-fcd6-11ec-a3fa-60f26273afc4', processInstanceId='9162fc1d-fd10-11ec-926c-60f26273afc4', parentTaskId='null', formKey='null', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='b2fb3f6f-0315-11ed-ad6c-005056c00008', owner='null', assignee='null', name='部门领导审批', description='null', createdDate=Thu Jul 14 09:38:45 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='leave:1:d437a1dd-f5c9-11ec-ad82-60f26273afc4', processInstanceId='b2f43a89-0315-11ed-ad6c-005056c00008', parentTaskId='null', formKey='deptLeaderVerify', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='b8557e6f-fd0f-11ec-926c-60f26273afc4', owner='null', assignee='null', name='信审勾选填写两个问题', description='null', createdDate=Wed Jul 06 17:40:50 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:1:425e7506-fcd6-11ec-a3fa-60f26273afc4', processInstanceId='b850c37b-fd0f-11ec-926c-60f26273afc4', parentTaskId='null', formKey='null', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='c7bde62f-024a-11ed-914d-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Wed Jul 13 09:26:11 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='annualVerification:2:559720dd-01d6-11ed-8ad6-005056c00008', processInstanceId='c7b581bb-024a-11ed-914d-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='cf1ef7a0-0419-11ed-aabc-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Fri Jul 15 16:40:41 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='annualVerification:2:559720dd-01d6-11ed-8ad6-005056c00008', processInstanceId='cf11630c-0419-11ed-aabc-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='ede1d665-fd0f-11ec-926c-60f26273afc4', owner='null', assignee='null', name='信审勾选填写两个问题', description='null', createdDate=Wed Jul 06 17:42:19 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:1:425e7506-fcd6-11ec-a3fa-60f26273afc4', processInstanceId='ede1d661-fd0f-11ec-926c-60f26273afc4', parentTaskId='null', formKey='null', status=CREATED, processDefinitionVersion=null, businessKey=null}, TaskImpl{id='ee71957d-00e7-11ed-8dc0-005056c00008', owner='null', assignee='null', name='RM', description='null', createdDate=Mon Jul 11 15:06:05 CST 2022, claimedDate=null, dueDate=null, priority=50, processDefinitionId='Process_a:3:c7084174-fdce-11ec-8bfc-60f26273afc4', processInstanceId='ee6c1739-00e7-11ed-8dc0-005056c00008', parentTaskId='null', formKey='Activity_1hrzbia', status=CREATED, processDefinitionVersion=null, businessKey=null}]

        TaskQuery taskQuery = taskService.createTaskQuery();
        if (StringUtils.isNotBlank(searchVo.getProcessName())) {
            taskQuery.taskNameLike("%" + searchVo.getProcessName() + "%");
        }
        if (searchVo.getStartDate() != null) {
            taskQuery.taskCreatedAfter(searchVo.getStartDate());
        }
        if (searchVo.getEndDate() != null) {
            taskQuery.taskCreatedBefore(searchVo.getEndDate());
        }
        // taskQuery.processDefinitionKey("Process_a");
        // List<Task> taskList = taskQuery.listPage(pageDomain.getPageNum(), pageDomain.getPageSize());
        List<org.activiti.engine.task.Task> taskList = taskQuery
                // .taskNameLike("%" + searchVo.getProcessName() + "%")
                // .taskCreatedAfter(searchVo.getStartDate())
                // .taskCreatedBefore(searchVo.getEndDate())
                .list();
        System.out.println("taskList===" + taskList.toString());

        if (totalItems != 0) {
            // List<ActTaskDTO> actTaskDTOS = new ArrayList<>();
            for (Task task : tasks) {
                Set<String> processInstanceIdIds = taskList.parallelStream()
                                                           .map(t -> t.getProcessInstanceId())
                                                           .collect(Collectors.toSet());
                System.out.println("processInstanceIdIds===" + processInstanceIdIds.toString());
                //processInstanceIdIds===[cf11630c-0419-11ed-aabc-005056c00008, ee6c1739-00e7-11ed-8dc0-005056c00008, 36fd298b-00fd-11ed-8e56-005056c00008, c7b581bb-024a-11ed-914d-005056c00008, 21058533-00d4-11ed-af01-005056c00008, 348fb455-01d8-11ed-b55f-005056c00008]
                List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
                                                                          .processInstanceIds(processInstanceIdIds)
                                                                          .list();
                System.out.println("processInstanceList===" + processInstanceList.toString());
                //processInstanceList===[ProcessInstance[21058533-00d4-11ed-af01-005056c00008], ProcessInstance[348fb455-01d8-11ed-b55f-005056c00008], ProcessInstance[36fd298b-00fd-11ed-8e56-005056c00008], ProcessInstance[c7b581bb-024a-11ed-914d-005056c00008], ProcessInstance[cf11630c-0419-11ed-aabc-005056c00008], ProcessInstance[ee6c1739-00e7-11ed-8dc0-005056c00008]]
                List<ActTaskDTO> actTaskDTOS = tasks.stream()
                                                    .map(t -> new ActTaskDTO(t, processInstanceList.parallelStream()
                                                                                                   .filter(pi -> t.getProcessInstanceId()
                                                                                                                  .equals(pi.getId()))
                                                                                                   .findAny()
                                                                                                   .get()))
                                                    .collect(Collectors.toList());
                System.out.println("actTaskDTOS===" + actTaskDTOS.toString());
                // ActTaskDTO actTaskDTO = new ActTaskDTO();
                // actTaskDTO.setId(task.getId());
                // actTaskDTO.setName(task.getName());
                // actTaskDTO.setStatus(task.getDelegationState().toString());
                // actTaskDTO.setCreatedDate(task.getCreateTime());
                // actTaskDTO.setInstanceName(task.na());
                // actTaskDTO.setDefinitionKey(task.getTaskDefinitionKey());
                // actTaskDTO.setBusinessKey(task.());

                // {
                //     "params": {},
                //     "id": "2111e147-00d4-11ed-af01-005056c00008",
                //         "name": "RM",
                //         "status": "CREATED",
                //         "createdDate": "2022-07-11 12:44:20",
                //         "instanceName": "若依的授信",
                //         "definitionKey": "Process_a",
                //         "businessKey": "7154f7ce27674565b19264c2c01deab7"
                // },

                list.addAll(actTaskDTOS);
            }
        }

        // list.addAll(tasks);
        // list.addAll((Collection<? extends ActTaskDTO>) taskss);

        return list;
    }

    @Override
    public List<String> formDataShow(String taskID) {
        Task task = taskRuntime.task(taskID);
/*  ------------------------------------------------------------------------------
            FormProperty_0ueitp2--__!!类型--__!!名称--__!!是否参数--__!!默认值
            例子：
            FormProperty_0lovri0--__!!string--__!!姓名--__!!f--__!!同意!!__--驳回
            FormProperty_1iu6onu--__!!int--__!!年龄--__!!s

            默认值：无、字符常量、FormProperty_开头定义过的控件ID
            是否参数：f为不是参数，s是字符，t是时间(不需要int，因为这里int等价于string)
            注：类型是可以获取到的，但是为了统一配置原则，都配置到
            */

        //注意!!!!!!!!:表单Key必须要任务编号一模一样，因为参数需要任务key，但是无法获取，只能获取表单key“task.getFormKey()”当做任务key
        UserTask userTask = (UserTask) repositoryService.getBpmnModel(task.getProcessDefinitionId())
                                                        .getFlowElement(task.getFormKey());

        if (userTask == null) {
            return null;
        }
        List<FormProperty> formProperties = userTask.getFormProperties();
        List<String> collect = formProperties.stream()
                                             .map(fp -> fp.getId())
                                             .collect(Collectors.toList());

        return collect;
    }

    @Override
    public int formDataSave(String taskID, List<ActWorkflowFormDataDTO> awfs) throws ParseException {
        Task task = taskRuntime.task(taskID);
        HashMap<String, Object> variables = new HashMap<String, Object>();
        Boolean hasVariables = false;//没有任何参数

        if ("Activity_1hw7ebl".equals(task.getFormKey())) {
            variables.put("toUnderwriter", 0);
            hasVariables = true;
// 查找上一个已完成的user task节点
            List<HistoricDetail> list = historyService.createHistoricDetailQuery()
                                                      .processInstanceId(task.getProcessInstanceId())
                                                      .orderByTime()
                                                      .desc()
                                                      .list();
            String tmpTaskId = "";
            for (HistoricDetail detail : list) {
                if (!"".equals(tmpTaskId) && !tmpTaskId.equals(detail.getTaskId())) {
                    break;
                } else if (detail.getTaskId() == null) {
                    continue;
                } else {
                    tmpTaskId = detail.getTaskId();
                }
                HistoricVariableUpdate historicVariableUpdate = (HistoricVariableUpdate) detail;
                if ("FormProperty_1g5r6sp".equals(historicVariableUpdate.getVariableName())) {
                    if (2 == Integer.valueOf(String.valueOf(historicVariableUpdate.getValue()))) {
                        variables.put("toUnderwriter", 1);
                        hasVariables = true;
                    }
                    break;
                }
            }
        }

        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                                                        .processInstanceId(task.getProcessInstanceId())
                                                        .singleResult();


        //前端传来的字符串，拆分成每个控件
        List<ActWorkflowFormData> acwfds = new ArrayList<>();
        for (ActWorkflowFormDataDTO awf : awfs) {
            ActWorkflowFormData actWorkflowFormData = new ActWorkflowFormData(processInstance.getBusinessKey(), awf, task);
            acwfds.add(actWorkflowFormData);
            //构建参数集合
            if (!"f".equals(awf.getControlIsParam())) {
                if (null != awf.getControlValue()) {
                    variables.put(awf.getControlId(), awf.getControlValue());
                    hasVariables = true;
                }
            }
            // if("dmSelect".equals(awf.getControlId())) {
            //     variables.put(awf.getControlId(), awf.getControlValue());
            //     hasVariables = true;
            // }
        }//for结束
        if (task.getAssignee() == null) {
            taskRuntime.claim(TaskPayloadBuilder.claim()
                                                .withTaskId(task.getId())
                                                .build());
        }
        if (hasVariables) {
            //带参数完成任务
            taskRuntime.complete(TaskPayloadBuilder.complete()
                                                   .withTaskId(taskID)
                                                   .withVariables(variables)
                                                   .build());
        } else {
            taskRuntime.complete(TaskPayloadBuilder.complete()
                                                   .withTaskId(taskID)
                                                   .build());
        }

        // 上一个节点的执行完，再次查询，获取下一个节点的任务信息
        org.activiti.engine.task.Task task1 = taskService.createTaskQuery()
                                                         .processInstanceId(processInstance.getProcessInstanceId())
                                                         .singleResult();
        if (task1 != null) { // 说明有任务

            System.out.println("下一个的任务节点二：" + task1);
            String taskId = task1.getId();
            System.out.println("下一个的任务节点的taskId： " + taskId);
            // 根据 taskId 去 act_ru_integration 表中查询 GROUP_ID_
            ActRuIdentitylink actRuIdentitylink = actRuIdentitylinkMapper.selectActRuIdentitylinkByTaskId(taskId);
            String groupId = actRuIdentitylink.getGroupId();    // 获取组
            String userName = actRuIdentitylink.getUserId();    // 获取用户，返回的是用户名

            if (!StringUtil.isEmpty(groupId)) {
                System.out.println("groupId==" + groupId);
                // 根据组查对应组成员id，在查到对应的用户对象信息
                List<SysUser> userList = sysPostMapper.selectUserListByPostCode(groupId);
                System.out.println("userList==" + userList.toString());
                if (userList != null && userList.size() > 0) {
                    for (SysUser sysUser : userList) {
                        System.out.println("sysUser==" + sysUser);
                        // 调用发送邮件方法
                        try {
                            sendMailByEmailInfo(sysUser);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                if (!StringUtil.isEmpty(userName)) {
                    System.out.println("userName==" + userName);
                    // 根据user_name查询用户
                    SysUser sysUser = sysUserMapper.selectUserByUserName(userName);
                    System.out.println("sysUser==" + sysUser.toString());
                    // 调用发送邮件方法
                    try {
                        sendMailByEmailInfo(sysUser);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        //写入数据库
        return actWorkflowFormDataService.insertActWorkflowFormDatas(acwfds);
    }

    @Override
    public Task getById(String taskID) {
        Task task = taskRuntime.task(taskID);
        return task;
    }

    /**
     * 根据流程实例查询下一节点组信息，在发送邮件
     *
     * @param sysUser
     * @throws IOException
     */
    public void sendMailByEmailInfo(SysUser sysUser) throws IOException {
        // 获取邮箱
        String email = sysUser.getEmail();
        if (!StringUtil.isEmpty(email)) {
            try {
                // 测试文本邮件发送（无附件）
                String to = email;
                String title = "您有待办任务请审批";
                String content = "待办任务请审批";
                //带附件方式调用
                new EmailUtil(from, mailSender).sendMessageCarryFiles(to, title, content, null);
                // return AjaxResult.success();
            } catch (Exception e) {
                e.printStackTrace();
                // return AjaxResult.error(e.getMessage());
            }
        }
    }

}
