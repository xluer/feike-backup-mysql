package com.ruoyi.activiti.mapper;

import com.ruoyi.activiti.domain.ActRuIdentitylink;
import org.apache.ibatis.annotations.Param;

/**
 * @author 神
 * @date 2022年07月26日 9:54
 */
public interface ActRuIdentitylinkMapper {

    public ActRuIdentitylink selectActRuIdentitylinkByTaskId(@Param("taskId") String taskId);
}
