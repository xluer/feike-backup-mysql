package com.ruoyi.activiti.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * @author 神
 * @date 2022年07月14日 17:21
 */
public class SearchVo extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 流程名称
     */
    private String processName;

    private String groupNameCN;
    private String dealerNameCN;
    /**
     * 申请类型
     */
    private String type;
    /**
     * 状态
     */
    private String state;
    /**
     * 开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    /**
     * 结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    private String order = "asc";

    private String orderByColumn;

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderByColumn() {
        return orderByColumn;
    }

    public void setOrderByColumn(String orderByColumn) {
        this.orderByColumn = orderByColumn;
    }

    public String getGroupNameCN() {
        return groupNameCN;
    }

    public void setGroupNameCN(String groupNameCN) {
        this.groupNameCN = groupNameCN;
    }

    public String getDealerNameCN() {
        return dealerNameCN;
    }

    public void setDealerNameCN(String dealerNameCN) {
        this.dealerNameCN = dealerNameCN;
    }

    @Override
    public String toString() {
        return "SearchVo{" +
                "processName='" + processName + '\'' +
                ", type='" + type + '\'' +
                ", state='" + state + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
